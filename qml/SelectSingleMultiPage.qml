import QtQuick

Item {
    id: selectSingleMultiPage

    signal singlePlayerClicked()
    signal hostGameClicked()
    signal joinGameClicked()
    Row {
        anchors.centerIn: parent
        spacing: pageMargins * 3

        Rectangle {
            color: globalColors.cardBg
            border.width: 1
            border.color: globalColors.cardBorder
            radius: pageMargins

            height: selectSingleMultiPage.height * 0.35
            width: height * 1.45

            Image {
                id: image
                anchors.centerIn: parent
                anchors.verticalCenterOffset: - pageMargins * 2
                source: "Player1.png"
                fillMode: Image.PreserveAspectFit
                height: parent.height * 0.5
            }

            Text {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: image.bottom
                    topMargin: pageMargins
                }
                text: qsTr("Single Player")
                font.pixelSize: fontPxSizeM;
            }

            opacity: sMArea.pressed ? 0.6 : 1.0
            MouseArea {
                id: sMArea
                anchors.fill: parent
                onClicked: selectSingleMultiPage.singlePlayerClicked()
            }
        }

        Rectangle {
            id: multiPlayerCard
            color: globalColors.cardBg
            border.width: 1
            border.color: globalColors.cardBorder
            radius: pageMargins

            height: selectSingleMultiPage.height * 0.35
            width: height * 1.45

            visible: gameControl.multiplayer

            Row {
                id: imageRow
                anchors.centerIn: parent
                anchors.verticalCenterOffset: - pageMargins * 2
                spacing: pageMargins
                Image {
                    source: "Player1.png"
                    fillMode: Image.PreserveAspectFit
                    height: multiPlayerCard.height * 0.5
                    width: height * sourceSize.width / sourceSize.height
                }

                Image {
                    source: "Player2.png"
                    fillMode: Image.PreserveAspectFit
                    height: multiPlayerCard.height * 0.5
                    width: height * sourceSize.width / sourceSize.height
                }

            }

            Text {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: imageRow.bottom
                    topMargin: pageMargins
                }
                text: qsTr("Host Game")
                font.pixelSize: fontPxSizeM;
            }

            opacity: mMArea.pressed ? 0.6 : 1.0
            MouseArea {
                id: mMArea
                anchors.fill: parent
                onClicked: selectSingleMultiPage.hostGameClicked()
            }
        }
        Rectangle {
            id: multiPlayerJoinCard
            color: globalColors.cardBg
            border.width: 1
            border.color: globalColors.cardBorder
            radius: pageMargins

            height: selectSingleMultiPage.height * 0.35
            width: height * 1.45

            visible: gameControl.multiplayer

            Row {
                id: imageRowJoin
                anchors.centerIn: parent
                anchors.verticalCenterOffset: - pageMargins * 2
                spacing: pageMargins
                Image {
                    source: "Player1.png"
                    fillMode: Image.PreserveAspectFit
                    height: multiPlayerCard.height * 0.5
                    width: height * sourceSize.width / sourceSize.height
                }

                Image {
                    source: "Player2.png"
                    fillMode: Image.PreserveAspectFit
                    height: multiPlayerCard.height * 0.5
                    width: height * sourceSize.width / sourceSize.height
                }

            }

            Text {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: imageRowJoin.bottom
                    topMargin: pageMargins
                }
                text: qsTr("Join Game")
                font.pixelSize: fontPxSizeM;
            }

            opacity: mjMArea.pressed ? 0.6 : 1.0
            MouseArea {
                id: mjMArea
                anchors.fill: parent
                onClicked: selectSingleMultiPage.joinGameClicked()
            }
        }
    }
}

