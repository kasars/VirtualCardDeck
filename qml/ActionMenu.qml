import QtQuick

Item {
    id: actionMenu

    property bool quitVisible: true
    property bool reloadVisible: true
    property bool undoVisible: true
    property bool setPlayerVisible: true

    readonly property bool isEmpty: !quitVisible && !reloadVisible && !undoVisible && !setPlayerVisible

    signal quitGame()
    signal quitGameConfirmed()
    signal reloadGame()
    signal reloadGameConfirmed()
    signal undoMove()
    signal setPlayerName()


    Column {
        anchors.fill: parent
        anchors.margins: pageMargins
        spacing: pageMargins * 2

        MenuDelegate {
            width: parent.width
            icon: "QuitButton.png"
            text: qsTr("Quit to menu")
            onClicked: quitGame()
            onDragConfirmed: quitGameConfirmed()
            visible: quitVisible
            dragConfirm: true
        }

        MenuDelegate {
            width: parent.width
            icon: "NewButton.png"
            text: qsTr("New")
            onClicked: reloadGame()
            onDragConfirmed: reloadGameConfirmed()
            visible: reloadVisible
            dragConfirm: true
        }

        MenuDelegate {
            width: parent.width
            icon: "UndoButton.png"
            text: qsTr("Undo")
            onClicked: undoMove()
            visible: undoVisible
            dragConfirm: false
        }

        MenuDelegate {
            width: parent.width
            icon: "NameButton.png"
            text: qsTr("Player name")
            onClicked: setPlayerName()
            visible: setPlayerVisible
            dragConfirm: false
        }

    }
}
