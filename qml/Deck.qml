import QtQuick
import VirtualCardDeck

DropArea {
    id: deck
    property alias cards: cardsModel
    readonly property int count: cardRepeater.count
    readonly property double topCardX: firstCardOffsetX + cardOffsetX * count
    readonly property double topCardY: firstCardOffsetY + cardOffsetY * count

    property bool faceUp: true
    property bool bottomDeck: true
    property variant tableItem: deck.parent
    property double cardOffsetX: 0
    property double cardOffsetY: 0
    property double firstCardOffsetX: 0
    property double firstCardOffsetY: 0
    property bool alwaysMoveAll: false
    property bool neverMoveAll: false

    width: tableCardWidth
    height: tableCardHeight

    property string allowedValue: ""
    property string allowedFamily: ""
    property bool allowMultiple: false
    property bool acceptDrops: true
    property variant allowedKeys: []

    property string player: ""
    property string deckId: ""

    keys: mouseArea.draging ? ["Do not accept drops"] : allowedKeys

    onAllowedValueChanged: allowedKeys = updateAllowedKeys()
    onAllowedFamilyChanged: allowedKeys = updateAllowedKeys()
    onCountChanged: allowedKeys = updateAllowedKeys()

    onPlayerChanged: setIdTmr.start();
    onDeckIdChanged: setIdTmr.start();
    Timer {
        id: setIdTmr
        interval: 1
        onTriggered: {
            gameControl.setDeckId(deck, player+"_"+deckId);
        }
    }
    function cancelSelect() {
        return false;
    }


    function sameColorFamilys(fam) {
        var fams = [];
        if (fam == VCDCard.Hearts || fam == VCDCard.Diamonds) {
            fams.push("Hearts", "Diamonds");
        }
        else {
            fams.push("Clubs", "Spades");
        }
        return fams;
    }

    function otherColorFamilys(fam) {
        var fams = [];
        if (fam == VCDCard.Hearts || fam == VCDCard.Diamonds) {
            fams.push("Clubs", "Spades");
        }
        else {
            fams.push("Hearts", "Diamonds");
        }
        return fams;
    }

    function updateAllowedKeys() {
        if (!acceptDrops) {
            return ["Do not accept drops"];
        }
        var valStrs = [];
        var famStrs = [];
        var famValStrs = [];

        var cardValue = (count === 0) ? "0" : cardRepeater.itemAt(0).value;
        var cardFamily = (count === 0) ? "4" : cardRepeater.itemAt(0).family;

        if (allowedValue === "Same") {
            valStrs.push(cardValue.toFixed(0));
        }
        else if (allowedValue === "OneLess") {
            if (cardValue == 0) cardValue = 14;
            cardValue--;
            valStrs.push(cardValue.toFixed(0));
        }
        else if (allowedValue === "OneMore") {
            cardValue++;
            valStrs.push(cardValue.toFixed(0));
        }
        else if (allowedValue === "Less") {
            if (cardValue == 0) cardValue = 14;
            for (var i=cardValue-1; i>0; --i) {
                valStrs.push(i.toFixed(0));
            }
        }
        else if (allowedValue === "More") {
            for (var i=cardValue+1; i<14; ++i) {
                valStrs.push(i.toFixed(0));
            }
        }
        else if (allowedValue === "SameLess") {
            if (cardValue == 0) cardValue = 14;
            for (var i=cardValue; i>0; --i) {
                valStrs.push(i.toFixed(0));
            }
        }
        else if (allowedValue === "SameMore") {
            for (var i=cardValue; i<14; ++i) {
                valStrs.push(i.toFixed(0));
            }
        }

        if (count > 0) {
            if (allowedFamily === "SameColor") {
                famStrs = sameColorFamilys(cardFamily);
            }
            else if (allowedFamily === "OtherColor") {
                famStrs = otherColorFamilys(cardFamily);
            }
            else if (allowedFamily === "SameFamily") {
                famStrs.push(cardsModel.familyStr(cardFamily));
            }
            else if (allowedFamily === "OtherFamily") {
                var fam = cardsModel.familyStr(cardFamily);
                if (fam != "Hearts") {
                    famStrs.push("Hearts");
                }
                if (fam != "Diamonds") {
                    famStrs.push("Diamonds");
                }
                if (fam != "Spades") {
                    famStrs.push("Spades");
                }
                if (fam != "Clubs") {
                    famStrs.push("Clubs");
                }
            }
        }

        for (var i=0; i<famStrs.length; ++i) {
            for (var j=0; j<valStrs.length; ++j) {
                famValStrs.push(famStrs[i] + "_" + valStrs[j]);
            }
        }

        var newKeys = [];
        if (famValStrs.length > 0) {
            newKeys = famValStrs;
        }
        else if (famStrs.lenght > 0) {
            newKeys = famStrs;
        }
        else {
            newKeys = valStrs;
        }

        if (allowMultiple) {
            // We allow the drop to contain multiple cards so add the "++" variants of the keys
            var keysCount = newKeys.length;
            for (var i=0; i<keysCount; ++i) {
                newKeys.push(newKeys[i]+"++");
            }
        }
        return newKeys;
    }

    function animateFaceUp() {
        if (count > 0) {
            cardRepeater.itemAt(0).animateFaceUp();
        }
    }

    function animateMove(toDeck, fromIndex, toIndex, moveId, cardId, isUndo, isRemote) {
        if (count == 0) {
            console.log("no card to animate");
            return;
        }
        if (typeof isUndo == "undefined") {
            console.log ("isUndo cannot be undefined!!");
            console.trace();
            return;
        }
        if (typeof isRemote == "undefined") {
            console.log ("isRemote cannot be undefined!!");
            console.trace();
            return;
        }
        var toPoint = toDeck.mapToItem(tableItem,0,0);
        var fromPoint = deck.mapToItem(tableItem,0,0);
        var card = cardRepeater.itemAt(fromIndex); // FIXME Check indexes
        card.dAnimation.toDeck = toDeck;
        card.dAnimation.fromX = fromPoint.x + deck.firstCardOffsetX + deck.cardOffsetX * (deck.count - fromIndex);
        card.dAnimation.fromY = fromPoint.y + deck.firstCardOffsetY + deck.cardOffsetY * (deck.count - fromIndex);

        card.dAnimation.toX = toPoint.x + toDeck.firstCardOffsetX + toDeck.cardOffsetX * (toDeck.count - toIndex);
        card.dAnimation.toY = toPoint.y + toDeck.firstCardOffsetY + toDeck.cardOffsetY * (toDeck.count - toIndex);
        card.dAnimation.dropTime = dropDuration(toPoint.x, fromPoint.x, toPoint.y, fromPoint.y);
        card.dAnimation.isUndo = isUndo;
        card.dAnimation.isRemote = isRemote;
        card.dAnimation.fromIndex = fromIndex;
        card.dAnimation.toIndex = toIndex;


        if (toDeck.faceUp != deck.faceUp) {
            card.dAnimation.dropTime = Math.max(200, card.dAnimation.dropTime);
            card.flipTime = card.dAnimation.dropTime;
            card.faceUp = toDeck.faceUp;
        }
        console.log("from:", deckId, "to:", toDeck.deckId, fromIndex, toIndex, isUndo);
        mouseArea.moveAll = false;
        card.dAnimation.start();
    }

    function pullNext() {
        var pullingDeck = undefined;
        var pushingDeck = undefined;

        for (const [pullKey, pullDeck] of Object.entries(tableItem.pullDecks)) {
            for (const [pushKey, pushDeck] of Object.entries(tableItem.pushDecks)) {
                // Here we now have one pulling and one pushing deck
                // Check if there is a match
                var pullKeys = pullDeck.allowedKeys;
                var pushKeys = pushDeck.cardDropKeys(0);

                for (var i=0; i<pullKeys.length; i++) {

                    for (var j=0; j<pushKeys.length; j++) {
                        if (pullKeys[i] === pushKeys[j]) {
                            pullingDeck = pullDeck;
                            pushingDeck = pushDeck;
                            break;
                        }
                    }
                    if (pullingDeck) break;
                }
                if (pullingDeck) break;
            }
            if (pullingDeck) break;
        }
        if (pullingDeck && pushingDeck) {
            var moveId = gameControl.nextMoveId();
            var cardId = pushingDeck.cards.cardId(0);
            pushingDeck.animateMove(pullingDeck, 0, 0, moveId, cardId, false, false);
        }
        else {
            deck.tableItem.pullActive = false;
        }
    }

    signal clicked()
    signal doubleClicked()

    Rectangle {
        anchors {
            top: parent.top
            left: parent.left
        }
        width: tableCardWidth
        height: tableCardHeight
        radius: tableCardWidth * 0.05
        visible: bottomDeck
        color: globalColors.black
        opacity: 0.2
    }

    Rectangle {
        anchors.fill: parent
        anchors.rightMargin: -topCardX
        anchors.bottomMargin: -topCardY
        radius: tableCardWidth * 0.05
        visible: deck.containsDrag
        color: globalColors.black
        opacity: 0.2
        z: 99
    }

    VCDDeckModel {
        id: cardsModel
    }

    function cardDropKeys(index) {
        if (index < 0 || index >= cardRepeater.count) {
            return [];
        }
        var cardValue = cardRepeater.itemAt(index).value;
        var cardFamily = cardRepeater.itemAt(index).family;
        var multiCard = cardRepeater.count > 1 && index > 0 ? "++" : ""
        return [
        cardsModel.familyStr(cardFamily) + multiCard,
        cardsModel.valueStr(cardValue) + multiCard,
        cardsModel.famValStr(cardFamily, cardValue) + multiCard
        ];
    }

    Item {
        id: dragAllItem
        property string name: "container"
        property variant dAnimation: moveAllAnim
        anchors {
            top: deck.top
            left: deck.left
        }

        width: deck.width
        height: deck.height

        //Rectangle { anchors.fill: parent; color: "blue"; opacity: 0.2}
        property variant dropKeys: count == 0 ? [] : cardDropKeys(count -1);

        Drag.active: mouseArea.drag.active && mouseArea.moveAll
        Drag.hotSpot.x: firstCardOffsetX + width/2
        Drag.hotSpot.y: firstCardOffsetY + width*1.45/2
        Drag.keys: dropKeys
        transformOrigin: Item.Center

        states: [
        State {
            name: "item-draging"
            when: (moveAllAnim.running || mouseArea.draging || mouseArea.pressed) && mouseArea.moveAll
            ParentChange { target: dragAllItem; parent: deck.tableItem }
            AnchorChanges { target: dragAllItem; anchors.top : undefined; anchors.horizontalCenter: undefined }
            PropertyChanges { target: dragAllItem; z: 100 }
            PropertyChanges { target: dragAllItem; scale: 1.04; }
            //PropertyChanges { target: mouseArea; visible: false }
        }
        ]

        SequentialAnimation {
            id: moveAllAnim
            property variant toDeck
            property double fromX
            property double fromY
            property double toX
            property double toY
            property double dropTime: 200
            property bool isUndo: false
            property bool isRemote: false

            ScriptAction { script: { moveAllAnim.toDeck.tableItem.animating = true; } }
            ParallelAnimation {
                NumberAnimation { target: dragAllItem; property: "x"; from: moveAllAnim.fromX; to: moveAllAnim.toX; duration: moveAllAnim.dropTime }
                NumberAnimation { target: dragAllItem; property: "y"; from: moveAllAnim.fromY; to: moveAllAnim.toY; duration: moveAllAnim.dropTime; easing.type: Easing.OutQuad }
            }
            ScriptAction {
                script: {
                    if (moveAllAnim.toDeck != deck) {
                        //console.log("moveAllAnim", moveAllAnim.toDeck.count);
                        var moveId = gameControl.nextMoveId();
                        var origCount = moveAllAnim.toDeck.count;
                        while (cards.rowCount() > 0) {
                            //console.log(moveAllAnim.toDeck.count, moveAllAnim.toDeck.count-origCount);
                            gameControl.moveCardFromTo(deck, moveAllAnim.toDeck, 0, moveAllAnim.toDeck.count-origCount, moveId, deck.cards.cardId(0), false);
                            cards.moveCardTo(moveAllAnim.toDeck.cards, 0, moveAllAnim.toDeck.count-origCount);
                        }
                    }
                    moveAllAnim.isUndo = false;
                    moveAllAnim.isRemote = false;
                    mouseArea.moveAll = false;
                    moveAllAnim.toDeck.tableItem.animating = false;
                }
            }
        }
        Repeater {
            id: cardRepeater
            model: cardsModel
            delegate: Card {
                id: card
                property string name: "card"
                property variant dAnimation: moveCardAnim
                property int mIndex: model.index
                property int cardId: model.cardId
                anchors {
                    left: dragAllItem.left
                    leftMargin: (cardRepeater.count - model.index - 1) * cardOffsetX + firstCardOffsetX
                    top: dragAllItem.top
                    topMargin: (cardRepeater.count - model.index - 1) * cardOffsetY + firstCardOffsetY
                }
                width: tableCardWidth
                height: tableCardHeight

                value: model.value
                family: model.family
                faceUp: deck.faceUp

                Drag.active: mouseArea.drag.active && !mouseArea.moveAll && model.index === 0
                Drag.hotSpot.x: width/2
                Drag.hotSpot.y: height/2
                Drag.keys: [cardsModel.familyStr(family), cardsModel.valueStr(value), cardsModel.famValStr(family, value)]

                z: cardRepeater.count - model.index

                transformOrigin: Item.Center

                states: [
                State {
                    name: "drag card"
                    when: (moveCardAnim.running || mouseArea.draging || mouseArea.pressed) &&
                    (mIndex === 0 || (mIndex != -1 && mIndex <= moveCardAnim.fromIndex) ) &&
                    !mouseArea.moveAll

                    ParentChange { target: card; parent: tableItem }
                    AnchorChanges { target: card; anchors.top : undefined; anchors.horizontalCenter: undefined }
                    PropertyChanges { target: card; z: cardRepeater.count - model.index + 100 }
                    PropertyChanges { target: card; scale: 1.04; }
                    //PropertyChanges { target: mouseArea; visible: false }
                }
                ]

                SequentialAnimation {
                    id: moveCardAnim
                    property variant toDeck
                    property double fromX
                    property double fromY
                    property double toX
                    property double toY
                    property double dropTime: 200
                    property int fromIndex: -1
                    property int toIndex: -1
                    property bool isUndo: false
                    property bool isRemote: false

                    ScriptAction { script: { moveCardAnim.toDeck.tableItem.animating = true; } }
                    ParallelAnimation {
                        NumberAnimation { target: card; property: "x"; from: moveCardAnim.fromX; to: moveCardAnim.toX; duration: moveCardAnim.dropTime }
                        NumberAnimation { target: card; property: "y"; from: moveCardAnim.fromY; to: moveCardAnim.toY; duration: moveCardAnim.dropTime; easing.type: Easing.OutQuad }
                    }
                    ScriptAction {
                        script: {
                            //console.log(card.state, "start moveCardAnim script");
                            //console.log(moveCardAnim.running || mouseArea.draging || mouseArea.pressed);
                            //console.log(mIndex === 0 || (mIndex != -1 && mIndex <= moveCardAnim.fromIndex) );
                            //console.log(!mouseArea.moveAll);
                            if (moveCardAnim.toDeck != deck) {
                                if (moveCardAnim.toIndex !== -1 && moveCardAnim.fromIndex !== -1) {
                                    //console.log("To-From", moveCardAnim.isUndo);
                                    if (moveCardAnim.isUndo) {
                                        gameControl.moveUndone();
                                    }
                                    else {
                                        gameControl.moveCardFromTo(deck, moveCardAnim.toDeck, moveCardAnim.fromIndex, moveCardAnim.toIndex, gameControl.nextMoveId(), card.cardId, moveCardAnim.isRemote);
                                    }
                                    // NOTE: gameControl.moveCardFromTo needs to be called before as
                                    // this "card" is deleted as a consequence of moveCardTo
                                    cards.moveCardTo(moveCardAnim.toDeck.cards, moveCardAnim.fromIndex, moveCardAnim.toIndex);
                                }
                                else if (typeof moveCardAnim.toDeck.insertIndex !== "undefined") {
                                    if (moveCardAnim.isUndo) {
                                        console.log("toDeckInsertIndex!!!!!!!!!!!!!!!!!", moveCardAnim.toDeck, deck);
                                    }
                                    gameControl.moveCardFromTo(deck, moveCardAnim.toDeck, 0, moveCardAnim.toDeck.insertIndex, gameControl.nextMoveId(), card.cardId, moveCardAnim.isRemote);
                                    cards.moveCardTo(moveCardAnim.toDeck.cards, 0, moveCardAnim.toDeck.insertIndex);
                                }
                                else {
                                    if (moveCardAnim.isUndo) {
                                        console.log("else!!!!!!!!!!!!!!!!!", moveCardAnim.toDeck, deck);
                                    }
                                    gameControl.moveCardFromTo(deck, moveCardAnim.toDeck, 0, 0, gameControl.nextMoveId(), card.cardId, moveCardAnim.isRemote);
                                    cards.moveCardTo(moveCardAnim.toDeck.cards, 0, 0);
                                }
                            }
                            moveCardAnim.fromIndex = -1;
                            moveCardAnim.toIndex = -1;
                            moveCardAnim.isUndo = false;

                            if (moveCardAnim.toDeck.tableItem.pullActive === true) {
                                moveCardAnim.toDeck.startPullNext();
                            }
                            moveCardAnim.toDeck.tableItem.animating = false;
                        }
                    }
                }
            }
        }
    }

    function startPullNext() { pullNextTmr.start(); }

    Timer {
        id: pullNextTmr
        interval: 1
        onTriggered: {
            if (deck.tableItem.pullActive === true) {
                deck.pullNext();
            }
        }
    }

    function dropDuration(x1, x2, y1, y2) {
        var xDist = Math.abs(x1 - x2);
        var yDist = Math.abs(y1 - y2);
        return Math.min(Math.max(xDist, yDist),150);
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        property bool moveAll: false
        property bool draging: false
        readonly property bool dragActive: drag.active

        property int oldX: 0
        property int oldY: 0
        property int velocity: 0

        onPositionChanged: {
            var xDiff = mouse.x - oldX;
            var yDiff = mouse.y - oldY;
            velocity = Math.sqrt(xDiff*xDiff + yDiff*yDiff);
            velocityTmr.start();
        }

        Timer {
            id: velocityTmr
            interval: 100
            onTriggered: {
                mouseArea.velocity = 0;
            }
        }

        onPressed: {
            console.log(deck.player+"_"+deck.deckId);
            if (cardRepeater.count == 0) {
                mouseArea.drag.target = undefined;
                return;
            }

            if ((mouse.y < cardRepeater.itemAt(0).y ||
                mouse.y > (cardRepeater.itemAt(0).y + cardRepeater.itemAt(0).height) ||
                deck.alwaysMoveAll) && !deck.neverMoveAll)
            {
                moveAll = true;
            }
            else {
                moveAll = false;
            }
            mouseArea.drag.target = moveAll ? dragAllItem : cardRepeater.itemAt(0);
            oldX = mouse.x;
            oldY = mouse.y;
            velocity = 0;
        }
        onDragActiveChanged: {
            console.log("onDragActiveChanged");
            if (dragActive) { draging = true; }
        }

        onCanceled: {
            console.log("onCanceled");
            if (draging) {
                animateDropBackToOrigin(drag.target, deck, moveAll);
            }
        }

        function animateDropToOtherDeck(dragItem, toDeck, moveAll) {
            var toPoint = toDeck.mapToItem(tableItem,0,0);
            dragItem.dAnimation.toDeck = toDeck;
            dragItem.dAnimation.fromX = dragItem.x;
            dragItem.dAnimation.fromY = dragItem.y;
            dragItem.dAnimation.toX = toPoint.x;
            dragItem.dAnimation.toY = toPoint.y + toDeck.topCardY;
            dragItem.dAnimation.dropTime = dropDuration(toDeck.x, dragItem.x, toDeck.y, dragItem.y);

            if (dragItem.faceUp != toDeck.faceUp && !moveAll) {
                dragItem.dAnimation.dropTime = Math.max(200, dragItem.dAnimation.dropTime);
                dragItem.flipTime = dragItem.dAnimation.dropTime;
                dragItem.faceUp = toDeck.faceUp;
            }
            dragItem.dAnimation.isUndo = false;
            dragItem.dAnimation.start();
        }

        function animateDropBackToOrigin(dragItem, toDeck, moveAll) {
            var topMagin = moveAll ? 0 : toDeck.topCardY - cardOffsetY;
            var toPoint = toDeck.mapToItem(tableItem, 0, 0);
            dragItem.dAnimation.toDeck = toDeck;
            dragItem.dAnimation.fromX = dragItem.x;
            dragItem.dAnimation.fromY = dragItem.y;
            dragItem.dAnimation.toX = toPoint.x;
            dragItem.dAnimation.toY = toPoint.y + topMagin
            dragItem.dAnimation.dropTime = dropDuration(toDeck.x, dragItem.x, toDeck.y, dragItem.y);
            dragItem.dAnimation.isUndo = false;
            dragItem.dAnimation.start();
        }

        onReleased: {
            console.log("onRelease");
            if (draging) {
                var dragItem = drag.target;
                var targetDeck = dragItem.Drag.target;

                console.log("onReleased: dragging");
                if (dragItem && targetDeck && targetDeck != deck) {
                    console.log("toDropDeck");
                    // Dropping on another deck
                    animateDropToOtherDeck(dragItem, targetDeck, moveAll);
                }
                else if (dragItem) {
                    console.log("check polling deck", deck.cardWidth*0.1, velocity);
                    var pullingDeck = undefined;
                    if (velocity > tableCardWidth*0.1) {
                        for (const [key, pullDeck] of Object.entries(tableItem.pullDecks)) {
                            for (var i=0; i<pullDeck.allowedKeys.length; i++) {
                                for (var j=0; j<dragItem.Drag.keys.length; j++) {
                                    if (pullDeck.allowedKeys[i] === dragItem.Drag.keys[j]) {
                                        pullingDeck = pullDeck;
                                        break;
                                    }
                                }
                                if (pullingDeck) break;
                            }
                            if (pullingDeck) break;
                        }
                    }
                    console.log("Pulling deck", pullingDeck);
                    if (pullingDeck) {
                        deck.tableItem.pullActive = true;
                        console.log("pullActive:", deck.tableItem.pullActive);
                        animateDropToOtherDeck(dragItem, pullingDeck, moveAll);
                    }
                    else {
                        // Drop the card(s) back to where they came from
                        animateDropBackToOrigin(dragItem, deck, moveAll);
                    }
                }
                else {
                    console.log("Is this even possible?");
                }
            }
            mouseArea.drag.target = undefined;
            mouseArea.draging = false;
        }

        onDoubleClicked: deck.doubleClicked();
        onClicked: deck.clicked();
    }

    Component.onDestruction: {
        // This will clear the pull decks multiple times :(
        deck.tableItem.pullDecks = new Object();
        deck.tableItem.pushDecks = new Object();
    }

}

