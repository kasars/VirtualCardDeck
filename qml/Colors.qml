import QtQuick
import QtQuick.Window

Item {
    Timer {
        id: tmr
        interval: 100
        running: true
        repeat: false
    }

    readonly property color white: tmr.runnig ? "#FFFFFF" : "#FFFFFF"
    readonly property color lightGray: tmr.runnig ? "#CCCCCC" : "#CCCCCC"
    readonly property color black: tmr.runnig ? "#000000" : "#000000"

    readonly property color table: tmr.runnig ? "green" : "green"
    readonly property color transparent: tmr.runnig ? "#00000000" : "#00000000"
    readonly property color pressShade: tmr.runnig ? "#55555555" : "#55555555"

    readonly property color cardBg: tmr.runnig ? "white" : "white"
    readonly property color cardBorder: tmr.runnig ? "black" : "black"

    readonly property color dialogShadow: tmr.runnig ? "black" : "black"
    readonly property color dialogShield: tmr.runnig ? "black" : "black"
    readonly property color dialogText: tmr.runnig ? "white" : "white"
    readonly property color dialogTextOutline: tmr.runnig ? "black" : "black"
    readonly property color dialogBg: tmr.runnig ? "#EE003300" : "#EE003300"

    readonly property color editActive: tmr.runnig ? "#88EEFF" : "#88EEFF"
    readonly property color editInActive: tmr.runnig ? "white" : "white"
    readonly property color editBg: tmr.runnig ? "#33FFFFFF" : "#33FFFFFF"
    readonly property color editText: tmr.runnig ? "#FFFFFF" : "#FFFFFF"

}
