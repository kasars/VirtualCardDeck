import QtQuick

Rectangle {
    id: tabBar
    color: globalColors.table
    property variant tabModel

    property alias currentIndex: tabView.currentIndex

    ListView {
        id: tabView
        anchors.fill: parent
        model: tabModel
        interactive: false
        orientation: ListView.Horizontal
        delegate: Rectangle {
            height: tabBar.height
            width: tabBar.width / tabView.count
            radius: pageMargins
            border.width: 1
            border.color: globalColors.black
            color: ListView.isCurrentItem ? globalColors.table : globalColors.transparent
            Text {
                anchors.fill: parent
                font.pixelSize: fontPxSizeM
                font.bold: true
                color: model.index === tabView.currentIndex ? globalColors.white : globalColors.lightGray
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: modelData
            }

            MouseArea {
                id: mArea
                anchors.fill: parent
                onClicked: tabView.currentIndex = model.index
            }

            Rectangle {
                anchors.fill: parent
                color: globalColors.pressShade
                visible: mArea.pressed
            }
            Rectangle {
                anchors {
                    bottom: parent.bottom
                    left: parent.left
                    right: parent.right
                }
                height: pageMargins+1
                color: globalColors.black
                visible: model.index != tabView.currentIndex
            }
        }
    }

    Rectangle {
        anchors {
            left: parent.left
            right: parent.right
            bottom:parent.bottom
        }
        height: pageMargins
        color: globalColors.table
    }
}

