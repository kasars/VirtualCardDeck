import QtQuick
import VirtualCardDeck

Item {
    id: cardList
    property variant cards: QtObject{}
    readonly property int count: listView.count
    readonly property double cardH: cardW * 1.45
    readonly property double cardW: cardList.width

    property bool faceUp: true

    signal cardSelected(int index);

    SortProxyModel {
        id: sortedModel
        srcModel: cards
        sortRole: SortProxyModel.IndexRole
        sortAscending: false
    }

    Rectangle {
        anchors.fill: parent
        color :"#555555"
        opacity: 0.3
    }
    ListView {
        id: listView
        anchors.fill: parent
        model: typeof sortedModel !== "undefined" ? sortedModel : 0

        add: Transition {
            NumberAnimation { properties: "x,y"; duration: 200; easing.type: Easing.OutBounce }
        }
        displaced: Transition {
            NumberAnimation { properties: "x,y"; duration: 200; easing.type: Easing.OutBounce }
        }

        delegate: Item {
            id: delegateRoot

            z: model.index
            // This is to used to identify the source (card from other deck or card from this deck)
            property int handIndex: model.index
            width: listView.width
            height: cardH / 2

            Card {
                id: card
                width: cardW
                height: cardH
                anchors {
                    left: parent.left;
                    top: parent.top
                }
                value: model.value
                family: model.family
                faceUp: cardList.faceUp

                MouseArea {
                    id:mouseArea
                    anchors.fill: parent
                    onClicked: cardList.cardSelected(model.index);
                }
            }
            Rectangle {
                anchors.fill: card
                color: globalColors.pressShade
                visible: mouseArea.pressed
            }
        }
    }
}
