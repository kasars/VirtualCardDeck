import QtQuick
import VirtualCardDeck

Item {
    id: selectGamePage

    signal gameSelected(string gameName, string confFile, string numPlayers)
    signal cancelClicked();

    property variant gamesModel
    property bool multiplayer: true

    FilterProxyModel {
        id: filteredGamesModel
        srcModel: gamesModel
        filterRole: VCDGamesModel.NumPlayersRole
        passFilters: !multiplayer ? [ 1 ] : [ tabBar.currentIndex + 2 ]
    }

    TabBar {
        id: tabBar
        tabModel: [ "Two players" , "Three players", "Four players" ]
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        height: parent.height * 0.2
        visible: multiplayer
    }

    GridView {
        id: gameListView

        anchors {
            top: multiplayer ? tabBar.bottom : parent.top
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: pageMargins
            topMargin: multiplayer ? 0 : undefined
            rightMargin: 0
        }
        model: filteredGamesModel

        cellHeight: selectGamePage.height * 0.22
        property int divisor: Math.max(Math.floor(gameListView.width / (cellHeight * 3.5)), 1)
        cellWidth: gameListView.width / divisor

        interactive: contentHeight > height

        delegate: Rectangle {
            width: gameListView.cellWidth - pageMargins
            height: gameListView.cellHeight - pageMargins
            radius: pageMargins

            color: globalColors.cardBg
            border.width: 1
            border.color: globalColors.cardBorder

            Image {
                id: iconItem
                anchors {
                    top: parent.top
                    bottom:parent.bottom
                    left: parent.left
                    margins: pageMargins
                }
                fillMode: Image.PreserveAspectFit
                source: "../Images/" + model.iconName
            }

            Text {
                anchors {
                    top: parent.top
                    left: iconItem.right
                    right: parent.right
                    bottom: parent.bottom
                    margins: pageMargins * 2
                }
                font.pixelSize: fontPxSizeM
                //horizontalAlignment: Text.AlignHCenter
                verticalAlignment:  Text.AlignVCenter
                text: model.gameName
                maximumLineCount: 2
                wrapMode: Text.Wrap
                elide: Text.ElideRight
            }

            Rectangle {
                anchors.fill: parent
                color: parent.color
                opacity: 0.3
                visible: mArea.pressed
                radius: parent.radius
            }
            MouseArea {
                id: mArea
                anchors.fill: parent
                onClicked: {
                    gameSelected(model.gameName, model.fileName, model.numPlayers);
                }
            }
        }
    }

    Keys.onPressed: (event)=> {
        if ((event.key === Qt.Key_Back || event.key === Qt.Key_Escape) && selectGamePage.visible) {
            event.accepted = true;
        }
    }
    Keys.onReleased: (event)=> {
        if ((event.key === Qt.Key_Back || event.key === Qt.Key_Escape) && selectGamePage.visible) {
            selectGamePage.cancelClicked();
            event.accepted = true;
        }
    }
}

