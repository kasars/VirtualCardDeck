import QtQuick
import VirtualCardDeck

Item {
    id: flipCard
    width: 100
    height: 150

    property int family: 1
    property int value: 1
    property bool faceUp: true

    property int flipTime: 0
    property int closedAngle: -180

    function animateFaceUp(reverse) {
        closedAngle = reverse ? -180 : 180
        flipUpAnim.start();
    }

    function animateFaceDown(reverse) {
        closedAngle = reverse ? -180 : 180
        flipDownAnim.start();
    }

    NumberAnimation { id: flipUpAnim; target: rotTransf; property: "angle"; from: closedAngle; to: 0; duration: 150; }
    NumberAnimation { id: flipDownAnim; target: rotTransf; property: "angle"; from: 0; to: closedAngle; duration: 150; }


    function valueString(value) {
        var vString;
        switch (value) {
            case VCDCard.Ace:
                vString = "A";
                break;
            case VCDCard.Two:
                vString = "2";
                break;
            case VCDCard.Three:
                vString = "3";
                break;
            case VCDCard.Four:
                vString = "4";
                break;
            case VCDCard.Five:
                vString = "5";
                break;
            case VCDCard.Six:
                vString = "6";
                break;
            case VCDCard.Seven:
                vString = "7";
                break;
            case VCDCard.Eight:
                vString = "8";
                break;
            case VCDCard.Nine:
                vString = "9";
                break;
            case VCDCard.Ten:
                vString = "10";
                break;
            case VCDCard.Jack:
                vString = "J";
                break;
            case VCDCard.Queen:
                vString = "Q";
                break;
            case VCDCard.King:
                vString = "K";
                break;
            case VCDCard.Joker:
                vString = "Joker";
                break;
        }
        return vString;
    }

    function familyString(family) {
        var fString;
        switch (family) {
            case VCDCard.Hearts:
                fString = "H";
                break;
            case VCDCard.Diamonds:
                fString = "D";
                break;
            case VCDCard.Spades:
                fString = "S";
                break;
            case VCDCard.Clubs:
                fString = "C";
                break;
        }
        return fString;
    }

    Flipable {
        id: fliper
        anchors.fill: parent

        transform: Rotation {
            id: rotTransf
            origin.x: flipCard.width/2
            origin.y: flipCard.height/2
            axis.x: 0; axis.y: 1; axis.z: 0     // set axis.y to 1 to rotate around y-axis
            angle: 0    // the default angle
        }

        states: State {
            name: "back"
            PropertyChanges { target: rotTransf; angle: closedAngle }
            when: !flipCard.faceUp
        }

        transitions: Transition {
            NumberAnimation { target: rotTransf; property: "angle"; duration: flipTime }
        }


        front: Rectangle {
            width: flipCard.width
            height: flipCard.height
            radius: width * 0.05

            border.width: Math.max(width * 0.007, 1)
            border.color: globalColors.cardBorder
            color: globalColors.cardBg

            Image {
                anchors {
                    fill: parent
                    margins: flipCard.width * 0.04
                }
                source: "../Cards-svg/" + valueString(value) + familyString(flipCard.family) +".svg"
            }
        }

        back: Rectangle {
            width: flipCard.width
            height: flipCard.height
            radius: width * 0.05

            border.width: Math.max(width * 0.007, 1)
            border.color: globalColors.cardBorder
            color: globalColors.cardBg

            Image {
                anchors {
                    fill: parent
                    margins: flipCard.width * 0.1
                }
                source: "../Cards-svg/Card_Back_Red.svg"
            }
        }
    }
}
