import QtQuick
import VirtualCardDeck

Item {
    id: group
    width: (tableCardWidth + deckSpacing) * availableColumns + pageMargins
    height: (tableCardHeight + deckSpacing) * rows + pageMargins

    property variant table: parent

    property variant model: []
    property variant dealDeck
    property int columns:1
    property int availableColumns: columns
    property int paddingColumns: (availableColumns - columns) / 2
    property int rows:1

    property string strId: ""


    property bool otherPlayer: false

    readonly property int deckCount: deckRepeater.count

    Repeater {
        id: deckRepeater
        model: group.model

        Item {
            id: cellItem
            width: tableCardWidth + (tableCardWidth + deckSpacing) * (model.columnSpan-1)
            height: tableCardHeight + (tableCardHeight + deckSpacing) * (model.rowSpan-1)

            x: pageMargins + (tableCardWidth + deckSpacing) * (model.column + paddingColumns)
            y: pageMargins + (tableCardHeight + deckSpacing) * rowIndex

            property int rowIndex:  otherPlayer ? rows - model.row - 1 : model.row

            property variant loader: loaderItem

            Loader {
                id: loaderItem
                anchors {
                    left: parent.left
                    top: parent.top
                }
                source: model.type + ".qml"

                property int dealCount: model.dealCount
                property int topDealCount: model.topDealCount

                onLoaded: {
                    item.width = Qt.binding(function() { return cellItem.width; });
                    item.height = Qt.binding(function() { return cellItem.height; });
                    if (typeof item.faceUp !== "undefined") {
                        item.faceUp = faceUp;
                    }
                    item.tableItem = table;

                    if (typeof item.allowedValue !== "undefined") {
                        item.allowedValue = model.allowedValue;
                    }
                    if (typeof item.allowedFamily !== "undefined") {
                        item.allowedFamily = model.allowedFamily;
                    }
                    if (typeof item.allowMultiple !== "undefined") {
                        item.allowMultiple = model.allowMultiple;
                    }
                    if (model.pullAllowed) {
                        // We use an Object here in stead of an Array because for some reason we get two instances of every deck
                        // (count is changed to the same value)
                        table.pullDecks[model.player+model.index] = item;
                    }
                    if (model.allowAutoPull) {
                        if (typeof item.topDeck !== undefined) {
                            table.pushDecks[model.player+model.index] = item.topDeck;
                        }
                        else {
                            table.pushDecks[model.player+model.index] = item;
                        }
                    }

                    if (typeof item.tableCardWidth !== "undefined") {
                        item.tableCardWidth = Qt.binding(function() { return group.tableCardWidth; });
                    }
                    if (typeof item.maxHeight !== "undefined") {
                        item.maxHeight = Qt.binding(function() { return cellItem.height; });
                    }
                    if (typeof item.maxWidth !== "undefined") {
                        item.maxWidth = Qt.binding(function() { return cellItem.width; });
                    }
                    if (typeof item.dealCount !== "undefined") {
                        item.dealCount = Qt.binding(function() { return model.dealCount; });
                    }
                    if (typeof item.otherPlayer !== "undefined") {
                        item.otherPlayer = Qt.binding(function() { return group.otherPlayer; });
                    }
                    if (typeof item.player !== "undefined") {
                        item.player = Qt.binding(function() { return model.player; });
                    }
                    if (typeof item.deckId !== "undefined") {
                        item.deckId = Qt.binding(function() { return model.index; });
                    }
                    //console.log(loaderItem.source, model.player, columns, rows, cellItem.x, tableCardWidth, cellItem.width, item.width, item.height);
                    var src = loaderItem.source + "";
                    //console.log("source", src, typeof(src));
                    if (src.indexOf("Hand.qml") !== -1 && !otherPlayer) {
                        cellItem.x = pageMargins + (tableCardWidth + deckSpacing) * (model.column + paddingColumns - 1)
                        cellItem.width += (tableCardWidth + deckSpacing) * 2
                        console.log(model.player, columns, rows, cellItem.x, tableCardWidth, cellItem.width, item.width, item.height);
                    }
                }
            }
        }
    }

    function deleteCards() {
        for (var i=0; i<deckRepeater.count; ++i) {
            var item = deckRepeater.itemAt(i).loader.item;
            item.cards.deleteCards();
            if (typeof item.topCards != "undefined") {
                item.topCards.deleteCards();
            }
            if (typeof item.moveCards != "undefined") {
                item.moveCards.deleteCards();
            }
        }
    }

    function dealOutCards() {
        //console.log(strId);
        //console.log(deckRepeater.count, "wh:", group.width, group.height, "xy:", group.x, group.y, "rc:", group.rows, group.columns);
        //console.log("xy:", mapToItem(deckTable, group.x, group.y));
        for (var i=0; i<deckRepeater.count; ++i) {
            var deck = deckRepeater.itemAt(i).loader.item;
            var loader = deckRepeater.itemAt(i).loader;
            //console.log(deck.player+"_"+deck.deckId, mapToItem(deckTable, loader.x, loader.y), loader.visible, deckRepeater.itemAt(i).visible, deckRepeater.visible, group.visible);

            if (loader.dealCount === -1) {
                // This is the deck that should get the rest of the cards later
                continue;
            }
            if (typeof deck.cards != "undefined" && loader.dealCount > 0) {
                for (var j=0; j<loader.dealCount; ++j) {
                    dealDeck.moveCardTo(deck.cards, [0], [0]);
                }
            }
            if (typeof deck.topCards != "undefined" && loader.topDealCount > 0) {
                for (var j=0; j<loader.topDealCount; ++j) {
                    dealDeck.moveCardTo(deck.topCards, [0], [0]);
                }
            }
        }
    }

    function dealRest() {
        for (var i=0; i<deckRepeater.count; ++i) {
            if (deckRepeater.itemAt(i).loader.dealCount === -1) {
                var deck = deckRepeater.itemAt(i).loader.item;
                while (dealDeck.rowCount() > 0) {
                    dealDeck.moveCardTo(deck.cards, [dealDeck.rowCount()-1] , [0]);
                }
            }
        }
    }

    function cancelSelect() {
        for (var i=0; i<deckRepeater.count; ++i) {
            //console.log(deckRepeater.itemAt(i).loader.item);
            if (deckRepeater.itemAt(i).loader.item.cancelSelect()) {
                return true;
            }
        }
        return false;
    }

}

