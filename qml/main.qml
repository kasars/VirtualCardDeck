import QtQuick
import QtCore
import QtQuick.Window
import VirtualCardDeck

Item {
    id: window
    width: 900
    height: 480
    // title: qsTr("Virtual Card Table")
    // visibility: Qt.platform.os === "android" ? "FullScreen" : "AutomaticVisibility"
    // flags: Qt.Window | (Qt.platform.os === "android" ? Qt.MaximizeUsingFullscreenGeometryHint : 0)

    Colors {
        id: globalColors
    }

    readonly property int deckSpacing: window.height * 0.03
    readonly property int pageMargins: window.height * 0.02
    readonly property double fontPxSizeL: window.height * 0.06
    readonly property double fontPxSizeM: window.height * 0.05
    readonly property double fontPxSizeS: window.height * 0.04

    readonly property int notSelectedMode: 0
    readonly property int singlePlayerMode: 1
    readonly property int multiPlayerMode: 2

    property int playMode: gameControl.multiplayer ? notSelectedMode : singlePlayerMode

    function cancelGame() {
        comModel.waitForGame();
        if (gameControl.multiplayer) {
            playMode = notSelectedMode;
        }
        else {
            playMode = singlePlayerMode;
            comModel.hostGame("", "", 0);
        }
    }

    function startNewGame() { startGameTmr.start(); }
    Timer {
        id: startGameTmr
        interval: 1
        onTriggered: w4players.startGame();
    }

    function openQuitDialog() {
        dialog.openDialog(qsTr("Quit the game and return to the main menu?"), dialog.yesButton | dialog.noButton, quitGameCbItem);
    }
    function openNewDealDialog() {
        dialog.openDialog(qsTr("Deal a new game?"), dialog.yesButton | dialog.noButton, newGameCbItem);
    }

    Item {
        anchors.fill: parent

        Rectangle { id: bgRect; anchors.fill: parent; color: globalColors.table }

        SelectHostPage {
            id: hostListView
            anchors.fill: parent
            visible: !comModel.waitingForStart && !comModel.gameStarted && playMode === multiPlayerMode
            onHostSelected: comModel.joinGame(hostId)
            onCancelClicked: cancelGame()
            onVisibleChanged: {
                if (visible) {
                    forceActiveFocus()
                }
            }
        }

        VCDGamesModel { id: allGamesModel }

        SelectGamePage {
            anchors.fill: parent
            visible: comModel.isServer && table.model.gameName === "" && !comModel.gameStarted && playMode !== notSelectedMode
            gamesModel: allGamesModel
            multiplayer: playMode == multiPlayerMode
            onGameSelected: (gameName, confFile, numPlayers) => {
                comModel.hostGame(table.model.gameName, confFile, numPlayers);
                table.model.setConfigFile(confFile);
            }
            onCancelClicked: cancelGame();
            onVisibleChanged: {
                if (visible) {
                    forceActiveFocus()
                }
            }
        }

        WaitForPlayersPage {
            id: w4players
            anchors.fill: parent
            visible: comModel.waitingForStart && table.model.gameName !== "" && !comModel.gameStarted
            onCancelClicked: openQuitDialog()

            onVisibleChanged: {
                if (visible) {
                    forceActiveFocus();
                    if (table.model.playerCount === 1) {
                        startNewGame();
                    }
                }
            }
            opacity: visible ? 1.0 : 0
            Behavior on opacity {
                NumberAnimation { duration: 150 }
            }

            function startGame() {
                table.dealDeck.newDeck();
                table.dealDeck.shuffle();
                comModel.sendCards(table.dealDeck.cardsJson());
                comModel.startGame();
            }
            CardDialog {
                id: startDialog
                anchors.fill: parent
                property bool showStart: comModel.readyToStart && !comModel.gameStarted && gameControl.multiplayer
                onShowStartChanged: {
                    state = showStart ? "open" : "closed";
                }
                text: qsTr("Start Game?");
                buttons: startDialog.yesButton | startDialog.cancelButton
                onAccepted: w4players.startGame()
                onCanceled: cancelGame()
            }
        }

        SelectSingleMultiPage {
            anchors.fill: parent
            visible: playMode === notSelectedMode

            onSinglePlayerClicked: {
                playMode = singlePlayerMode;
                comModel.hostGame("", "", 0);
            }
            onHostGameClicked: {
                playMode = multiPlayerMode;
                comModel.hostGame("", "", 0);
            }
            onJoinGameClicked: {
                playMode = multiPlayerMode;
            }
        }

        CardTable {
            id: table
            anchors.centerIn: parent
            width: rotation === 0 ? parent.width : parent.height
            height: rotation === 0 ? parent.height : parent.width
            rotation: landscape ? 0 : Qt.platform.os == "android" ? 270 : 0
            focus: true

            visible: comModel.gameStarted
            onVisibleChanged: {
                if (visible) {
                    forceActiveFocus()
                }
            }

            function startGame() {
                table.deleteCards();
                table.dealCards();
                gameControl.clearMoves();
            }
        }

        SideMenu {
            id: actionMenu
            anchors.fill: parent
            sourceComponent:  ActionMenu {
                id: actionMenuContent
                quitVisible: !gameControl.multiplayer ? comModel.gameStarted : playMode !== notSelectedMode;
                reloadVisible: comModel.gameStarted && (comModel.isServer || playMode === singlePlayerMode)
                undoVisible: comModel.gameStarted
                setPlayerVisible: !comModel.gameStarted && playMode !== singlePlayerMode

                onQuitGame: { openQuitDialog(); actionMenu.closeMenu();}
                onQuitGameConfirmed: { actionMenu.closeMenu(); cancelGame();}
                onReloadGame: { openNewDealDialog();  actionMenu.closeMenu(); }
                onReloadGameConfirmed: { actionMenu.closeMenu(); startNewGame(); }
                onUndoMove: { gameControl.undoLast(); }
                onSetPlayerName: {
                    playerDialog.openDialog("", playerDialog.okButton | playerDialog.cancelButton, nameCbItem);
                    playerDialog.item.text = comModel.playerName;
                    actionMenu.closeMenu();
                }
            }
            leftMenu: true
            menuWidth: parent.width * 0.3
            state: showMenuInfo && visible ? "open" : "closed"
            onStateChanged: {
                if (state === "open") {
                    table.cancelSelects();
                }
            }

            visible: !item.isEmpty
            showMenuInfo: true
            onMenuInfoAcknowledged: {
                showMenuInfo = false;
                state = "closed";
            }

            onVisibleChanged: {
                if (visible && showMenuInfo) {
                    state = "open";
                }
            }
        }

        Item { id: quitGameCbItem; function accepted() { cancelGame(); } }
        Item { id: newGameCbItem;  function accepted() { startNewGame(); } }
        Item { id: nameCbItem;     function accepted() { comModel.playerName = playerDialog.item.text; } }

        CardDialog {
            id: dialog
            anchors.fill: parent
        }

        CardDialog {
            id: playerDialog
            anchors.fill: parent
            sourceComponent:  PlayerNameInput {}
        }

        Component.onCompleted: {
            if (!gameControl.multiplayer) {
                playMode = singlePlayerMode;
                comModel.hostGame("", "", 0);
            }
            else if (comModel.playerName === "Player NN") {
                playerDialog.openDialog("", playerDialog.okButton | playerDialog.cancelButton, nameCbItem);
            }
        }

        Item {
            id: preLoadCardsLoaderItem
            anchors.centerIn: parent
            Repeater {
                id: cardRep
                model: 52
                Loader {
                    id: cardLoader
                    source: lazyLoadTmr.loadedCards > model.index ? "Card.qml" : ""
                    onLoaded: {
                        item.value = model.index%13+1;
                        item.family = (model.index/13);
                        lazyLoadTmr.start();
                    }
                }
            }

            Timer {
                id: lazyLoadTmr
                interval: 1
                property int loadedCards: 1
                onTriggered: lazyLoadTmr.loadedCards++;
            }
            opacity: 0.0001
        }

        LicenseDialog {
            id: licenseDialog
            anchors.fill: parent
            property bool showOnStart: true
            visible: showOnStart
            onDismissed: showOnStart = false;
        }

        Settings {
            property alias showMenuInfo: actionMenu.showMenuInfo
            property alias showLicenseInfo: licenseDialog.showOnStart
        }

        Connections {
            target: comModel

            function onGameDeckDataChanged(data) {
                table.model.setConfig(data);
            }
            function onIsServerChanged() {
                console.log("isServer changed to", comModel.isServer);
            }
            function onGameStartedChanged() {
                if (comModel.gameStarted) {
                    table.startGame()
                }
            }
            function onDealerDeckSet(jsonData) {
                table.dealDeck.setCardsJson(jsonData);
            }
            function onRemoteMove(moveInfo) {
                console.log("Remote Move", moveInfo);
                gameControl.remoteMoved(moveInfo);
            }
            function onRemoteUndoLast() {
                console.log("Remote undo");
                gameControl.undoLastRemote();
            }
        }

        Connections {
            target: gameControl
            function onSignalMoveToRemote(moveInfo) {
                console.log(moveInfo);
                comModel.sendMoveInfo(moveInfo);
            }
            function onSignalUndoToRemote() {
                comModel.sendUndoLast();
            }
        }

        // Eat back button presses to prevent exiting
        Keys.onPressed: (event)=> {
            if (event.key === Qt.Key_Back) {
                event.accepted = true;
            }
        }
        Keys.onReleased: (event)=> {
            if (event.key === Qt.Key_Back) {
                event.accepted = true;
            }
        }
    }
}

