import QtQuick

Item {
    property alias text: input.text
    Column {
        anchors.centerIn: parent
        width: parent.width - pageMargins * 4
        spacing: pageMargins * 4
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: fontPxSizeM
            text: qsTr("Enter player name:")
            color: globalColors.dialogText
        }

        Rectangle {
            width: parent.width
            height: input.height + pageMargins * 4
            border.color: input.activeFocus ? globalColors.editActive : globalColors.editInActive
            border.width: 2
            color: globalColors.editBg
            radius: pageMargins

            TextInput {
                id: input
                anchors.centerIn: parent
                width: parent.width - pageMargins * 4
                font.pixelSize: fontPxSizeL

                color: globalColors.editText
            }
        }
    }
}
