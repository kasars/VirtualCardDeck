import QtQuick
import VirtualCardDeck

Item {
    id: selectHostPage

    signal hostSelected(string hostId)
    signal cancelClicked();

    Row {
        id: header
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            margins: pageMargins
        }
        spacing: pageMargins
        Text { text: qsTr("Host"); font.pixelSize: fontPxSizeM; width: hostWidth }
        Text { text: qsTr("Player Name"); font.pixelSize: fontPxSizeM; width: nameWidth }
        Text { text: qsTr("Game Name"); font.pixelSize: fontPxSizeM; width: gameWidth }
        Text { text: qsTr("Players"); font.pixelSize: fontPxSizeM; width: nrWidth }
    }

    Rectangle {
        id: separator
        anchors {
            top: header.bottom
            left: parent.left
            right:parent.right
            margins: pageMargins
        }

        height: fontPxSizeL * 0.08
        color: globalColors.cardBorder
    }

    readonly property double hostWidth: header.width * 0.2
    readonly property double nameWidth: header.width * 0.3
    readonly property double gameWidth: header.width * 0.3
    readonly property double nrWidth: header.width * 0.2

    ListView {
        id: hostListView

        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: pageMargins
        }

        model: comModel.hostInfos

        delegate: Item {
            width: hostListView.width
            height: Math.min(window.width, window.height) * 0.15

            opacity: model.modelData.gameName !== "" ? 1.0 : 0.6

            Row {
                anchors.fill: parent
                spacing: pageMargins
                Text {
                    id: hostName
                    font.pixelSize: fontPxSizeS
                    verticalAlignment:  Text.AlignVCenter
                    text:  model.modelData.hostName
                    width: hostWidth
                    wrapMode: Text.Wrap
                    height: parent.height
                }
                Text {
                    font.pixelSize: fontPxSizeS
                    verticalAlignment:  Text.AlignVCenter
                    text:  model.modelData.playerName
                    width: nameWidth
                    wrapMode: Text.Wrap
                    height: parent.height
                }
                Text {
                    font.pixelSize: fontPxSizeS
                    verticalAlignment:  Text.AlignVCenter
                    text:  model.modelData.gameName
                    width: gameWidth
                    wrapMode: Text.Wrap
                    height: parent.height
                }
                Text {
                    font.pixelSize: fontPxSizeS
                    verticalAlignment:  Text.AlignVCenter
                    text:  {
                        console.log(model.modelData.numJoined, model.modelData.numPlayers);
                        return qsTr("%1/%2").arg(model.modelData.numJoined).arg(model.modelData.numPlayers);
                    }
                    width: nrWidth
                    wrapMode: Text.Wrap
                    height: parent.height
                }
            }
            Rectangle {
                anchors {
                    top: parent.top
                    horizontalCenter: parent.horizontalCenter
                }
                height: parent.height * 0.01
                width: parent.width
                color: hostName.color
                visible: model.index > 0
            }

            Rectangle {
                anchors.fill: parent
                color: hostName.color
                opacity: 0.3
                visible: mArea.pressed
            }
            MouseArea {
                id: mArea
                anchors.fill: parent
                onClicked: hostSelected(model.modelData.hostId);
                visible: model.modelData.gameName !== ""
            }
        }
    }

    Keys.onPressed: (event)=> {
        if ((event.key === Qt.Key_Back || event.key === Qt.Key_Escape) && selectHostPage.visible) {
            event.accepted = true;
        }
    }
    Keys.onReleased: (event)=> {
        if ((event.key === Qt.Key_Back || event.key === Qt.Key_Escape) && selectHostPage.visible) {
            selectHostPage.cancelClicked();
            event.accepted = true;
        }
    }
}

