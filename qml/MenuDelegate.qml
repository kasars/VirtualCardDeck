import QtQuick
import VirtualCardDeck

Item {
    id: menuDelegate

    property alias icon: iconItem.source
    property alias text: textItem.text
    property bool dragConfirm: false

    height: fontPxSizeM * 2
    width: fontPxSizeM * 2 + textItem.width + pageMargins


    signal clicked()
    signal dragConfirmed()

    Image {
        id: iconItem
        anchors {
            top: parent.top
            left: parent.left
            bottom: parent.bottom
        }
        width: height
        opacity: mArea.pressed ? 0.7 : 1
    }

    Text {
        id: textItem
        anchors {
            top: parent.top
            left: iconItem.right
            bottom: parent.bottom
            leftMargin: pageMargins
        }
        opacity: mArea.pressed ? 0.7 : 1
        font.pixelSize: fontPxSizeM
        font.bold: true
        color: globalColors.dialogText
        style: Text.Outline;
        styleColor: globalColors.dialogTextOutline
        verticalAlignment: Text.AlignVCenter
    }

    Image {
        id: arrowItem
        anchors {
            top: parent.top
            right: dragConfirm && mArea.pressed ? undefined : iconItem.right
            bottom: parent.bottom
        }
        width: height
        source: "RightArrow.png"
        visible: mArea.pressed && dragConfirm
    }

    Image {
        id: okImage
        anchors {
            top: parent.top
            left: parent.left
            bottom: parent.bottom
        }
        width: height
        visible: arrowItem.x > (parent.width /2)
        source: "OkButton.png"
    }

    MouseArea {
        id: mArea
        anchors.fill: parent

        drag.target: dragConfirm ? arrowItem : undefined
        drag.axis: Drag.XAxis
        drag.minimumX: 0
        drag.maximumX: menuDelegate.width - arrowItem.width

        onClicked: menuDelegate.clicked()
        onReleased: {
            if (arrowItem.x > (parent.width /2)) {
                menuDelegate.dragConfirmed();
            }
        }
    }
}
