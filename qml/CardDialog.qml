import QtQuick
import VirtualCardDeck


Item {
    id: cardDialog

    property alias source: dialogLoader.source
    property alias sourceComponent: dialogLoader.sourceComponent
    property alias item: dialogLoader.item
    property alias status: dialogLoader.status
    property alias text: dialogText.text

    property variant callbackItem: undefined

    property int buttons: 3

    readonly property int okButton: 1
    readonly property int cancelButton: 2
    readonly property int yesButton: 4
    readonly property int noButton: 8
    readonly property int customAcceptButton: 16
    readonly property int customCancelButton: 32

    opacity: 0
    visible: opacity > 0.00001;

    signal loaded()
    signal accepted()
    signal canceled()

    function cancel() {
        if (typeof callbackItem !== "undefined" &&
            typeof callbackItem.canceled == "function")
        {
            callbackItem.canceled();
        }
        else {
            cardDialog.canceled();
        }
        callbackItem = undefined;
        state = "closed";
    }

    function accept() {
        if (typeof callbackItem !== "undefined" &&
            typeof callbackItem.accepted == "function")
        {
            callbackItem.accepted();
        }
        else {
            cardDialog.accepted();
        }
        callbackItem = undefined;
        state = "closed";
    }

    function openDialog(text, buttons, callback) {
        dialogText.text = text;
        cardDialog.buttons = buttons ? buttons : 3

        cardDialog.callbackItem = callback;
        state = "open";
    }

    function openCustomButtonDialog(text, acceptText, cancelText, callback) {
        dialogText.text = text;
        var buttonMask = 0;
        if (acceptText !== "") {
            customBtnAccept.text = acceptText;
            buttonMask |= customAcceptButton;
        }
        if (cancelText !== "") {
            customBtnCancel.text = cancelText;
            buttonMask |= customCancelButton;
        }
        cardDialog.buttons = buttonMask;

        cardDialog.callbackItem = callback;
        state = "open";
    }


    Behavior on opacity {
        NumberAnimation { duration: 150 }
    }
    state: "closed"
    states: [
        State { name: "open"; PropertyChanges { target: cardDialog; opacity: 1.0 } },
        State { name: "closed"; PropertyChanges { target: cardItem; scale: 0.1 } }
    ]

    Rectangle {
        id: backgroundShield
        anchors.fill: parent
        color: globalColors.black
        opacity: 0.3
    }

    MouseArea {
        anchors.fill: parent
        onClicked: cancel();
    }

    Rectangle {
        id: cardItem
        anchors.centerIn: parent
        height: parent.height * 0.6
        width: parent.width * 0.6
        radius: width * 0.01
        color: globalColors.dialogBg

        Behavior on scale {
            NumberAnimation { duration: 150 }
        }

        Text {
            id: dialogText
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                bottom: buttonRow.top
                margins: pageMargins * 3
            }
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: fontPxSizeM
            font.bold: true
            wrapMode: Text.WordWrap
            color: globalColors.dialogText
        }

        MouseArea { anchors.fill: parent }

        Loader {
            id: dialogLoader
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                bottom: buttonRow.top
                margins: pageMargins * 3
            }
            onLoaded: {
                cardDialog.loaded();
            }
        }

        Row {
            id: buttonRow
            anchors {
                bottom: parent.bottom
                right: parent.right
                margins: pageMargins * 2
                rightMargin: pageMargins * 4
            }
            spacing: pageMargins * 4

            MenuDelegate {
                id: customBtnAccept
                icon: "OkButton.png"
                text: qsTr("Yes")
                onClicked: accept()
                visible: (buttons & customAcceptButton) !== 0
            }
            MenuDelegate {
                id: customBtnCancel
                icon: "OkButton.png"
                text: qsTr("No")
                onClicked: cancel()
                visible: (buttons & customCancelButton) !== 0
            }
            MenuDelegate {
                icon: "OkButton.png"
                text: qsTr("Yes")
                onClicked: accept()
                visible: (buttons & yesButton) !== 0
            }
            MenuDelegate {
                icon: "QuitButton.png"
                text: qsTr("No")
                onClicked: cancel()
                visible: (buttons & noButton) !== 0
            }
            MenuDelegate {
                icon: "OkButton.png"
                text: qsTr("OK")
                onClicked: accept()
                visible: (buttons & okButton) !== 0
            }
            MenuDelegate {
                icon: "QuitButton.png"
                text: qsTr("Cancel")
                onClicked: cancel()
                visible: (buttons & cancelButton) !== 0
            }
        }
    }
}
