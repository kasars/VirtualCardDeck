import QtQuick
import VirtualCardDeck

Item {
    id: waitForPlayers

    signal cancelClicked();

    ListView {
        id: gameListView

        anchors.fill: parent

        model: comModel.joinedPlayers

        delegate: Text {
            width: waitForPlayers.width
            height: Math.min(waitForPlayers.width, waitForPlayers.height) * 0.15
            font.pixelSize: fontPxSizeL
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment:  Text.AlignVCenter
            text: model.modelData

            Rectangle {
                anchors {
                    top: parent.top
                    horizontalCenter: parent.horizontalCenter
                }
                height: parent.height * 0.01
                width: parent.width * 0.8
                color: parent.color
                visible: model.index > 0
            }
        }
    }

    Keys.onPressed: {
        if ((event.key === Qt.Key_Back || event.key === Qt.Key_Escape) && waitForPlayers.visible) {
            waitForPlayers.cancelClicked();
            event.accepted = true;
        }
    }
}
