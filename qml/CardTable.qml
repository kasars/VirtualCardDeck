import QtQuick
import VirtualCardDeck

FocusScope {
    id: deckTable
    width: 800
    height: 600

    property alias model: tableModel
    property alias dealDeck: dealCardsDeck


    readonly property int visibleRows: tableModel.playerRows + tableModel.commonRows + (tableModel.playerCount > 1 ? tableModel.playerRows-1: 0)

    readonly property int visibleColumns: Math.max(Math.max(tableModel.playerColumns, tableModel.commonColumns),
                                                    (tableModel.playerCount > 1 ? tableModel.playerColumns * (tableModel.playerCount-1): 0))

    readonly property double maxCardW: (width-deckSpacing) / visibleColumns - deckSpacing
    readonly property double maxCardH: (height-deckSpacing) / visibleRows - deckSpacing

    readonly property double tableCardWidth: Math.min(maxCardW, maxCardH/1.45)
    readonly property double tableCardHeight: tableCardWidth * 1.45

    readonly property bool landscape: visibleRows * 1.45 < visibleColumns

    property variant pullDecks: new Object()
    property variant pushDecks: new Object()
    property bool pullActive: false
    property bool animating: false

    VCDTableModel {
        id: tableModel
    }

    function deleteCards() {
        player0Decks.deleteCards();
        player1Decks.deleteCards();
        player2Decks.deleteCards();
        player3Decks.deleteCards();
        common.deleteCards();
    }

    function dealCards() {
        for (var i = 0; i < comModel.joinedPlayers.length;  ++i) {
            //console.log("Deal to player: ", i, " at decks ", playerToDeckIndex(i));
            playersDecks[playerToDeckIndex(i)].dealOutCards();
        }
        common.dealOutCards();
        common.dealRest();

        // If the common cards has no "rest" (cards left) try player0
        if (comModel.joinedPlayers.length > 0) {
            playersDecks[playerToDeckIndex(0)].dealRest();
        }
    }

    //playerToDeck vs deck to player
    readonly property variant playersFilters: ["player0Decks", "player1Decks", "player2Decks", "player3Decks"]

    readonly property variant playersDecks: [player0Decks, player1Decks, player2Decks, player3Decks]

    function playerIndexAtDeck(index) {
        var newIndex = index + comModel.playerIndex;
        if (newIndex >= comModel.joinedPlayers.length) {
            newIndex -= comModel.joinedPlayers.length;
        }
        return newIndex;
    }

    function playerToDeckIndex(index) {
        var newIndex = index - comModel.playerIndex;
        if (newIndex < 0) {
            newIndex += comModel.joinedPlayers.length;
        }
        return newIndex;
    }

    FilterProxyModel {
        id: meModel
        srcModel: tableModel
        filterRole: VCDTableModel.PlayerRole
        passFilters: [ playersFilters[playerIndexAtDeck(0)] ]
    }

    FilterProxyModel {
        id: player1DecksModel
        srcModel: tableModel
        filterRole: VCDTableModel.PlayerRole
        passFilters: [ playersFilters[playerIndexAtDeck(1)] ]
    }

    FilterProxyModel {
        id: player2DecksModel
        srcModel: tableModel
        filterRole: VCDTableModel.PlayerRole
        passFilters: [ playersFilters[playerIndexAtDeck(2)] ]
    }

    FilterProxyModel {
        id: player3DecksModel
        srcModel: tableModel
        filterRole: VCDTableModel.PlayerRole
        passFilters: [ playersFilters[playerIndexAtDeck(3)] ]
    }

    FilterProxyModel {
        id: commonModel
        srcModel: tableModel
        filterRole: VCDTableModel.PlayerRole
        passFilters: [ "common" ]
    }

    Row {
        id: othersRow
        anchors {
            top: parent.top
            topMargin: tableModel.showAllPlayerHands ? -tableCardHeight *0.70 : -(tableCardHeight+pageMargins)
            left: parent.left
            right: parent.right
        }
        Item {
            id: player1DecksContainer
            width: deckTable.width / Math.max(1, tableModel.playerCount-1)
            height: player1Decks.height
            visible: player1Decks.deckCount > 0

            //Rectangle {
            //    anchors.fill: player1Decks
            //    color: "yellow"
            //}
            PlayerDecks {
                id: player1Decks
                anchors.centerIn: player1DecksContainer
                model: player1DecksModel
                dealDeck: dealCardsDeck
                columns: tableModel.playerColumns
                rows: tableModel.playerRows
                table: deckTable
                otherPlayer: true
                strId: "player 1"
            }
        }
        Item {
            id: player2DecksContainer
            width: deckTable.width / Math.max(1, tableModel.playerCount-1)
            height: player2Decks.height
            visible: player2Decks.deckCount > 0

            //Rectangle {
            //    anchors.fill: player2Decks
            //    color: "red"
            //}
            PlayerDecks {
                id: player2Decks
                anchors.centerIn: player2DecksContainer
                model: player2DecksModel
                dealDeck: dealCardsDeck
                columns: tableModel.playerColumns
                rows: tableModel.playerRows
                table: deckTable
                otherPlayer: true
                strId: "player 2"
            }
        }
        Item {
            id: player3DecksContainer
            width: deckTable.width / Math.max(1, tableModel.playerCount-1)
            height: player3Decks.height
            visible: player3Decks.deckCount > 0

            //Rectangle {
            //    anchors.fill: player3Decks
            //    color: "blue"
            //}
            PlayerDecks {
                id: player3Decks
                anchors.centerIn: player3DecksContainer
                model: player3DecksModel
                dealDeck: dealCardsDeck
                columns: tableModel.playerColumns
                rows: tableModel.playerRows
                table: deckTable
                otherPlayer: true
                strId: "player 3"
            }
        }
        visible: tableModel.playerCount > 1
    }

    Item {
        id: commonContainer
        anchors {
            top: othersRow.bottom
            left:parent.left
            right: parent.right
            bottom: meContainer.top
        }

        width: deckTable.width
        height: common.height
        visible: tableModel.commonColumns > 0

        PlayerDecks {
            id: common
            anchors.centerIn: commonContainer
            model: commonModel
            columns: tableModel.commonColumns
            rows: tableModel.commonRows
            dealDeck: dealCardsDeck
            table: deckTable
            strId: "common"
        }
    }
    Item {
        id: meContainer
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            bottomMargin: tableModel.showAllPlayerHands ? -tableCardHeight *0.33 : 0
        }

        width: deckTable.width
        height: player0Decks.height

        //Rectangle {
        //   anchors.fill: player0Decks
        //   color: "orange"
        //}
        PlayerDecks {
            id: player0Decks
            anchors.centerIn: meContainer
            strId: "Me player"
            model: meModel
            dealDeck: dealCardsDeck
            columns: tableModel.playerColumns
            availableColumns: tableModel.playerColumns * Math.max(tableModel.playerCount - 1, 1)
            rows: tableModel.playerRows
            table: deckTable
            otherPlayer: false
        }
    }

    MouseArea {
        id: pullActiveShield
        anchors.fill: parent
        visible: animating || pullActive
        onPressed: {
            pullActive = false;
        }
    }

    VCDDeckModel {
        id: dealCardsDeck
    }

    Connections {
        target: gameControl

        function onUndoMove(fromDeck, toDeck, fromIndex: int, toIndex: int, moveId: int, cardId: int) {
            //console.log("Undo", fromDeck, toDeck, fromIndex, toIndex, moveId, cardId);
            // NOTE: inverted decks and indexes
            toDeck.animateMove(fromDeck, toIndex, fromIndex, moveId, cardId, true, true);
        }

        function onRemoteMove(fromDeck, toDeck, fromIndex: int, toIndex: int, moveId: int, cardId: int) {
            console.log("Remote Move Card:", fromDeck, toDeck, fromIndex, toIndex, moveId, cardId);
            fromDeck.animateMove(toDeck, fromIndex, toIndex, moveId, cardId, false, true);
        }
    }

    function cancelSelects() {
        player0Decks.cancelSelect();
        player1Decks.cancelSelect();
        player2Decks.cancelSelect();
        player3Decks.cancelSelect();
        common.cancelSelect();
    }

    Keys.onPressed: (event)=> {
        if (event.key === Qt.Key_Escape) {
            console.log("Pressed:", event.key, event.isAutoRepeat);
            //console.log(deckTable.children.length)
            if (player0Decks.cancelSelect()) {}
            else if (player1Decks.cancelSelect()) {}
            else if (player2Decks.cancelSelect()) {}
            else if (player3Decks.cancelSelect()) {}
            else if (common.cancelSelect()) {}
            else if (!gameControl.undoLast()) {
                console.log("No more undo:s available");
            }
            event.accepted = true;
        }
    }
    Keys.onReleased: (event)=> {
        if (event.key === Qt.Key_Escape) {
            console.log("Released:", event.key);
            event.accepted = true;
        }
    }
}
