import QtQuick
import VirtualCardDeck


Row {
    id: nextCardDeck
    property alias cards: closedDeck.cards

    property variant tableItem: parent
    property alias topCards: openDeck.cards
    property variant topDeck: openDeck

    spacing: deckSpacing

    Deck {
        id: closedDeck
        bottomDeck: true
        faceUp: false
        cardOffsetY: height * 0.003
        tableItem: nextCardDeck.tableItem

        keys: ["No drop"]

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if (closedDeck.count == 0) {
                    var moveId = gameControl.nextMoveId();
                    while (openDeck.cards.rowCount() > 0) {
                        gameControl.moveCardFromTo(openDeck, closedDeck, 0 , 0, moveId, openDeck.cards.cardId(0));
                        openDeck.cards.moveCardTo(closedDeck.cards, 0, 0);
                    }
                }
                else {
                    var moveId = gameControl.nextMoveId();
                    closedDeck.animateMove(openDeck, 0, 0, moveId, closedDeck.cards.cardId(0), false, false);
                }
            }
        }
    }

    Deck {
        id: openDeck
        bottomDeck: true
        faceUp: true
        cardOffsetY: height*0.002
        keys: ["No drop"]
        neverMoveAll: true
        tableItem: nextCardDeck.tableItem
    }
    function cancelSelect() {
        return false;
    }
}
