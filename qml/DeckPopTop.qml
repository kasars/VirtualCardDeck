import QtQuick
import VirtualCardDeck

Deck {
    id: popTopBottomDeck

    faceUp: false
    cardOffsetX: 0
    cardOffsetY: height * 0.013
    property double topCardOffsetX: 0
    property double topCardOffsetY: tableCardHeight * 0.15


    keys: ["Do not accept drops"]

    property double maxHeight: tableCardHeight
    property double maxWidth: tableCardWidth
    height: maxHeight
    width: maxWidth

    property alias topCards: topDeckItem.cards
    property variant topDeck: topDeckItem
    property alias moveCards: moveDeck.cards

    readonly property int topCount: topDeckItem.count + moveDeck.count

    function topCardDropKeys(index) {
        return topDeckItem.cardDropKeys(index);
    }

    Deck {
        id: topDeckItem
        anchors.fill: parent
        bottomDeck: false
        firstCardOffsetX: (popTopBottomDeck.count) * popTopBottomDeck.cardOffsetX
        firstCardOffsetY: (popTopBottomDeck.count) * popTopBottomDeck.cardOffsetY

        z:199
        allowedValue: popTopBottomDeck.allowedValue
        allowedFamily: popTopBottomDeck.allowedFamily
        allowMultiple: popTopBottomDeck.allowMultiple


        tableItem: popTopBottomDeck.tableItem
        cardOffsetX: topCardOffsetX
        cardOffsetY: topCardOffsetY

        player: popTopBottomDeck.player
        deckId: popTopBottomDeck.deckId + "_top"

        onClicked: {
            if (topDeckItem.count == 0 && popTopBottomDeck.count > 0) {
                gameControl.moveCardFromTo(popTopBottomDeck, topDeckItem, 0, 0, gameControl.nextMoveId(), popTopBottomDeck.cards.cardId(0), false);
                popTopBottomDeck.cards.moveCardTo(topDeckItem.cards, 0, 0);
                topDeckItem.animateFaceUp();
            }
            else if (topDeckItem.count > 2) {
                cardList.visible = true;
            }
        }

        visible: !cardList.visible

        Rectangle {
            anchors.fill: parent
            color: globalColors.pressShade
            visible: moveDeck.count > 0
        }
    }

    Rectangle {
        id: listShadow
        anchors.fill: parent
        color: globalColors.pressShade
        visible: cardList.visible
        z: 200

        MouseArea {
            anchors.fill: parent
            onClicked: {
                cardList.visible = false;
            }
        }
    }

    Deck {
        id: moveDeck
        anchors.fill: parent
        firstCardOffsetY: topDeckItem.topCardY

        z: 201

        bottomDeck: false
        tableItem: popTopBottomDeck.tableItem
        cardOffsetX: topDeckItem.cardOffsetX
        cardOffsetY: topDeckItem.cardOffsetY
        alwaysMoveAll: true

        player: popTopBottomDeck.player
        deckId: popTopBottomDeck.deckId + "_move"
        acceptDrops: false

        onClicked: {
            if (moveDeck.count > 0) {
                var moveId = gameControl.nextMoveId();
                while (moveDeck.cards.rowCount() > 0) {
                    gameControl.moveCardFromTo(moveDeck, topDeckItem, moveDeck.cards.rowCount()-1, 0, moveId, moveDeck.cards.cardId(moveDeck.cards.rowCount()-1), false);
                    moveDeck.cards.moveCardTo(topDeckItem.cards, moveDeck.cards.rowCount()-1, 0);
                }
            }
        }
        visible: count > 0
    }

    CardList {
        id: cardList
        anchors {
            top: parent.top
            topMargin: pageMargins
            left: popTopBottomDeck.left
            right: popTopBottomDeck.right
            bottom: parent.bottom
            bottomMargin: pageMargins
        }
        z: 202
        visible: false
        onCardSelected: {
            var moveId = gameControl.nextMoveId();
            while (topDeckItem.cards.rowCount() > index) {
                gameControl.moveCardFromTo(topDeckItem, moveDeck, 0, moveDeck.cards.rowCount(), moveId, topDeckItem.cards.cardId(0), false);
                topDeckItem.cards.moveCardTo(moveDeck.cards, 0, moveDeck.cards.rowCount());
            }
            visible = false;
        }

        states: [
        State {
            name: "selecting"
            when: cardList.visible
            ParentChange { target: cardList; parent: popTopBottomDeck.tableItem; }
            PropertyChanges { target: cardList; cards: popTopBottomDeck.topCards; }
            ParentChange { target: listShadow; parent: popTopBottomDeck.tableItem; }
        }
        ]
    }

    function cancelSelect() {
        if (cardList.visible) {
            cardList.visible = false;
            return true;
        }
        return false;
    }
}
