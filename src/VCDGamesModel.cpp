/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#include "VCDGamesModel.h"

#include <QDebug>
#include <QDateTime>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QFile>
#include <QDir>



class VCDGamesModel::Private {
public:

    struct GameInfo {
        QString name;
        QString file;
        QString icon;
        int numPlayers = 0;
    };

    QList<GameInfo> games;


    bool validIndex(int i) { return i>=0 && i<games.count(); }


    GameInfo readGameInfo(const QString &fileName)
    {
        GameInfo info;

        QFile gameInfo(fileName);
        if (!gameInfo.open(QIODevice::ReadOnly)) {
            qDebug() << "Failed to read" << fileName;
            return info;
        }

        QJsonParseError error;
        QJsonDocument confDoc(QJsonDocument::fromJson(gameInfo.readAll(), &error));
        QJsonObject root = confDoc.object();

        if (root.isEmpty()) {
            qDebug() << "Failed to read configuration" << error.errorString() << error.offset;
            qDebug() << confDoc << fileName;
            return info;
        }

        info.name = root["name"].toString();
        info.numPlayers = root["playerCount"].toInt();
        info.icon = root["icon"].toString();
        info.file = fileName;
        return info;
    }



};

VCDGamesModel::VCDGamesModel(QObject *parent) : QAbstractListModel(parent), d(new Private())
{
    readGamesConfig();
}
VCDGamesModel::~VCDGamesModel() {}



Q_INVOKABLE void VCDGamesModel::readGamesConfig()
{
    beginResetModel();
    d->games.clear();
    // Search for built-in games in ":/Games"
    QDir qrcDir(QLatin1String(":/Games"));
    const QFileInfoList games = qrcDir.entryInfoList(QDir::Files | QDir::Readable, QDir::Name | QDir::LocaleAware);

    for (int i=0; i<games.size(); ++i) {
        Private::GameInfo game = d->readGameInfo(games[i].filePath());
        if (!game.file.isEmpty()) {
            d->games.append(game);
        }
    }

    // Search for configured games in ...
    //QDir qrcDir(QLatin1String(":/Games"))
    //const QFileInfoList games = qrcDir.entryInfoList(QDir::Files | QDir::Readable, QDir::Name | QDir::LocaleAware);
    //
    //for (int i=0; i<games.size(); ++i) {
    //}
    endResetModel();
}

QHash<int, QByteArray> VCDGamesModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole]        = "gameName";
    roles[FileNameRole]    = "fileName";
    roles[IconNameRole]    = "iconName";
    roles[NumPlayersRole]  = "numPlayers";

    return roles;
}


int VCDGamesModel::rowCount(const QModelIndex &) const
{
    return d->games.count();
}


QVariant VCDGamesModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !d->validIndex(index.row())) {
        return QVariant();
    }

    switch (role) {
        case NameRole:
            return d->games[index.row()].name;
        case FileNameRole:
            return d->games[index.row()].file;
        case IconNameRole:
            return d->games[index.row()].icon;
        case NumPlayersRole:
            return d->games[index.row()].numPlayers;
    }
    return QVariant();
}


