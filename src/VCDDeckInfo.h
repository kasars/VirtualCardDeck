// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QDebug>

struct VCDDeckInfo
{
    QString type;
    uint64_t ownerId = 0;
    int row = 0;
    int rowSpan = 0;
    int column = 0;
    int columnSpan = 0;
    bool faceUp = true;
    QString allowedValue;
    QString allowedFamily;
    bool allowMultiple = false;
    bool pullAllowed = false;
    bool allowAutoPull = false;
    int dealCount = 0;
    int topDealCount = 0;
    int deckIdBase;
    int deckIdTop;
    int deckIdMove;
    bool operator==(const VCDDeckInfo&) const = default;
};

typedef QList<VCDDeckInfo> VCDDeckList;
Q_DECLARE_METATYPE(VCDDeckInfo);
Q_DECLARE_METATYPE(VCDDeckList);

inline QDebug operator<<(QDebug debug, const VCDDeckInfo &d)
{
    QDebugStateSaver saver(debug);
    debug.space().noquote() << "[VCDDeckInfo type:" << d.type << d.ownerId << d.row << d.rowSpan << d.column
    << d.columnSpan << d.faceUp << d.allowedValue << d.allowedFamily << d.allowMultiple
    << d.pullAllowed << d.allowAutoPull << d.dealCount << d.topDealCount
    << d.deckIdBase << d.deckIdTop << d.deckIdMove  << "]";
    return debug;
}
