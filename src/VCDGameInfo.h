// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "VCDDeckInfo.h"

#include <QObject>

struct VCDGameInfo {
    QString name;
    QString base64QImageData;
    int playerCount = 0;
    bool operator==(const VCDGameInfo &) const = default;
};

typedef QList<VCDGameInfo> VCDGameList;
Q_DECLARE_METATYPE(VCDGameInfo);
Q_DECLARE_METATYPE(VCDGameList);

struct VCDGameDetails {
    VCDGameInfo info;
    VCDDeckList decks;

    bool showAllPlayerHands = false;
    double othersShowRows = 0;
    double othersShowColumns = 0;

    double meShowRows = 0;
    double meShowColumns = 0;

    int playerRows = 0;
    int playerColumns = 0;

    int commonRows = 0;
    int commonColumns = 0;

    double meSpace = 0.4;
    double commonSpace = 0.25;
    double opponentSpace = 0.35;

    void clear()
    {
        *this = VCDGameDetails();
    }

    bool operator==(const VCDGameDetails &) const = default;
};

Q_DECLARE_METATYPE(VCDGameDetails);

inline QDebug operator<<(QDebug debug, const VCDGameInfo &g)
{
    QDebugStateSaver saver(debug);
    debug.nospace().noquote() << "[VCDGameInfo name: " << g.name << ", base64QImageData: " << g.base64QImageData
                              << ", playerCount: " << g.playerCount << "]";
    return debug;
}

inline QDebug operator<<(QDebug debug, const VCDGameDetails &g)
{
    QDebugStateSaver saver(debug);
    debug.noquote() << "[VCDGameDetails info:" << g.info << g.showAllPlayerHands << g.othersShowRows
                    << g.othersShowColumns << g.meShowRows << g.meShowColumns << g.playerRows << g.playerColumns
                    << g.commonRows << g.commonColumns << g.meSpace << g.commonSpace << g.opponentSpace << '\n';
    int i = 0;
    for (const auto &deck : g.decks) {
        debug.nospace().noquote() << QString("    %1: ").arg(i, 3, u' ') << deck << '\n';
        i++;
    }
    return debug;
}
