// /* ============================================================
//  *
//  * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
//  *
//  * This program is free software; you can redistribute it and/or
//  * modify it under the terms of the GNU General Public License as
//  * published by the Free Software Foundation; either version 2 of
//  * the License or (at your option) version 3 or any later version.
//  *
//  * This program is distributed in the hope that it will be useful,
//  * but WITHOUT ANY WARRANTY; without even the implied warranty of
//  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  * GNU General Public License for more details.
//  *
//  * You should have received a copy of the GNU General Public License.
//  * along with this program.  If not, see <http://www.gnu.org/licenses/>
//  *
//  * ============================================================ */
// #include "VCDCard.h"
//
// class VCDCard::Private {
// public:
//     Private(): value(VCDCard::Ace), family(VCDCard::Hearts) {}
//     VCDCard::Value value;
//     VCDCard::Family family;
// };
//
// VCDCard::VCDCard(QObject *parent) : QObject(parent), d(new Private()) {}
// VCDCard::VCDCard(QObject *parent) : QObject(parent), d(new Private()) {}
// VCDCard::~VCDCard()
// {
//     delete d;
// }
//
// VCDCard::Family VCDCard::family() const
// {
//     return Hearts;
// }
//
// void VCDCard::setFamily(VCDCard::Family family)
// {
//     if (m_family != family) {
//         m_family = family;
//         emit familyChanged();
//     }
// }
//
// VCDCard::Color VCDCard::color() const
// {
//     if (m_family == Hearts || m_family == Diamonds) {
//         return Red;
//     }
//     return Black;
// }
//
// VCDCard::Value VCDCard::value() const
// {
//     return m_value;
// }
//
// void VCDCard::setValue(VCDCard::Value value)
// {
//     if (m_value != value) {
//         m_value = value;
//         emit valueChanged();
//     }
// }
