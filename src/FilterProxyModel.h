/* ============================================================
 *
 * Copyright (C) 2016 by Kåre Särs <kare.sars@iki .fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#ifndef FilterProxyModel_h
#define FilterProxyModel_h

#include <QSortFilterProxyModel>
#include <QVariantList>

class FilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(int filterRole READ filterRole WRITE setFilterRole NOTIFY filtersChanged)
    Q_PROPERTY(QObject* srcModel READ srcModel WRITE setSrcModel NOTIFY srcModelChanged)
    Q_PROPERTY(QVariantList passFilters READ passFilters WRITE setPassFilters NOTIFY filtersChanged)
    Q_PROPERTY(QVariantList blockFilters READ blockFilters WRITE setBlockFilters NOTIFY filtersChanged)

public:
    int filterRole() const;
    void setFilterRole(int role);

    QObject *srcModel() const;
    void setSrcModel(QObject *model);

    const QVariantList passFilters() const;
    void setPassFilters(const QVariantList &list);

    const QVariantList blockFilters() const;
    void setBlockFilters(const QVariantList &list);

    Q_INVOKABLE int mapToSrcRow(int row) const;

    Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const override;

Q_SIGNALS:
    void filtersChanged();
    void srcModelChanged();

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex & source_parent) const override;

private:
    QVariantList m_blockFilters;
    QVariantList m_passFilters;
};

#endif
