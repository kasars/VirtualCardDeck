/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#ifndef VCDTableModel_H
#define VCDTableModel_H

#include <QObject>
#include <QAbstractListModel>
#include <QVariantList>

class VCDTableModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString gameName READ gameName NOTIFY gameChanged)
    Q_PROPERTY(bool playerHasHand READ playerHasHand NOTIFY gameChanged)
    Q_PROPERTY(bool showAllPlayerHands READ showAllPlayerHands NOTIFY gameChanged)
    Q_PROPERTY(int playerCount READ playerCount NOTIFY gameChanged)
    Q_PROPERTY(int playerRows READ playerRows NOTIFY gameChanged)
    Q_PROPERTY(int playerColumns READ playerColumns NOTIFY gameChanged)
    Q_PROPERTY(int commonRows READ commonRows NOTIFY gameChanged)
    Q_PROPERTY(int commonColumns READ commonColumns NOTIFY gameChanged)

    Q_PROPERTY(QString config READ config NOTIFY gameChanged)

public:

    enum VCDTableModelRoles {
        PlayerRole = Qt::UserRole+1,
        RowRole,
        ColumnRole,
        RowSpanRole,
        ColumnSpanRole,
        TypeRole,
        FaceUpRole,
        AllowedValueRole,
        AllowedFamilyRole,
        AllowMultipleRole,
        PullAllowedRole,
        AllowPullRole,
        DealCountRole,
        TopDealCountRole,
    };
    Q_ENUM(VCDTableModelRoles)

    explicit VCDTableModel(QObject *parent = 0);
    ~VCDTableModel();

    QString gameName() const;
    bool playerHasHand() const;
    bool showAllPlayerHands() const;
    int playerCount() const;

    int playerRows() const;
    int playerColumns() const;

    int commonRows() const;
    int commonColumns() const;

    const QString config() const;

    Q_INVOKABLE bool setConfig(const QString &data);
    Q_INVOKABLE bool setConfigFile(const QString &path);

Q_SIGNALS:
    void gameChanged();
    void failedToReadGameConfig();

public:
    QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    Q_INVOKABLE QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    class Private;
    Private *const d;
};

#endif
