// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "VCDDeckInventory.h"

std::optional<QList<uint64_t>> VCDDeckInventory::reserveMove(uint64_t playerId, const VCDCardAndPosition &source, const VCDCardAndPosition &target)
{
    // Clear reservations from this player
    for (auto& deck : decks) {
        if (deck.reservedByPlayerId == playerId) {
            deck.reservedByPlayerId = 0;
        }
    }

    // Check if move is blocked
    auto reservedBy = moveBlocked(playerId, source, target);
    if (!reservedBy) {
        return std::nullopt;
    }

    if (!reservedBy.value().isEmpty()) {
        return reservedBy;
    }
    if (source.deckIndex != -1) {
        decks[source.deckIndex].reservedByPlayerId = playerId;
    }
    if (target.deckIndex != -1) {
        decks[target.deckIndex].reservedByPlayerId = playerId;
    }
    return reservedBy;
}


bool VCDDeckInventory::moveCard(const VCDCardAndPosition &source, const VCDCardAndPosition &target)
{
    if (source.deckIndex < 0 || source.deckIndex >= decks.size()) {
        qWarning() << "Source deck index invalid:" << source.deckIndex << decks.size();
        return false;
    }
    auto & srsDeck = decks[source.deckIndex];
    if (source.inDeckIndex < 0 || source.inDeckIndex >= srsDeck.cardIds.size())
    {
        qWarning() << "Source in deck index is invalid:" << source.inDeckIndex << srsDeck.cardIds.size();
        return false;
    }
    if (source.cardId != srsDeck.cardIds[source.inDeckIndex]) {
        qWarning() << "Source card id does not match:" << source.cardId << srsDeck.cardIds[source.inDeckIndex];
        return false;
    }


    if (target.deckIndex < 0 || target.deckIndex >= decks.size()) {
        qWarning() << "Target deck index invalid:" << target.deckIndex << decks.size();
        return false;
    }
    auto & trgDeck = decks[target.deckIndex];
    if (target.inDeckIndex < 0 || target.inDeckIndex > trgDeck.cardIds.size())
    {
        qWarning() << "Target in deck index is invalid:" << target.inDeckIndex << trgDeck.cardIds.size();
        return false;
    }

    trgDeck.cardIds.insert(target.inDeckIndex, srsDeck.cardIds.takeAt(source.inDeckIndex));
    trgDeck.reservedByPlayerId = 0;
    srsDeck.reservedByPlayerId = 0;
    return true;
}

std::optional<QList<uint64_t>> VCDDeckInventory::moveBlocked(uint64_t playerId,
                                                             const VCDCardAndPosition &source,
                                                             const VCDCardAndPosition &target)
{
    QList<uint64_t> reservedBy;

    // Check source card
    if (source.deckIndex != -1) {
        if (source.deckIndex < 0 || source.deckIndex >= decks.size()) {
            qWarning() << "Source deck index is invalid:" << source.deckIndex << decks.size();
            return std::nullopt;
        }
        auto & srsDeck = decks[source.deckIndex];
        if (source.inDeckIndex < 0 || source.inDeckIndex >= srsDeck.cardIds.size())
        {
            qWarning() << "Source in deck index is invalid:" << source.inDeckIndex << srsDeck.cardIds.size();
            return std::nullopt;
        }
        if (source.cardId != srsDeck.cardIds[source.inDeckIndex]) {
            qWarning() << "Source card id does not match:" << source.cardId << srsDeck.cardIds[source.inDeckIndex];
            return std::nullopt;
        }
        if (srsDeck.reservedByPlayerId != 0 && srsDeck.reservedByPlayerId != playerId) {
            reservedBy.append(srsDeck.reservedByPlayerId);
        }
    }

    // Check target card
    if (target.deckIndex != -1) {
        if (target.deckIndex < 0 || target.deckIndex >= decks.size()) {
            qWarning() << "Target deck index is invalid:" << target.deckIndex << decks.size();
            return std::nullopt;
        }
        auto & trgDeck = decks[target.deckIndex];
        if (target.inDeckIndex < 0 || target.inDeckIndex > trgDeck.cardIds.size())
        {
            qWarning() << "Target in-deck index is invalid:" << target.inDeckIndex << trgDeck.cardIds.size();
            return std::nullopt;
        }
        if (trgDeck.reservedByPlayerId != 0 && trgDeck.reservedByPlayerId != playerId) {
            reservedBy.append(trgDeck.reservedByPlayerId);
        }
    }

    return reservedBy;
}
