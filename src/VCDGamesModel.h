/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#ifndef VCDGamesModel_H
#define VCDGamesModel_H

#include <QObject>
#include <QAbstractListModel>
#include <QVariantList>

class VCDGamesModel : public QAbstractListModel
{
    Q_OBJECT

public:

    enum VCDGamesModelRoles {
        NameRole = Qt::UserRole+1,
        FileNameRole,
        IconNameRole,
        NumPlayersRole,
    };
    Q_ENUM(VCDGamesModelRoles)

    explicit VCDGamesModel(QObject *parent = 0);
    ~VCDGamesModel();

    Q_INVOKABLE void readGamesConfig();

public:
    QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    Q_INVOKABLE QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    class Private;
    Private *const d;
};

#endif
