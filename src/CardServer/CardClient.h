// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

#include "VCDCard.h"
#include "VCDDeckInventory.h"
#include "VCDGameInfo.h"
#include "VCDPlayer.h"

class QJsonObject;

class CardClient : public QObject {
    Q_OBJECT
public:
    explicit CardClient(QObject *parent = nullptr);

    void connectToServer(const QString &host, int port);

    bool connected() const;

    void reserveMove(const VCDCardAndPosition &source, const VCDCardAndPosition &target);
    void requestMove(const VCDCardAndPosition &source, const VCDCardAndPosition &target);
    void requestUndoLast();
    void sendPlayerInfo(const VCDPlayerInfo &player);
    void sendSelectGame(int index);
    void sendStartGame();
    void sendClearGame();

private Q_SLOTS:
    void onErrorOccurred(QAbstractSocket::SocketError socketError);
    void onStateChanged(QAbstractSocket::SocketState state);
    void onReadReady();

Q_SIGNALS:
    void serverConnectionClosed();
    void connectedChanged();

    void gameList(const VCDGameList &list);
    void gameSelection(int listIndex);
    void playerInfo(const VCDPlayerInfo &player);
    void playerList(const VCDPlayerList &list);
    void gameDetails(const VCDGameDetails &game);
    void deckInventory(const VCDDeckInventory &list);
    void cardMoved(const VCDCardAndPosition &source, const VCDCardAndPosition &target);
    void reserveOK();
    void reserveBlocked(const QList<uint64_t> &players);
    void startFailed();
    void reservedDecks(const QList<int> &deckIndexes);

protected:
    // Protected to override in tests
    virtual bool sendToServer(const QJsonObject &obj);

private:
    void parseServerJson(const QJsonObject &obj);

    QTcpSocket m_tcpSocket;

    friend class GameManagerTest;
};
