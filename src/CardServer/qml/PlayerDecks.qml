import QtQuick
import VirtualCardDeck
import "./"

Item {
    id: root

    property string playerId: ""
    property int rowCount: 1
    property int columnCount: 1

    property double shownRows: rowCount
    property double shownColumns: columnCount

    readonly property double widthSpace: root.width * columnCount / shownColumns
    readonly property double heightSpace: root.height * rowCount / shownRows

    readonly property double maxCardWidth: widthSpace / columnCount
    readonly property double maxCardHeight: heightSpace / rowCount
    readonly property double wMaxHeight: maxCardWidth * Const.cardHwRatio

    readonly property double cardHeight: Math.min(wMaxHeight, maxCardHeight)
    readonly property double cardWidth: cardHeight / Const.cardHwRatio


    readonly property var deckInfoModel: gameManager ? gameManager.deckInfoModel : []

    FilterProxyModel {
        id: deckModel
        srcModel: root.deckInfoModel
        filterRole: VCDDeckInfoModel.OwnerIdRole
        passFilters: [ root.playerId ]
    }

    Item {
        anchors {
            top: parent.top
            horizontalCenter: parent.horizontalCenter
        }
        width: childrenRect.width
        height: childrenRect.height

        Repeater {
            id: deckRepeater
            model: deckModel

            Item {
                id: cellItem
                width: root.cardWidth + root.cardWidth * (model.columnSpan-1)
                height: root.cardHeight + root.cardHeight * (model.rowSpan-1)

                x: root.cardWidth * model.column
                y: root.cardHeight * model.row

                Rectangle {
                    anchors.fill: parent
                    anchors.margins: Const.pageMargin / 2
                    color: Colors.cardBg
                    border.width: 1
                    border.color: Colors.cardBorder
                    radius: Const.pageMargin / 2
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log(root.rowCount, root.columnCount, root.shownRows, root.shownColumns);
                        console.log(root.widthSpace, root.heightSpace);
                        console.log(root.maxCardWidth, root.maxCardHeight);
                    }
                }

                /*
                 *            property variant loader: loaderItem
                 *
                 *            Loader {
                 *                id: loaderItem
                 *                anchors {
                 *                    left: parent.left
                 *                    top: parent.top
            }
            source: model.type + ".qml"

            property int dealCount: model.dealCount
            property int topDealCount: model.topDealCount

            onLoaded: {
            item.width = Qt.binding(function() { return cellItem.width; });
            item.height = Qt.binding(function() { return cellItem.height; });
            if (typeof item.faceUp !== "undefined") {
                item.faceUp = faceUp;
            }
            item.tableItem = table;

            if (typeof item.allowedValue !== "undefined") {
                item.allowedValue = model.allowedValue;
            }
            if (typeof item.allowedFamily !== "undefined") {
                item.allowedFamily = model.allowedFamily;
            }
            if (typeof item.allowMultiple !== "undefined") {
                item.allowMultiple = model.allowMultiple;
            }
            if (model.pullAllowed) {
                // We use an Object here in stead of an Array because for some reason we get two instances of every deck
                // (count is changed to the same value)
                table.pullDecks[model.player+model.index] = item;
            }
            if (model.allowAutoPull) {
                if (typeof item.topDeck !== undefined) {
                    table.pushDecks[model.player+model.index] = item.topDeck;
            }
            else {
                table.pushDecks[model.player+model.index] = item;
            }
            }

            if (typeof item.cardWidth !== "undefined") {
                item.cardWidth = Qt.binding(function() { return root.cardWidth; });
            }
            if (typeof item.maxHeight !== "undefined") {
                item.maxHeight = Qt.binding(function() { return cellItem.height; });
            }
            if (typeof item.maxWidth !== "undefined") {
                item.maxWidth = Qt.binding(function() { return cellItem.width; });
            }
            if (typeof item.dealCount !== "undefined") {
                item.dealCount = Qt.binding(function() { return model.dealCount; });
            }
            if (typeof item.otherPlayer !== "undefined") {
                item.otherPlayer = Qt.binding(function() { return group.otherPlayer; });
            }
            if (typeof item.player !== "undefined") {
                item.player = Qt.binding(function() { return model.player; });
            }
            if (typeof item.deckId !== "undefined") {
                item.deckId = Qt.binding(function() { return model.index; });
            }
            //console.log(loaderItem.source, model.player, columns, rows, cellItem.x, root.cardWidth, cellItem.width, item.width, item.height);
            var src = loaderItem.source + "";
            //console.log("source", src, typeof(src));
            if (src.indexOf("Hand.qml") !== -1 && !otherPlayer) {
                cellItem.x = pageMargins + (root.cardWidth + deckSpacing) * (model.column + paddingColumns - 1)
                cellItem.width += (root.cardWidth + deckSpacing) * 2
                console.log(model.player, columns, rows, cellItem.x, root.cardWidth, cellItem.width, item.width, item.height);
            }
            }
            }
            */
            }
        }
    }
}
