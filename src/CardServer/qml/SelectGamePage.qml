import QtQuick
import QtQuick.Controls
import VirtualCardDeck
import "./"

Rectangle {
    id: root

    implicitWidth: Const.windowWidth
    implicitHeight: Const.windowHeight

    onImplicitWidthChanged: console.log("####", implicitWidth, Const.windowWidth)
    color: Colors.tableBg

    FilterProxyModel {
        id: filteredGamesModel
        srcModel: gameManager ? gameManager.gamesModel ?? null : null
        filterRole: VCDGameListModel.PlayerCountRole
        passFilters: [ tabBar.currentIndex + 1 ]
    }

    TabBar {
        id: tabBar
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        Repeater {
            model: [ qsTr("Solitaire") , qsTr("Two players") , qsTr("Three players"), qsTr("Four players") ]

            TabButton {
                text: modelData
            }
        }
    }

    GridView {
        id: gameListView
        clip: true

        cellHeight: Const.gameIconHeight
        cellWidth: Const.gameIconWidth

        anchors {
            top: tabBar.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: Const.pageMargin
        }
        model: filteredGamesModel

        interactive: contentHeight > height

        delegate: Rectangle {
            width: gameListView.cellWidth - Const.pageMargin
            height: gameListView.cellHeight - Const.pageMargin
            radius: Const.pageMargin

            color: Colors.cardBg
            border.width: 1
            border.color: Colors.cardBorder

            Image {
                id: iconItem
                anchors {
                    top: parent.top
                    bottom:parent.bottom
                    left: parent.left
                    margins: Const.pageMargin
                }
                fillMode: Image.PreserveAspectFit
                source: "image://base64/" + model.imageData
            }

            Text {
                anchors {
                    top: parent.top
                    left: iconItem.right
                    right: parent.right
                    bottom: parent.bottom
                    margins: Const.pageMargin * 2
                }
                font.pixelSize: Const.fontPxSizeM
                verticalAlignment:  Text.AlignVCenter
                text: model.name
                maximumLineCount: 2
                wrapMode: Text.Wrap
                elide: Text.ElideRight
            }

            Rectangle {
                anchors {
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                    margins: Const.pageMargin
                }
                height: Const.pageMargin
                width: height
                radius: height / 2
                color: Colors.cardSelected
                visible: model.selected
            }

            Rectangle {
                anchors.fill: parent
                color: parent.color
                opacity: 0.3
                visible: mArea.pressed
                radius: parent.radius
            }

            MouseArea {
                id: mArea
                anchors.fill: parent
                onClicked: {
                    gameManager.selectGame(model.serverIndex);
                }
            }
        }
    }
}

