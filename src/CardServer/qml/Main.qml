import QtQuick
import QtCore
import QtQuick.Controls
import VirtualCardDeck
import "./"

Window {
    id: root
    width: 800
    height: 400
    visible: true
    title: qsTr("Hello World")

    color: Colors.tableBg

    onWidthChanged: {
        Const.windowWidth = width;
    }
    onHeightChanged: {
        Const.windowHeight = height;
    }

    Row {
        anchors.fill: parent
        spacing: Const.pageMargin
        ListView {
            id: playersView
            height: root.height
            width: Const.pageMargin * 20
            model: gameManager ? gameManager.playersModel ?? 0 : 0
            spacing: Const.pageMargin
            delegate: Item {
                width: Const.pageMargin * 20
                height: Const.pageMargin * 5

                Rectangle {
                    anchors.fill: parent
                    color: "#DDDDFF"
                }
                Text {
                    anchors.centerIn: parent
                    text: model.name
                    font.pixelSize: Const.fontPxSizeM
                }
            }
        }

        FilterProxyModel {
            id: filteredGamesModel
            srcModel: gameManager ? gameManager.gamesModel ?? null : null
            filterRole: VCDGameListModel.SelectedRole
            passFilters: [ true ]
        }

        ListView {
            id: gamesView
            height: root.height
            width: Const.pageMargin * 20
            model: filteredGamesModel
            spacing: Const.pageMargin
            delegate: Rectangle {
                height: Const.pageMargin * 7
                width: Const.pageMargin * 20
                radius: Const.pageMargin

                color: Colors.cardBg
                border.width: 1
                border.color: Colors.cardBorder

                Image {
                    anchors {
                        top: parent.top
                        bottom:parent.bottom
                        left: parent.left
                        margins: Const.pageMargin
                    }
                    fillMode: Image.PreserveAspectFit
                    source: "image://base64/" + model.imageData
                }
                Text {
                    anchors.centerIn: parent
                    text: qsTr("%1 (%2)").arg(model.name).arg(model.numPlayers)
                    font.bold: model.selected
                }
                Rectangle {
                    anchors {
                        top: parent.top
                        left: parent.left
                        margins: Const.pageMargin
                    }
                    width: Const.pageMargin * 2
                    height: Const.pageMargin * 2
                    radius: Const.pageMargin
                    color: "#FF0000"
                    visible: model.selected
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        gameManager.selectGame(index);
                    }
                }
            }
        }
        ListView {
            id: deckInfoView
            height: root.height
            width: Const.pageMargin * 20
            model: gameManager ? gameManager.deckInfoModel ?? 0 : 0
            spacing: Const.pageMargin
            delegate: Rectangle {
                color: "#DDDDFF"
                width: Const.pageMargin * 20
                height: Const.pageMargin * 5
                radius: Const.pageMargin
                Grid {
                    columns: 2
                    anchors.centerIn: parent
                    spacing: Const.pageMargin / 2
                    Text { text: qsTr("Type:") }
                    Text { text: model.type }
                    Text { text: qsTr("Row: %1").arg(model.row) }
                    Text { text: qsTr("Column: %1").arg(model.column) }
                }
            }
        }
        Row {
            Button {
                text: qsTr("Start")
                onClicked: {
                    gameManager.startGame();
                }
            }
        }
    }

    GameTable {
        anchors.fill: parent
    }

    SideMenu {
        id: actionMenu
        anchors.fill: parent
        sourceComponent:  ActionMenu {
            id: actionMenuContent
            onCloseMenu: {
                actionMenu.closeMenu();
            }
        }
        leftMenu: true
        menuWidth: parent.width * 0.3
        state: showMenuInfo ? "open" : "closed"

        showMenuInfo: true
        onMenuInfoAcknowledged: {
            showMenuInfo = false;
            state = "closed";
        }

        onVisibleChanged: {
            if (visible && showMenuInfo) {
                state = "open";
            }
        }
    }

    CardDialog {
        id: dialog
        anchors.fill: parent
    }

    LicenseDialog {
        id: licenseDialog
        anchors.fill: parent
        property bool showOnStart: true
        visible: showOnStart
        onDismissed: showOnStart = false
    }

    property string settingsPlayerName: gameManager ? gameManager.playerName : settings.playerName

    Settings {
        id: settings
        property alias showMenuInfo: actionMenu.showMenuInfo
        property alias showLicenseInfo: licenseDialog.showOnStart
        property string playerName: root.settingsPlayerName
    }

    Component.onCompleted: {
        gameManager.playerName = settings.playerName;
        Const.windowWidth = root.width;
        Const.windowHeight = root.height;
    }
    Component.onDestruction: {
        settings.playerName = settingsPlayerName
    }
}
