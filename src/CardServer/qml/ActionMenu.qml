import QtQuick
import VirtualCardDeck
import "./"

Item {
    id: root
    signal closeMenu()

    Column {
        anchors.fill: parent
        anchors.margins: Const.pageMargin
        spacing: Const.pageMargin * 2
        onSpacingChanged: console.log(Const.pageMargin * 2)

        MenuDelegate {
            width: parent.width
            icon: "qrc:/Ui/QuitButton.png"
            text: qsTr("Quit to menu")
            onClicked: console.log("Clicked quit")
            onDragConfirmed: console.log("Drag quit")
            dragConfirm: true
        }

        MenuDelegate {
            width: parent.width
            icon: "qrc:/Ui/NewButton.png"
            text: qsTr("New")
            onClicked: console.log("Clicked New")
            onDragConfirmed: console.log("Drag New")
            dragConfirm: true
        }

        MenuDelegate {
            id: gameSelection
            width: parent.width
            icon: "qrc:/Ui/Player3.png"
            text: qsTr("Select Game")
            onClicked: {
                VCDPopup.requestComponentDialog("SelectGamePage.qml", VCDPopup.okButton | VCDPopup.cancelButton, gameSelection);
                //root.closeMenu();
            }
            function accepted(component) {
                console.log("Selected Game");
            }
        }

        MenuDelegate {
            id: nameDelegate
            width: parent.width
            icon: "qrc:/Ui/NameButton.png"
            text: qsTr("Player name")
            onClicked: {
                VCDPopup.requestComponentDialog("PlayerNameInput.qml", VCDPopup.okButton | VCDPopup.cancelButton, nameDelegate);
                //root.closeMenu();
            }
            function accepted(component) {
                gameManager.playerName = component.text;
            }
        }
    }
}
