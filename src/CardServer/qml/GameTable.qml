import QtQuick
import VirtualCardDeck
import "./"

Item {
    id: root

    // We restrict to landscape.
    readonly property var deckInfoModel: gameManager ? gameManager.deckInfoModel : []
    readonly property var playersModel: gameManager ? gameManager.playersModel : []
    readonly property string mePlayerId: gameManager ? gameManager.mePlayerId : ""

    readonly property double othersShowRows: deckInfoModel.othersShowRows ?? 0
    readonly property double othersShowColumns: deckInfoModel.othersShowColumns ?? 0
    readonly property double meShowRows: deckInfoModel.meShowRows ?? 0
    readonly property double meShowColumns: deckInfoModel.meShowColumns ?? 0

    readonly property double playerRows: deckInfoModel.playerRows ?? 0
    readonly property double playerColumns: deckInfoModel.playerColumns ?? 0
    readonly property double commonRows: deckInfoModel.commonRows ?? 0
    readonly property double commonColumns: deckInfoModel.commonColumns ?? 0

    readonly property double meSpace: deckInfoModel.meSpace ?? 0.4
    readonly property double commonSpace: deckInfoModel.commonSpace ?? 0.25
    readonly property double opponentSpace: deckInfoModel.opponentSpace ?? 0.35

    visible: meShowRows > 0 // FIXME use something more reliable

    onMeShowRowsChanged: console.log("#", meShowRows, deckInfoModel.meShowRows)
    onMeShowColumnsChanged: console.log("#", meShowColumns, deckInfoModel.meShowColumns)
    onMePlayerIdChanged: console.log("##", mePlayerId, gameManager.mePlayerId)
    onMeSpaceChanged: console.log("###", meSpace, deckInfoModel.meSpace)
    onPlayerRowsChanged: console.log("######### Rows", playerRows, deckInfoModel.playerRows, playerColumns, deckInfoModel.playerColumns)
    onPlayerColumnsChanged: console.log("######### Columns", playerRows, deckInfoModel.playerRows, playerColumns, deckInfoModel.playerColumns)

    FilterProxyModel {
        id: oppPlayersModel
        srcModel: root.playersModel
        filterRole: VCDPlayersListModel.ServerIdRole
        blockFilters: [ root.mePlayerId, "0" ]
    }

    Rectangle {
        anchors.fill: parent
        color: "#77FFFFFF"
    }

    Column {
        id: othersRow
        anchors.fill: parent
        Item {
            id: oppContainer
            width: root.width
            height: root.height * root.opponentSpace
            visible: otherRepeater.count > 0
            Row {
                anchors.fill: oppContainer
                Repeater {
                    id: otherRepeater
                    model: oppPlayersModel
                    delegate: Item {
                        height: oppContainer.height
                        width: otherRepeater.count > 0 ? oppContainer.width / otherRepeater.count : 0

                        Rectangle {
                            anchors.fill: parent
                            radius: Const.pageMargin
                            color: "#220000FF"
                            border.width: 1; border.color: "#000000"
                        }
                        PlayerDecks {
                            anchors.fill: parent
                            anchors.margins: Const.pageMargin
                            rotation: 180
                            playerId: model.serverId
                            rowCount: root.commonRows
                            columnCount: root.commonColumns
                        }
                    }
                }
            }
        }

        Item {
            width: root.width
            height: root.height * root.commonSpace
            visible: root.commonRows > 0
            Rectangle { anchors.fill: parent; color: "#2200FF00"; border.width: 1; border.color: "#000000"; radius: Const.pageMargin}
            PlayerDecks {
                anchors.fill: parent
                anchors.margins: Const.pageMargin
                playerId: "0"
                rowCount: root.commonRows
                columnCount: root.commonColumns
            }
        }

        Item {
            width: root.width
            height: root.height * root.meSpace
            visible: root.playerRows > 0
            Rectangle { anchors.fill: parent; color: "#22FF0000"; border.width: 1; border.color: "#000000"; radius: Const.pageMargin }
            PlayerDecks {
                anchors.fill: parent
                playerId: root.mePlayerId
                shownRows: root.meShowRows
                shownColumns: root.meShowColumns
                rowCount: root.playerRows
                columnCount: root.playerColumns
            }
        }
    }
}
