pragma Singleton
import QtQuick

Item {
    property double windowWidth: 600
    property double windowHeight: 300

    readonly property double pageMargin: Math.min(windowHeight * 0.025, windowWidth * 0.025)

    readonly property double gameIconHeight: pageMargin * 8
    readonly property double gameIconWidth: pageMargin * 38

    readonly property double fontPxSizeS: pageMargin * 1.5
    readonly property double fontPxSizeM: pageMargin * 2
    readonly property double fontPxSizeL: pageMargin * 2.5

    readonly property double cardHwRatio: 1.45
}
