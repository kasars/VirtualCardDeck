import QtQuick
import VirtualCardDeck
import "./"

DropArea {
    id: hand
    property alias cards: cardsModel
    readonly property int count: listView.count
    property bool faceUp: true
    property variant tableItem: hand.parent
    readonly property double topCardY: listView.height - tableCardHeight

    property int dealCount: -1
    property bool otherPlayer: false

    property string player: ""
    property string deckId: ""

    height: tableCardHeight
    width:  (tableCardWidth + pageMargins) * 3

    onPlayerChanged: setIdTmr.start();
    onDeckIdChanged: setIdTmr.start();
    Timer {
        id: setIdTmr
        interval: 1
        onTriggered: {
            gameControl.setDeckId(hand, player+"_"+deckId);
        }
    }

    onEntered: {
        dropIndicator.hovering++;
    }

    onExited: {
        if (dropIndicator.hovering > 0) {
            dropIndicator.hovering--;
        }
    }

    Rectangle {
        anchors.fill: parent
        radius: height * 0.05
        color: globalColors.pressShade
    }

    VCDDeckModel {
        id: cardsModel
    }

    ListView {
        id: listView
        anchors {
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
        }
        width: Math.min(tableCardWidth*count, hand.width)
        height: parent.height
        model: cardsModel
        orientation: ListView.Horizontal
        interactive: contentWidth > width*1.00001

        displaced: Transition {
            NumberAnimation { properties: "x,y"; duration: 200; easing.type: Easing.InOutQuad }
        }
        addDisplaced: Transition {
            NumberAnimation { properties: "x,y"; duration: 200; easing.type: Easing.InOutQuad }
        }

        delegate: Item {
            id: delegateRoot

            z: model.index
            // This is to used to identify the source (card from other deck or card from this deck)
            property int handIndex: model.index
            width: listView.width / listView.count
            height: hand.height

            DropArea {
                id: myDropArea
                anchors{
                    left: parent.left
                    bottom: parent.bottom
                }
                height: tableCardHeight
                width: tableCardWidth
                property variant cards: hand.cards
                readonly property bool faceUp: hand.faceUp && !hand.otherPlayer
                readonly property variant deckId: hand
                readonly property int insertIndex: model.index + 1
                readonly property double topCardY: listView.height - tableCardHeight
                onEntered: {
                    if (typeof drag.source.handIndex !== "undefined") {
                        console.log("internal move", drag.source.handIndex, delegateRoot.handIndex);
                        gameControl.moveCardFromTo(hand, hand, drag.source.handIndex, delegateRoot.handIndex,
                                                   gameControl.nextMoveId(), card.cardId, false /*moveCardAnim.isRemote*/);
                        cards.moveCard(drag.source.handIndex, delegateRoot.handIndex, 1);
                    }
                    else {
                        console.log("External move?");
                        dropIndicator.hovering++;
                    }
                }

                onExited: {
                    if (dropIndicator.hovering > 0) {
                        dropIndicator.hovering--;
                    }
                }
            }

            Rectangle {
                anchors.fill: myDropArea
                color: globalColors.pressShade
            }


            Card {
                id: card
                width: tableCardWidth
                height: tableCardHeight
                anchors {
                    left: parent.left;
                    bottom: parent.bottom
                }
                value: model.value
                family: model.family
                faceUp: hand.faceUp && !hand.otherPlayer
                property int cardId: model.cardId

                Drag.active: mouseArea.drag.active
                Drag.source: delegateRoot
                Drag.hotSpot.x: width / 2
                Drag.hotSpot.y: height / 2
                Drag.keys: [cardsModel.familyStr(family), cardsModel.valueStr(value), cardsModel.famValStr(family, value)]

                states: [
                State {
                    name: "dragging"
                    when: mouseArea.drag.active || mouseArea.draging
                    ParentChange { target: card; parent: hand.tableItem; }
                    AnchorChanges { target: card; anchors.left: undefined; anchors.bottom: undefined; anchors.top: undefined  }
                },
                State {
                    name: "selected"
                    when: model.selected
                    AnchorChanges { target: card; anchors.top: parent.top; anchors.bottom: undefined }
                }
                ]

                SequentialAnimation {
                    id: dropAnim
                    property variant toDeck
                    property double fromX
                    property double fromY
                    property double toX
                    property double toY
                    property double dropTime: 200

                    ParallelAnimation {
                        NumberAnimation { target: card; property: "x"; from: dropAnim.fromX; to: dropAnim.toX; duration: dropAnim.dropTime }
                        NumberAnimation { target: card; property: "y"; from: dropAnim.fromY; to: dropAnim.toY; duration: dropAnim.dropTime }
                    }
                    ScriptAction {
                        script: {
                            if (myDropArea != dropAnim.toDeck) {
                                if (dropAnim.toDeck.cards === cards) {
                                    console.log("same deck");
                                    gameControl.moveCardFromTo(hand, hand, model.index, 0, gameControl.nextMoveId(), card.cardId, false /*moveCardAnim.isRemote*/);
                                    cards.moveCard(model.index, 0);
                                }
                                else {
                                    console.log(dropAnim.toDeck.cards, dropAnim.toDeck.deckId, model.index);
                                    gameControl.moveCardFromTo(hand, dropAnim.toDeck, model.index, 0, gameControl.nextMoveId(), card.cardId, false /*moveCardAnim.isRemote*/);
                                    cards.moveCardTo(dropAnim.toDeck.cards, model.index, 0);
                                }
                            }
                            mouseArea.draging = false;
                        }
                    }
                }


                MouseArea {
                    id:mouseArea
                    anchors.fill: parent
                    drag.target: card

                    property bool draging: false
                    readonly property bool dragActive: drag.active

                    acceptedButtons: Qt.LeftButton | Qt.RightButton


                    onClicked: {
                        cards.setSelected(model.index, !model.selected);
                    }

                    onDragActiveChanged: {
                        if (dragActive) { draging = true; }
                    }

                    onReleased: {
                        if (draging) {
                            if (card.Drag.target && card.Drag.target.deckId != hand) {
                                var toDeck = card.Drag.target;
                                var toPoint = toDeck.mapToItem(hand.tableItem,0,0);
                                dropAnim.toDeck = toDeck;
                                dropAnim.fromX = card.x;
                                dropAnim.fromY = card.y;
                                dropAnim.toX = toPoint.x;
                                dropAnim.toY = toPoint.y + toDeck.topCardY;
                                dropAnim.dropTime = dropDuration(toDeck.x, card.x, toDeck.y, card.y);

                                if (card.faceUp != toDeck.faceUp) {
                                    dropAnim.dropTime = Math.max(200, dropAnim.dropTime);
                                    card.flipTime = dropAnim.dropTime;
                                    card.faceUp = toDeck.faceUp;
                                }
                                dropAnim.start();
                            }
                            else {
                                var toPoint = myDropArea.mapToItem(hand.tableItem, 0, 0);
                                dropAnim.toDeck = myDropArea;
                                dropAnim.fromX = card.x;
                                dropAnim.fromY = card.y;
                                dropAnim.toX = toPoint.x;
                                dropAnim.toY = toPoint.y;
                                if (model.selected) dropAnim.toY -= listView.height - tableCardHeight;
                                dropAnim.dropTime = dropDuration(myDropArea.x, card.x, myDropArea.y, card.y);
                                dropAnim.start();
                            }
                        }
                    }

                    function dropDuration(x1, x2, y1, y2) {
                        var xDist = Math.abs(x1 - x2);
                        var yDist = Math.abs(y1 - y2);
                        return Math.min(Math.max(xDist, yDist),150);
                    }
                }
            }
        }
    }

    function animateMove(toDeck, fromIndex, toIndex, moveId, cardId, isUndo, isRemote) {
        if (count == 0) {
            console.log("no card to animate");
            return;
        }
        if (typeof isUndo == "undefined") {
            console.log ("isUndo cannot be undefined!!");
            console.trace();
            return;
        }
        if (typeof isRemote == "undefined") {
            console.log ("isRemote cannot be undefined!!");
            console.trace();
            return;
        }
        console.log("from:", deckId, "to:", toDeck.deckId, fromIndex, toIndex, "isUndo:", isUndo);
        if (toDeck === hand) {
            console.log("animate same deck");
            gameControl.moveCardFromTo(hand, hand, fromIndex, toIndex, moveId, cardId, isRemote);
            cards.moveCard(fromIndex, toIndex);
        }
        else {
            gameControl.moveCardFromTo(hand, toDeck, fromIndex, toIndex, moveId, cardId, isRemote);
            cards.moveCardTo(toDeck.cards, fromIndex, toIndex);
        }
    }


    Rectangle {
        id: dropIndicator
        anchors.fill: parent
        radius: height * 0.05
        color: globalColors.pressShade
        property int hovering: 0
        visible: hovering > 0
    }

    function cancelSelect() {
        return false;
    }
}
