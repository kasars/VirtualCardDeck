pragma Singleton
import QtQuick

Item {
    readonly property int okButton: 1
    readonly property int cancelButton: 2
    readonly property int yesButton: 4
    readonly property int noButton: 8
    readonly property int customAcceptButton: 16
    readonly property int customCancelButton: 32

    signal requestOpenDialog(string text, int buttons, var callback)
    signal requestComponentDialog(string source, int buttons, var callback)
    signal requestOpenCustomButtonDialog(string text, string acceptText, string cancelText, var callback)

    function openDialog(text, buttons, callback) {
        requestOpenDialog(text, buttons, callback);
    }

    function openComponentDialog(source, buttons, callback) {
        requestComponentDialog(source, buttons, callback);
    }

    function openCustomButtonDialog(text, acceptText, cancelText, callback) {
        requestOpenCustomButtonDialog(text, acceptText, cancelText, callback);
    }
}
