import QtQuick
import "./"

Item {
    id: sideMenu
    property bool leftMenu: true

    property alias source: menuLoader.source
    property alias sourceComponent: menuLoader.sourceComponent
    property alias item: menuLoader.item
    property alias status: menuLoader.status
    property alias state: menuLoader.state
    property alias menuWidth: menuLoader.width
    property alias menuHeight: menuLoader.height
    property double menuTopMargin: 0
    property bool showMenuInfo: false
    clip: true

    signal loaded()
    signal menuInfoAcknowledged()

    function closeMenu() {
        menuLoader.state = "closed";
    }

    Rectangle {
        id: shadow
        anchors.fill: parent
        color: Colors.dialogShadow
        opacity: (Math.abs(menuLoader.x - menuLoader.closedX)*0.3)/menuLoader.width
    }

    onWidthChanged: updateX.start();
    onHeightChanged: updateX.start();
    onLeftMenuChanged: updateX.start();

    Rectangle {
        anchors.fill: menuLoader
        color: Colors.dialogShadow
        opacity: (Math.abs(menuLoader.x - menuLoader.closedX)*0.3)/menuLoader.width + 0.1
    }


    MouseArea {
        id: dragArea
        anchors {
            top: menuLoader.state === "open" ? parent.top : menuLoader.top
            right: menuLoader.state === "open" ? parent.right : leftMenu ? parent.right : menuLoader.left
            left: menuLoader.state === "open" ? parent.left : leftMenu ? menuLoader.right : parent.left
            bottom: menuLoader.state === "open" ? parent.bottom : menuLoader.bottom

            leftMargin: leftMenu ? 0 : mAreaMargin
            rightMargin: leftMenu ? mAreaMargin : 0
        }
        property double mAreaMargin: menuLoader.state === "open" ? 0 : sideMenu.width - Screen.pixelDensity*2

        drag {
            target: menuLoader
            minimumX: leftMenu ? -menuLoader.width : sideMenu.width-menuLoader.width
            maximumX: leftMenu ? 0 : sideMenu.width
        }

        function changeState() {
            if (menuLoader.state === "open" && Math.abs(menuLoader.x - menuLoader.openX) > menuLoader.width * 0.3) {
                menuLoader.state = "closed";
            }
            else if (menuLoader.state === "closed" && Math.abs(menuLoader.x - menuLoader.closedX) > menuLoader.width * 0.3) {
                menuLoader.state = "open";
            }
            else {
                updateX.start();
            }
        }

        onClicked: {
            if (menuLoader.state === "open") {
                menuLoader.state = "closed";
            }
        }

        onReleased: changeState();
        onCanceled: changeState();
    }

    MouseArea {
        id: clickShield
        anchors.fill: menuLoader
        visible: menuLoader.state === "open"
    }

    Rectangle {
        anchors {
            verticalCenter: menuLoader.verticalCenter
            left: leftMenu ? menuLoader.right : undefined
            right: leftMenu ? undefined : menuLoader.left
        }
        color: Colors.cardBg

        width: 2
        height: menuLoader.height * 0.6
        Rectangle {
            anchors {
                top: parent.top
                bottom: parent.bottom
                left: leftMenu ? parent.right : undefined
                right: leftMenu ? undefined : parent.left
            }
            width: 1
            color: Colors.cardBorder
        }
    }

    Loader {
        id: menuLoader
        property double openX: leftMenu ? 0 : sideMenu.width - menuLoader.width;
        property double closedX: leftMenu ? -menuLoader.width : sideMenu.width;

        anchors.top: parent.top
        anchors.topMargin: sideMenu.menuTopMargin
        height: parent.height
        width: sideMenu.width * 0.7
        x: leftMenu ? 0 : parent.width - width
        visible: x !== closedX

        Behavior on x {
            NumberAnimation { duration: 200 }
        }

        state: "open"
        states: [ State { name: "open" }, State { name: "closed" } ]

        onStateChanged: {
            updateX.start();
        }

        onLoaded: {
            updateX.start();
            sideMenu.loaded();
        }

        onWidthChanged: updateX.start();

        function setXforState() {
            menuLoader.x = menuLoader.state === "open" ? menuLoader.openX : menuLoader.closedX;
        }
    }

    Item {
        anchors {
            left: leftMenu ? menuLoader.right : undefined
            right: leftMenu ? undefined : menuLoader.left
            verticalCenter: menuLoader.verticalCenter
            margins: Const.pageMargin
        }
        width: parent.width / 2
        height: parent.height / 2
        visible: showMenuInfo
        Rectangle {
            anchors.fill: parent
            color: Colors.dialogBg
            radius: Const.pageMargin / 2
        }
        Text {
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                bottom: dismissButton.top
                margins: Const.pageMargin * 2
            }
            color: Colors.dialogText
            font.pixelSize: Const.fontPxSizeM
            font.bold: true
            style: Text.Outline;
            styleColor: Colors.dialogTextOutline
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Note: You can open this menu by swiping it open from the side")
        }
        MouseArea { anchors.fill: parent }
        MenuDelegate {
            id: dismissButton
            anchors {
                right: parent.right
                bottom: parent.bottom
                margins: Const.pageMargin * 2
                rightMargin: Const.pageMargin * 4
            }
            icon: "qrc:/Ui/OkButton.png"
            text: qsTr("OK")
            onClicked: {
                sideMenu.menuInfoAcknowledged()
            }
        }
    }

    Timer {
        id: updateX
        interval: 1
        onTriggered: {
            menuLoader.setXforState();
        }
    }
}
