import QtQuick
import VirtualCardDeck
import "./"

Item {
    id: menuDelegate

    property alias icon: iconItem.source
    property alias text: textItem.text
    property bool dragConfirm: false

    height: Const.fontPxSizeM * 2
    width: Const.fontPxSizeM * 2 + textItem.width + Const.pageMargin


    signal clicked()
    signal dragConfirmed()

    Image {
        id: iconItem
        anchors {
            top: parent.top
            left: parent.left
            bottom: parent.bottom
        }
        width: height
        opacity: mArea.pressed ? 0.7 : 1
    }

    Text {
        id: textItem
        anchors {
            top: parent.top
            left: iconItem.right
            bottom: parent.bottom
            leftMargin: Const.pageMargin
        }
        opacity: mArea.pressed ? 0.7 : 1
        font.pixelSize: Const.fontPxSizeM
        font.bold: true
        color: Colors.dialogText
        style: Text.Outline;
        styleColor: Colors.dialogTextOutline
        verticalAlignment: Text.AlignVCenter
    }

    Image {
        id: arrowItem
        anchors {
            top: parent.top
            right: dragConfirm && mArea.pressed ? undefined : iconItem.right
            bottom: parent.bottom
        }
        width: height
        source: "qrc:/Ui/RightArrow.png"
        visible: mArea.pressed && dragConfirm
    }

    Image {
        id: okImage
        anchors {
            top: parent.top
            left: parent.left
            bottom: parent.bottom
        }
        width: height
        visible: arrowItem.x > (parent.width /2)
        source: "qrc:/Ui/OkButton.png"
    }

    MouseArea {
        id: mArea
        anchors.fill: parent

        drag.target: dragConfirm ? arrowItem : undefined
        drag.axis: Drag.XAxis
        drag.minimumX: 0
        drag.maximumX: menuDelegate.width - arrowItem.width

        onClicked: menuDelegate.clicked()
        onReleased: {
            if (arrowItem.x > (parent.width /2)) {
                menuDelegate.dragConfirmed();
            }
        }
    }
}
