pragma Singleton
import QtQuick

Item {
    readonly property color tableBg: "green"
    readonly property color black: "#000000"
    readonly property color transparent: "#00000000"
    readonly property color pressShade: "#55555555"

    readonly property color cardBg: "white"
    readonly property color cardBorder: "black"
    readonly property color cardSelected: "#DD0000"

    readonly property color dialogShadow: "black"
    readonly property color dialogShield: "black"
    readonly property color dialogText: "white"
    readonly property color dialogTextOutline: "black"
    readonly property color dialogBg: "#EE003300"

    readonly property color editActive: "#88EEFF"
    readonly property color editInActive: "white"
    readonly property color editBg: "#33FFFFFF"
    readonly property color editText: "#FFFFFF"
}
