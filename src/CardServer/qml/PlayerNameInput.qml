import QtQuick
import VirtualCardDeck
import "./"

Item {
    id: root
    property alias text: input.text
    Column {
        anchors.centerIn: parent
        width: parent.width - Const.pageMargin * 4
        spacing: Const.pageMargin * 2
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: Const.fontPxSizeM
            text: qsTr("Enter player name:")
            color: Colors.dialogText
        }

        Rectangle {
            width: parent.width
            height: input.height + Const.pageMargin * 4
            border.color: input.activeFocus ? Colors.editActive : Colors.editInActive
            border.width: 2
            color: Colors.editBg
            radius: Const.pageMargin

            TextInput {
                id: input
                anchors.centerIn: parent
                width: parent.width - Const.pageMargin * 4
                font.pixelSize: Const.fontPxSizeL
                color: Colors.editText
                text: gameManager.playerName
            }
        }
    }
}
