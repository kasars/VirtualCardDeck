import QtQuick
import "./"

DeckPopTop {
    id: spreadRightDeck
    cardOffsetX: width * 0.012
    cardOffsetY: 0

    topCardOffsetX: width * 0.1
    topCardOffsetY: 0
}
