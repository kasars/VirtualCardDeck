import QtQuick
import VirtualCardDeck
import "./"


Item {
    id: root

    property alias source: dialogLoader.source
    property alias sourceComponent: dialogLoader.sourceComponent
    property alias item: dialogLoader.item
    property alias status: dialogLoader.status
    property alias text: dialogText.text

    property variant callbackItem: undefined

    property int buttons: 3

    readonly property int okButton: 1
    readonly property int cancelButton: 2
    readonly property int yesButton: 4
    readonly property int noButton: 8
    readonly property int customAcceptButton: 16
    readonly property int customCancelButton: 32

    opacity: 0
    visible: opacity > 0.00001;

    signal loaded()
    signal accepted()
    signal canceled()

    Connections {
        target: VCDPopup

        function onRequestOpenDialog(text, buttons, callback) {
            root.openDialog(text, buttons, callback);
        }

        function onRequestComponentDialog(source, buttons, callback) {
            root.openComponentDialog(source, buttons, callback);
        }
    }

    function close() {
        dialogText.text = "";
        root.buttons = 0;
        root.callbackItem = undefined;
        dialogLoader.source = "";
        root.state = "closed";
    }

    function cancel() {
        if (typeof callbackItem !== "undefined" &&
            typeof callbackItem.canceled == "function")
        {
            callbackItem.canceled(dialogLoader.item);
        }
        else {
            root.canceled();
        }
        root.close();
    }

    function accept() {
        if (typeof callbackItem !== "undefined" &&
            typeof callbackItem.accepted == "function")
        {
            callbackItem.accepted(dialogLoader.item);
        }
        else {
            root.accepted();
        }
        root.close();
    }

    function openDialog(text, buttons, callback) {
        dialogText.text = text;
        root.buttons = buttons ? buttons : 3

        root.callbackItem = callback;
        root.state = "open";
    }

    function openComponentDialog(source, buttons, callback) {
        dialogLoader.source = source;
        root.buttons = buttons ? buttons : 3
        root.callbackItem = callback;
        root.state = "open";
    }

    function openCustomButtonDialog(text, acceptText, cancelText, callback) {
        dialogText.text = text;
        var buttonMask = 0;
        if (acceptText !== "") {
            customBtnAccept.text = acceptText;
            buttonMask |= customAcceptButton;
        }
        if (cancelText !== "") {
            customBtnCancel.text = cancelText;
            buttonMask |= customCancelButton;
        }
        root.buttons = buttonMask;

        root.callbackItem = callback;
        root.state = "open";
    }


    Behavior on opacity {
        NumberAnimation { duration: 150 }
    }
    state: "closed"
    states: [
        State { name: "open"; PropertyChanges { root.opacity: 1.0 } },
        State { name: "closed"; PropertyChanges { cardItem.scale: 0.1 } }
    ]

    Rectangle {
        id: backgroundShield
        anchors.fill: parent
        color: Colors.black
        opacity: 0.3
    }

    MouseArea {
        anchors.fill: parent
        onClicked: root.cancel();
    }

    Rectangle {
        id: cardItem
        anchors.centerIn: parent
        height: Math.max(parent.height * 0.6, contentHeight - Const.pageMargin * 4) + verticalReserved
        width: Math.max(parent.width * 0.6, contentWidth - Const.pageMargin * 4) + horizontalReserved
        radius: Const.pageMargin
        color: Colors.dialogBg

        readonly property double contentImplicitWidth: dialogLoader.status === Loader.Ready ? dialogLoader.item.implicitWidth : 0
        readonly property double contentImplicitHeight: dialogLoader.status === Loader.Ready ? dialogLoader.item.implicitHeight : 0

        readonly property double horizontalReserved: Const.pageMargin * 4
        readonly property double verticalReserved: Const.pageMargin * 10

        readonly property double contentWidth: Math.min(contentImplicitWidth, root.width - horizontalReserved)
        readonly property double contentHeight: Math.min(contentImplicitHeight, root.height - verticalReserved)

        Behavior on scale {
            NumberAnimation { duration: 150 }
        }

        Text {
            id: dialogText
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                bottom: buttonRow.top
                margins: Const.pageMargin * 2
            }
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: Const.fontPxSizeM
            font.bold: true
            wrapMode: Text.WordWrap
            color: Colors.dialogText
        }

        MouseArea { anchors.fill: parent }

        Loader {
            id: dialogLoader
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                bottom: buttonRow.top
                margins: Const.pageMargin * 2
            }
            onLoaded: {
                root.loaded();
            }
        }

        Row {
            id: buttonRow
            anchors {
                bottom: parent.bottom
                right: parent.right
                margins: Const.pageMargin * 2
                rightMargin: Const.pageMargin * 4
            }
            spacing: Const.pageMargin * 4

            MenuDelegate {
                id: customBtnAccept
                icon: "qrc:/Ui/OkButton.png"
                text: qsTr("Yes")
                onClicked: root.accept()
                visible: (root.buttons & VCDPopup.customAcceptButton) !== 0
            }
            MenuDelegate {
                id: customBtnCancel
                icon: "qrc:/Ui/OkButton.png"
                text: qsTr("No")
                onClicked: root.cancel()
                visible: (root.buttons & VCDPopup.customCancelButton) !== 0
            }
            MenuDelegate {
                icon: "qrc:/Ui/OkButton.png"
                text: qsTr("Yes")
                onClicked: root.accept()
                visible: (root.buttons & VCDPopup.yesButton) !== 0
            }
            MenuDelegate {
                icon: "qrc:/Ui/QuitButton.png"
                text: qsTr("No")
                onClicked: root.cancel()
                visible: (root.buttons & VCDPopup.noButton) !== 0
            }
            MenuDelegate {
                icon: "qrc:/Ui/OkButton.png"
                text: qsTr("OK")
                onClicked: root.accept()
                visible: (root.buttons & VCDPopup.okButton) !== 0
            }
            MenuDelegate {
                icon: "qrc:/Ui/QuitButton.png"
                text: qsTr("Cancel")
                onClicked: root.cancel()
                visible: (root.buttons & VCDPopup.cancelButton) !== 0
            }
        }
    }
}
