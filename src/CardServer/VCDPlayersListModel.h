#pragma once

#include "VCDPlayer.h"

#include <QAbstractListModel>
#include <QObject>

class VCDPlayersListModel : public QAbstractListModel {
    Q_OBJECT

public:
    enum Roles {
        NameRole = Qt::UserRole + 1,
        ReadyRole,
        ServerIdRole,
        SortIndexRole,
    };
    Q_ENUM(Roles)

    explicit VCDPlayersListModel(QObject *parent = nullptr);

    void setPlayers(const VCDPlayerList &players);

public:
    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    QList<VCDPlayerInfo> m_players;
};
