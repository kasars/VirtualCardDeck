#pragma once

#include "VCDCard.h"
#include "VCDDeckInventory.h"
#include "VCDGameInfo.h"
#include "VCDPlayer.h"

#include "VCDDeckDataModel.h"
#include "VCDDeckInfoModel.h"
#include "VCDGameListModel.h"
#include "VCDPlayersListModel.h"

#include <QObject>

class ClientGameManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QObject* playersModel READ playersModel CONSTANT)
    Q_PROPERTY(QObject* gamesModel READ gamesModel CONSTANT)
    Q_PROPERTY(QObject* deckInfoModel READ deckInfoModel CONSTANT)
    Q_PROPERTY(int dummyDeckCount READ dummyDeckCount NOTIFY dummyDeckCountChanged)

    Q_PROPERTY(QString playerName READ playerName WRITE setPlayerName NOTIFY playerNameChanged)
    Q_PROPERTY(bool playerReady READ playerReady WRITE setPlayerReady NOTIFY playerReadyChanged)
    Q_PROPERTY(QString mePlayerId READ mePlayerId NOTIFY mePlayerIdChanged)

public:
    explicit ClientGameManager(QObject *parent = nullptr);

    QObject *playersModel();
    QObject *gamesModel();
    QObject *deckInfoModel();
    Q_INVOKABLE const QObject *deckDataModel(int deckId, int dummyCount) const;
    int dummyDeckCount() const;

    QString playerName() const;
    void setPlayerName(const QString &name);

    bool playerReady() const;
    void setPlayerReady(bool ready);

    QString mePlayerId() const;

    Q_INVOKABLE void selectGame(int index);
    Q_INVOKABLE void startGame();

public Q_SLOTS:
    void connectedToGameServer();
    void onGameList(const VCDGameList &list);
    void onGameSelection(int listIndex);
    void onPlayerInfo(const VCDPlayerInfo &player);
    void onPlayerList(const VCDPlayerList &list);
    void onGameDetails(const VCDGameDetails &game);
    void onDeckInventory(const VCDDeckInventory &list);
    void onCardMoved(const VCDCardAndPosition &source, const VCDCardAndPosition &target);
    void onReserveOK();
    void onReserveBlocked(const QList<uint64_t> &players);
    void onStartFailed();
    void onReservedDecks(const QList<int> &deckIndexes);

Q_SIGNALS:
    void dummyDeckCountChanged(); // used only to update deckDataModel bindings
    void playerNameChanged();
    void playerReadyChanged();
    void mePlayerIdChanged();

    void reserveMove(const VCDCardAndPosition &source, const VCDCardAndPosition &target);
    void requestMove(const VCDCardAndPosition &source, const VCDCardAndPosition &target);
    void requestUndoLast();
    void sendPlayerInfo(const VCDPlayerInfo &player);
    void sendSelectGame(int index);
    void sendStartGame();
    void sendClearGame();

private:
    VCDGameListModel m_gamesModel;
    VCDDeckInfoModel m_deckInfoModel;
    std::vector<std::unique_ptr<VCDDeckDataModel>> m_deckDataModels;
    VCDPlayersListModel m_players;
    VCDPlayerInfo m_player;
};
