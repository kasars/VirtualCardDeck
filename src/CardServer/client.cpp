// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: LGPL-2.0-or-later

#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QTimer>

#include "ClientComManager.h"
#include "VCDB64ImageProvider.h"

#include "FilterProxyModel.h"
#include "SortProxyModel.h"
#include "VCDGameListModel.h"

using namespace Qt::Literals::StringLiterals;

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QCoreApplication::setOrganizationName("Sars");
    QCoreApplication::setApplicationName("VirtualCardDeck-NG");

    QQmlApplicationEngine engine;
    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreationFailed,
        &app,
        []() {
            QCoreApplication::exit(-1);
        },
        Qt::QueuedConnection);

    ClientComManager manager;

    qmlRegisterUncreatableType<VCDGameListModel>("VirtualCardDeck",
                                                 1,
                                                 0,
                                                 "VCDGameListModel",
                                                 "VCDGameListModel is non-creatable");

    qmlRegisterUncreatableType<VCDDeckInfoModel>("VirtualCardDeck",
                                                 1,
                                                 0,
                                                 "VCDDeckInfoModel",
                                                 "VCDDeckInfoModel is non-creatable");

    qmlRegisterUncreatableType<VCDDeckDataModel>("VirtualCardDeck",
                                                 1,
                                                 0,
                                                 "VCDDeckDataModel",
                                                 "VCDDeckDataModel is non-creatable");

    qmlRegisterUncreatableType<VCDPlayersListModel>("VirtualCardDeck",
                                                    1,
                                                    0,
                                                    "VCDPlayersListModel",
                                                    "VCDPlayersListModel is non-creatable");

    qmlRegisterType<SortProxyModel>("VirtualCardDeck", 1, 0, "SortProxyModel");
    qmlRegisterType<FilterProxyModel>("VirtualCardDeck", 1, 0, "FilterProxyModel");

    engine.rootContext()->setContextProperty("gameManager", manager.gameManager());
    engine.addImageProvider(u"base64"_s, new VCDB64ImageProvider());
    engine.loadFromModule("VirtualCardDeck", "Main");

    return app.exec();
}
