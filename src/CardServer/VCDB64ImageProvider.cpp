#include "VCDB64ImageProvider.h"

VCDB64ImageProvider::VCDB64ImageProvider()
: QQuickImageProvider(QQuickImageProvider::Pixmap)
{
}

QPixmap VCDB64ImageProvider::requestPixmap(const QString &base64Data, QSize *size, const QSize &requestedSize)
{
    QByteArray imageData = QByteArray::fromBase64(base64Data.toUtf8());
    QImage image = QImage::fromData(imageData);

    if (size) {
        *size = image.size();
    }

    if (requestedSize.isValid()) {
        image = image.scaled(requestedSize);
    }
    return QPixmap::fromImage(image);
}
