// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "CardServer.h"

#include <QByteArray>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

CardServer::CardServer(QObject *parent)
    : QObject(parent)
{
    connect(&m_tcpServer, &QTcpServer::newConnection, this, &CardServer::onNewConnection);
}

void CardServer::startServer(int port)
{
    if (!m_tcpServer.listen(QHostAddress::Any, port)) {
        qWarning() << "Failed to start server" << m_tcpServer.errorString();
    }
}

void CardServer::onNewConnection()
{
    while (m_tcpServer.hasPendingConnections()) {
        // qDebug() << "player wants to join";
        QTcpSocket *clientConnection = m_tcpServer.nextPendingConnection();
        connect(clientConnection, &QAbstractSocket::disconnected, clientConnection, &QObject::deleteLater);
        connect(clientConnection, &QTcpSocket::destroyed, this, &CardServer::onConnectionDestroyed);
        connect(clientConnection, &QIODevice::readyRead, this, &CardServer::onReadReady);
        m_clientMap.insert(clientConnection, {"", false, (uint64_t)clientConnection});
    }
}

void CardServer::onErrorOccurred(QAbstractSocket::SocketError socketError)
{
    qWarning() << socketError;
}

void CardServer::onStateChanged(QAbstractSocket::SocketState state)
{
    qDebug() << state;
}

void CardServer::onConnectionDestroyed(QObject *obj)
{
    // When we get here the object is not a valid QTcpSocket any more, but we only remove it,
    // so we force it to QTcpSocket*
    QTcpSocket *client = static_cast<QTcpSocket *>(obj);

    if (m_clientMap.contains(client)) {
        Q_EMIT playerLeft(m_clientMap.value(client));
        qDebug() << "Client connection closed:" << m_clientMap.value(client);
        m_clientMap.remove(client);
    }
    else {
        qWarning() << "unknown player closed connection";
    }
}

void CardServer::onReadReady()
{
    QTcpSocket *client = qobject_cast<QTcpSocket *>(sender());
    if (!client) {
        qWarning() << "Sender is not a QTcpSocket";
        return;
    }
    while (client->canReadLine()) {
        QByteArray jsonData = client->readLine();
        QJsonParseError error;
        QJsonDocument doc(QJsonDocument::fromJson(jsonData, &error));
        QJsonObject root = doc.object();
        parseClientJson(client, root);
    }
}

void CardServer::sendToAll(const QJsonObject &obj)
{
    QJsonDocument doc(obj);
    QByteArray data = doc.toJson(QJsonDocument::Compact) + "\n";
    for (auto client : m_clientMap.keys()) {
        // qDebug() << "Sending to player:" << data;
        client->write(data);
    }
}

void CardServer::sendToPlayer(const VCDPlayerInfo &player, const QJsonObject &obj)
{
    QTcpSocket *client = (QTcpSocket *)player.serverPlayerId;
    if (!m_clientMap.contains(client)) {
        qWarning() << "Failed to find player";
        return;
    }
    QJsonDocument doc(obj);
    QByteArray data = doc.toJson(QJsonDocument::Compact) + "\n";
    // qDebug() << "Sending data:" << data;
    client->write(data);
}

void CardServer::sendGameList(const VCDGameList &list)
{
    QJsonObject root;
    root["cmd"] = "GameList";
    QJsonArray array;
    for (const auto &game : list) {
        QJsonObject obj;
        obj["Name"] = game.name;
        obj["NumPlayers"] = game.playerCount;
        obj["Base64QImageData"] = game.base64QImageData;
        array.append(obj);
    }
    root["Games"] = array;
    sendToAll(root);
}

void CardServer::sendGameSelection(int listIndex)
{
    QJsonObject root;
    root["cmd"] = "SelectedGame";
    root["SelectedGame"] = listIndex;
    sendToAll(root);
}

void CardServer::sendGameDetails(const VCDGameDetails &game)
{
    QJsonObject root;
    root["cmd"] = "GameDetails";
    root["ShowAllPlayerHands"] = game.showAllPlayerHands;

    root["MeShowRows"] = game.meShowRows;
    root["MeShowColumns"] = game.meShowColumns;
    root["OthersShowRows"] = game.othersShowRows;
    root["OthersShowColumns"] = game.othersShowColumns;
    root["PlayerRows"] = game.playerRows;
    root["PlayerColumns"] = game.playerColumns;
    root["CommonRows"] = game.commonRows;
    root["CommonColumns"] = game.commonColumns;

    root["MeSpace"] = game.meSpace;
    root["CommonSpace"] = game.commonSpace;
    root["OpponentSpace"] = game.opponentSpace;
    root["PlayerCount"] = game.info.playerCount;
    root["Name"] = game.info.name;
    root["Base64QImageData"] = game.info.base64QImageData;

    QJsonArray decks;
    for (const auto &deck : game.decks) {
        QJsonObject obj;
        obj["OwnerId"] = QString::number(deck.ownerId);
        obj["Row"] = deck.row;
        obj["RowSpan"] = deck.rowSpan;
        obj["Column"] = deck.column;
        obj["ColumnSpan"] = deck.columnSpan;
        obj["DeckType"] = deck.type;
        obj["FaceUp"] = deck.faceUp; // FIXME visible to???
        obj["AllowedValue"] = deck.allowedValue;
        obj["AllowedFamily"] = deck.allowedFamily;
        obj["AllowMultiple"] = deck.allowMultiple;
        obj["PullAllowed"] = deck.pullAllowed;
        obj["AllowAutoPull"] = deck.allowAutoPull;
        obj["DealCount"] = deck.dealCount;
        obj["TopDealCount"] = deck.topDealCount;
        obj["DeckIdBase"] = deck.deckIdBase;
        obj["DeckIdTop"] = deck.deckIdTop;
        obj["DeckIdMove"] = deck.deckIdMove;
        decks.append(obj);
    }
    root["GameDecks"] = decks;
    sendToAll(root);
}

void CardServer::sendPlayerInfo(const VCDPlayerInfo &player)
{
    QJsonObject root;
    root["cmd"] = "Player";
    QJsonObject obj;
    obj["ServerId"] = QString::number(player.serverPlayerId);
    obj["Name"] = player.name;
    obj["Ready"] = player.ready;
    root["Player"] = obj;
    sendToPlayer(player, root);
}

void CardServer::sendPlayerList(const VCDPlayerList &list)
{
    QJsonObject root;
    root["cmd"] = "Players";
    QJsonArray array;
    for (const auto &player : list) {
        QJsonObject obj;
        obj["ServerId"] = QString::number(player.serverPlayerId);
        obj["Name"] = player.name;
        obj["Ready"] = player.ready;
        obj["SortIndex"] = player.sortIndex;
        array.append(obj);
    }
    root["Players"] = array;
    sendToAll(root);
}

void CardServer::sendDeckInventory(const VCDDeckInventory &inventory)
{
    QJsonObject root;
    root["cmd"] = "DealCards";
    QJsonArray deckArray;
    for (const auto &deck : inventory.decks) {
        QJsonArray cardArray;
        for (const auto &cardId : deck.cardIds) {
            cardArray.append(cardId);
        }
        deckArray.append(cardArray);
    }
    root["Decks"] = deckArray;
    sendToAll(root);
}

void CardServer::sendCardMoved(const VCDCardAndPosition &source, const VCDCardAndPosition &target)
{
    QJsonObject root;
    root["cmd"] = "CardMove";
    root["SourceCardId"] = source.cardId;
    root["SourceDeck"] = source.deckIndex;
    root["SourceInDeckIndex"] = source.inDeckIndex;
    root["TargetCardId"] = target.cardId;
    root["TargetDeck"] = target.deckIndex;
    root["TargetInDeckIndex"] = target.inDeckIndex;
    sendToAll(root);
}

void CardServer::sendReserveOK(const VCDPlayerInfo &player)
{
    QJsonObject root;
    root["cmd"] = "ReserveOK";
    sendToPlayer(player, root);
}

void CardServer::sendStartFailed()
{
    QJsonObject root;
    root["cmd"] = "StartFailed";
    sendToAll(root);
}

void CardServer::sendReserveBlocked(const VCDPlayerInfo &player, const QList<uint64_t> &players)
{
    QJsonObject root;
    root["cmd"] = "ReserveBlocked";
    QJsonArray array;
    for (const auto &p : players) {
        array.append(QString::number(p));
    }
    root["BlockedBy"] = array;
    sendToPlayer(player, root);
}

void CardServer::sendReservedDecks(const QList<int>& deckIndexes)
{
    QJsonObject root;
    root["cmd"] = "ReservedDecks";
    QJsonArray array;
    for (const auto &index : deckIndexes) {
        array.append(index);
    }
    root["ReservedDecks"] = array;
    sendToAll(root);
}

void CardServer::parseClientJson(QTcpSocket *client, const QJsonObject &root)
{
    QString cmd = root["cmd"].toString();
    if (cmd == "ReserveCardMove") {
        VCDCardAndPosition source;
        VCDCardAndPosition target;
        source.cardId = root["SourceCardId"].toInt();
        source.deckIndex = root["SourceDeck"].toInt();
        source.inDeckIndex = root["SourceInDeckIndex"].toInt();
        target.cardId = root["TargetCardId"].toInt();
        target.deckIndex = root["TargetDeck"].toInt(-1);
        target.inDeckIndex = root["TargetInDeckIndex"].toInt(-1);
        Q_EMIT reserveMove(m_clientMap[client], source, target);
    }
    else if (cmd == "RequestCardMove") {
        VCDCardAndPosition source;
        VCDCardAndPosition target;
        source.cardId = root["SourceCardId"].toInt();
        source.deckIndex = root["SourceDeck"].toInt();
        source.inDeckIndex = root["SourceInDeckIndex"].toInt();
        target.cardId = root["TargetCardId"].toInt();
        target.deckIndex = root["TargetDeck"].toInt(-1);
        target.inDeckIndex = root["TargetInDeckIndex"].toInt(-1);
        Q_EMIT requestMove(m_clientMap[client], source, target);
    }
    else if (cmd == "UndoLast") {
        Q_EMIT requestUndoLast();
    }
    else if (cmd == "UpdatePlayer") {
        VCDPlayerInfo &player = m_clientMap[client];
        player.serverPlayerId = (uint64_t)client;
        player.name = root["Name"].toString();
        player.ready = root["Ready"].toBool(false);
        Q_EMIT updatePlayerInfo(player);
    }
    else if (cmd == "SelectedGame") {
        Q_EMIT selectGame(root["SelectedGame"].toInt(0));
    }
    else if (cmd == "StartGame") {
        Q_EMIT startGame();
    }
    else if (cmd == "ClearGame") {
        Q_EMIT clearGame();
    }
}
