// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "ServerComManager.h"

SeverComManager::SeverComManager(QObject *parent)
    : QObject(parent)
{
    connect(&m_server, &CardServer::reserveMove, &m_gameManager, &GameManager::onReserveMove);
    connect(&m_server, &CardServer::requestMove, &m_gameManager, &GameManager::onRequestMove);
    connect(&m_server, &CardServer::requestUndoLast, &m_gameManager, &GameManager::onRequestUndoLast);
    connect(&m_server, &CardServer::updatePlayerInfo, &m_gameManager, &GameManager::onUpdatePlayerInfo);
    connect(&m_server, &CardServer::selectGame, &m_gameManager, &GameManager::onSelectGame);
    connect(&m_server, &CardServer::startGame, &m_gameManager, &GameManager::onStartGame);
    connect(&m_server, &CardServer::clearGame, &m_gameManager, &GameManager::onClearGame);
    connect(&m_server, &CardServer::playerLeft, &m_gameManager, &GameManager::onPlayerLeft);

    connect(&m_gameManager, &GameManager::sendGameList, &m_server, &CardServer::sendGameList);
    connect(&m_gameManager, &GameManager::sendGameSelection, &m_server, &CardServer::sendGameSelection);
    connect(&m_gameManager, &GameManager::sendPlayerInfo, &m_server, &CardServer::sendPlayerInfo);
    connect(&m_gameManager, &GameManager::sendPlayerList, &m_server, &CardServer::sendPlayerList);
    connect(&m_gameManager, &GameManager::sendGameDetails, &m_server, &CardServer::sendGameDetails);
    connect(&m_gameManager, &GameManager::sendDeckInventory, &m_server, &CardServer::sendDeckInventory);
    connect(&m_gameManager, &GameManager::sendCardMoved, &m_server, &CardServer::sendCardMoved);
    connect(&m_gameManager, &GameManager::sendReserveOK, &m_server, &CardServer::sendReserveOK);
    connect(&m_gameManager, &GameManager::sendReserveBlocked, &m_server, &CardServer::sendReserveBlocked);
    connect(&m_gameManager, &GameManager::sendReservedDecks, &m_server, &CardServer::sendReservedDecks);
    connect(&m_gameManager, &GameManager::sendStartFailed, &m_server, &CardServer::sendStartFailed);

    m_server.startServer(23174);
}

