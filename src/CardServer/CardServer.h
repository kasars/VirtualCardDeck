// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include "VCDCard.h"
#include "VCDGameInfo.h"
#include "VCDDeckInventory.h"
#include "VCDPlayer.h"

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>

class CardServer : public QObject
{
    Q_OBJECT
public:
    explicit CardServer(QObject *parent = nullptr);

    void startServer(int port);

    void sendGameList(const VCDGameList &list);
    void sendGameSelection(int listIndex);
    void sendPlayerInfo(const VCDPlayerInfo &player);
    void sendPlayerList(const VCDPlayerList &list);
    void sendGameDetails(const VCDGameDetails &game);
    void sendDeckInventory(const VCDDeckInventory &list);
    void sendCardMoved(const VCDCardAndPosition &source, const VCDCardAndPosition &target);

    // Responses to requests
    void sendReserveOK(const VCDPlayerInfo &player);
    void sendReserveBlocked(const VCDPlayerInfo &player, const QList<uint64_t> &players);
    void sendStartFailed();
    void sendReservedDecks(const QList<int>& deckIndexes);

private Q_SLOTS:
    void onNewConnection();
    void onErrorOccurred(QAbstractSocket::SocketError socketError);
    void onStateChanged(QAbstractSocket::SocketState state);

    void onConnectionDestroyed(QObject *obj);

    void onReadReady();

Q_SIGNALS:
    void reserveMove(const VCDPlayerInfo &player, const VCDCardAndPosition &source, const VCDCardAndPosition &target);
    void requestMove(const VCDPlayerInfo &player, const VCDCardAndPosition &source, const VCDCardAndPosition &target);
    void requestUndoLast();
    void updatePlayerInfo(const VCDPlayerInfo &player);
    void selectGame(int index);
    void startGame();
    void clearGame();

    void playerLeft(const VCDPlayerInfo &player);

protected:
    virtual void sendToAll(const QJsonObject &obj);
    virtual void sendToPlayer(const VCDPlayerInfo &player, const QJsonObject &obj);

private:
    void parseClientJson(QTcpSocket *client, const QJsonObject &obj);
    QTcpServer m_tcpServer;
    QMap<QTcpSocket*, VCDPlayerInfo> m_clientMap;

    friend class GameManagerTest;
};
