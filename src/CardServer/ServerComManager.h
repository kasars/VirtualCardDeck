// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <QObject>
#include "CardServer.h"
#include "GameManager.h"

class SeverComManager : public QObject
{
    Q_OBJECT
public:
    explicit SeverComManager(QObject *parent = nullptr);

private:
    CardServer m_server;
    GameManager m_gameManager;
};
