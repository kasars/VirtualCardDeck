// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "GameManager.h"

#include <QDir>
#include <QFileInfoList>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QRandomGenerator>

using namespace Qt::Literals::StringLiterals;

GameManager::GameManager(QObject *parent)
    : QObject(parent)
{
    readGamesInfo();
}

GameManager::GameInfo GameManager::readGameInfo(const QString &fileName)
{
    GameInfo info;

    QFile gameInfo(fileName);
    if (!gameInfo.open(QIODevice::ReadOnly)) {
        qDebug() << "Failed to read" << fileName;
        return info;
    }

    QJsonParseError error;
    QJsonDocument confDoc(QJsonDocument::fromJson(gameInfo.readAll(), &error));
    QJsonObject root = confDoc.object();

    if (root.isEmpty()) {
        qDebug() << "Failed to read configuration" << error.errorString() << error.offset;
        qDebug() << confDoc << fileName;
        return info;
    }

    info.fileName = fileName;
    info.gameInfo.name = root["name"].toString();
    info.gameInfo.playerCount = root["playerCount"].toInt();
    QString iconFile = ":/Images/" + root["icon"].toString();
    QFile icon(iconFile);
    if (icon.open(QIODevice::ReadOnly)) {
        info.gameInfo.base64QImageData = icon.readAll().toBase64();
    }
    else {
        qDebug() << "Failed to read" << iconFile << icon.errorString();
    }
    return info;
}

void GameManager::readGamesInfo()
{
    m_gameList.clear();
    // Search for built-in games in ":/Games"
    QDir qrcDir(u":/Games"_s);
    const QFileInfoList games = qrcDir.entryInfoList(QDir::Files | QDir::Readable, QDir::Name | QDir::LocaleAware);
    for (const auto &fileInfo : games) {
        m_gameList.append(readGameInfo(fileInfo.filePath()));
    }
}

VCDGameList GameManager::toGameList(const QList<GameInfo> &games)
{
    VCDGameList list;
    for (const auto &game : games) {
        list.append(game.gameInfo);
    }
    return list;
}

void GameManager::loadGame(const QString &fileName)
{
    QFile gameInfo(fileName);
    if (!gameInfo.open(QIODevice::ReadOnly)) {
        qDebug() << "Failed to read" << fileName;
        return;
    }

    QJsonParseError error;
    QJsonDocument confDoc(QJsonDocument::fromJson(gameInfo.readAll(), &error));
    QJsonObject root = confDoc.object();

    if (root.isEmpty()) {
        qDebug() << "Failed to read configuration" << error.errorString() << error.offset;
        qDebug() << confDoc << fileName;
        return;
    }

    m_gameDetails.info.name = root["name"].toString();
    m_gameDetails.info.playerCount = root["playerCount"].toInt();
    m_gameDetails.showAllPlayerHands = root["showAllPlayerHands"].toBool();
    m_gameDetails.meSpace = root["meSpace"].toDouble();
    m_gameDetails.commonSpace = root["commonSpace"].toDouble();
    m_gameDetails.opponentSpace = root["opponentSpace"].toDouble();

    m_gameDetails.meShowRows = root["meShowRows"].toDouble();
    m_gameDetails.meShowColumns = root["meShowColumns"].toDouble();
    m_gameDetails.othersShowRows = root["othersShowRows"].toDouble();
    m_gameDetails.othersShowColumns = root["othersShowColumns"].toDouble();

    QJsonObject pDecks = root["playerDecks"].toObject();
    m_gameDetails.playerRows = pDecks["rows"].toInt();
    m_gameDetails.playerColumns = pDecks["columns"].toInt();
    m_gameDetails.decks.clear();
    m_gameDecks.decks.clear();
    int deckIndex = 0;
    QJsonArray array = pDecks["decks"].toArray();
    for (const auto &arrVal : std::as_const(array)) {
        QJsonObject deck = arrVal.toObject();
        VCDDeckInfo info;
        info.row = deck["row"].toInt();
        info.column = deck["column"].toInt();
        info.rowSpan = deck["rowSpan"].toInt();
        info.columnSpan = deck["columnSpan"].toInt();
        info.type = deck["type"].toString();
        info.faceUp = deck["faceUp"].toBool();
        info.allowedValue = deck["allowedValue"].toString();
        info.allowedFamily = deck["allowedFamily"].toString();
        info.allowMultiple = deck["allowMultiple"].toBool();
        info.pullAllowed = deck["pullAllowed"].toBool();
        info.allowAutoPull = deck["allowAutoPull"].toBool();
        info.dealCount = deck["dealCount"].toInt(0);
        info.topDealCount = deck["topDealCount"].toInt(0);
        for (const auto &player : std::as_const(m_players)) {
            if (!player.ready) {
                continue;
            }
            info.ownerId = player.serverPlayerId;
            info.deckIdBase = deckIndex++;
            info.deckIdTop = deckIndex++;
            info.deckIdMove = deckIndex++;
            m_gameDetails.decks.append(info);
        }
    }

    QJsonObject cDecks = root["commonDecks"].toObject();
    m_gameDetails.commonRows = cDecks["rows"].toInt();
    m_gameDetails.commonColumns = cDecks["columns"].toInt();
    array = cDecks["decks"].toArray();
    for (const auto &arrVal : std::as_const(array)) {
        QJsonObject deck = arrVal.toObject();
        VCDDeckInfo info;
        info.row = deck["row"].toInt();
        info.column = deck["column"].toInt();
        info.rowSpan = deck["rowSpan"].toInt();
        info.columnSpan = deck["columnSpan"].toInt();
        info.type = deck["type"].toString();
        info.faceUp = deck["faceUp"].toBool();
        info.allowedValue = deck["allowedValue"].toString();
        info.allowedFamily = deck["allowedFamily"].toString();
        info.allowMultiple = deck["allowMultiple"].toBool();
        info.pullAllowed = deck["pullAllowed"].toBool();
        info.allowAutoPull = deck["allowAutoPull"].toBool();
        info.dealCount = deck["dealCount"].toInt();
        info.topDealCount = deck["topDealCount"].toInt();
        info.ownerId = 0;
        info.deckIdBase = deckIndex++;
        info.deckIdTop = deckIndex++;
        info.deckIdMove = deckIndex++;
        m_gameDetails.decks.append(info);
    }
    m_gameDecks.decks.resize(m_gameDetails.decks.size() * 3);
}

void GameManager::onUpdatePlayerInfo(const VCDPlayerInfo &player)
{
    if (!m_players.contains(player.serverPlayerId)) {
        int numPlayers = m_players.size();
        m_players.insert(player.serverPlayerId, player);
        m_players[player.serverPlayerId].sortIndex = numPlayers;
        Q_EMIT sendGameList(toGameList(m_gameList));
        Q_EMIT sendGameSelection(m_selectedGameIndex);
    }
    else {
        m_players[player.serverPlayerId].name = player.name;
        m_players[player.serverPlayerId].ready = player.ready;
    }
    Q_EMIT sendPlayerInfo(m_players[player.serverPlayerId]);
    Q_EMIT sendPlayerList(m_players.values());
}

void GameManager::onSelectGame(int index)
{
    m_selectedGameIndex = index;
    Q_EMIT sendGameSelection(m_selectedGameIndex);
    qDebug() << "onSelectGame" << index;
}

void GameManager::onStartGame()
{
    // Check if we can start the game
    if (m_selectedGameIndex < 0 || m_selectedGameIndex > m_gameList.size()) {
        qWarning() << "Can't start game with invalid game index:" << m_selectedGameIndex << m_gameList.size();
        Q_EMIT sendStartFailed();
        return;
    }

    int playersReady = 0;
    for (const auto &p : m_players) {
        if (p.ready) {
            playersReady++;
        }
    }

    if (playersReady != m_gameList[m_selectedGameIndex].gameInfo.playerCount) {
        qWarning() << "Can't start game before right ammount of players have joined:" << playersReady;
        Q_EMIT sendStartFailed();
        return;
    }

    loadGame(m_gameList[m_selectedGameIndex].fileName);

    QList<int> dealDeck;
    // Create a card deck (no jacks)
    for (int i = VCDCard::Hearts; i <= VCDCard::Clubs; i++) {
        for (int j = VCDCard::Ace; j <= VCDCard::King; j++) {
            VCDCard card((VCDCard::Family)i, (VCDCard::Value)j);
            dealDeck << card.cardId();
        }
    }

    // Shuffle the deck
    QRandomGenerator *randGen = QRandomGenerator::global();
    for (int i = 0; i < dealDeck.count(); ++i) {
        int toIndex = randGen->bounded(dealDeck.count() - 1);
        dealDeck.swapItemsAt(i, toIndex);
    }

    // Deal out the cards
    for (auto &deck : m_gameDetails.decks) {
        // m_gameDecks.size() == m_gameDetails.decks.size() * 3
        if (deck.dealCount == -1) {
            continue;
            // We fill the "rest-deck" after all others
        }
        for (int i = 0; i < deck.dealCount; ++i) {
            if (dealDeck.isEmpty()) {
                qWarning() << "Deal deck is empty!";
                break;
            }
            if (deck.deckIdBase < 0 || deck.deckIdBase >= m_gameDecks.decks.size()) {
                qWarning() << "Deck index is invalid" << deck.deckIdBase << m_gameDecks.decks.size();
                break;
            }
            m_gameDecks.decks[deck.deckIdBase].cardIds.append(dealDeck.takeFirst());
        }
    }

    // Deal out to the rest cards
    for (auto &deck : m_gameDetails.decks) {
        // Fill any "deal the rest" top-deck
        if (deck.topDealCount == -1) {
            if (deck.deckIdTop < 0 || deck.deckIdTop >= m_gameDecks.decks.size()) {
                qWarning() << "Top-Deck index is invalid" << deck.deckIdTop << m_gameDecks.decks.size();
                continue;
            }
            while (!dealDeck.isEmpty()) {
                m_gameDecks.decks[deck.deckIdTop].cardIds.append(dealDeck.takeFirst());
            }
            break;
        }
        // Now try if the bottom deck is a "deal-rest" deck
        if (deck.dealCount == -1) {
            if (deck.deckIdBase < 0 || deck.deckIdBase >= m_gameDecks.decks.size()) {
                qWarning() << "Base-Deck index is invalid" << deck.deckIdBase << m_gameDecks.decks.size();
                continue;
            }
            while (!dealDeck.isEmpty()) {
                m_gameDecks.decks[deck.deckIdBase].cardIds.append(dealDeck.takeFirst());
            }
            break;
        }
    }

    Q_EMIT sendGameDetails(m_gameDetails);
    Q_EMIT sendDeckInventory(m_gameDecks);
}

void GameManager::onClearGame()
{
    m_gameDetails.clear();
    m_gameDecks.clear();
    Q_EMIT sendGameDetails(m_gameDetails);
    Q_EMIT sendDeckInventory(m_gameDecks);
}


void GameManager::onReserveMove(const VCDPlayerInfo &player,
                                const VCDCardAndPosition &source,
                                const VCDCardAndPosition &target)
{
    qDebug() << "Rese:" << player.serverPlayerId << source.deckIndex << source.inDeckIndex << target.deckIndex
             << target.inDeckIndex;
    auto reservedBy = m_gameDecks.reserveMove(player.serverPlayerId, source, target);
    if (!reservedBy) {
        Q_EMIT sendReserveBlocked(player, QList<uint64_t>());
        return;
    }

    if (!reservedBy.value().isEmpty()) {
        Q_EMIT sendReserveBlocked(player, reservedBy.value());
        return;
    }

    QList<int> reservedDecks;
    for (int i = 0; i< m_gameDecks.decks.size(); ++i) {
        if (m_gameDecks.decks[i].reservedByPlayerId != 0) {
            reservedDecks.append(i);
        }
    }
    Q_EMIT sendReservedDecks(reservedDecks);
    Q_EMIT sendReserveOK(player);
}

void GameManager::onRequestMove(const VCDPlayerInfo &player,
                                const VCDCardAndPosition &source,
                                const VCDCardAndPosition &target)
{
    auto reservedBy = m_gameDecks.moveBlocked(player.serverPlayerId, source, target);
    if (!reservedBy) {
        Q_EMIT sendReserveBlocked(player, QList<uint64_t>());
        return;
    }

    if (!reservedBy.value().isEmpty()) {
        Q_EMIT sendReserveBlocked(player, reservedBy.value());
        return;
    }
    bool ok = m_gameDecks.moveCard(source, target);
    if (!ok) {
        Q_EMIT sendReserveBlocked(player, QList<uint64_t>());
    }
    else {
        QList<int> reservedDecks;
        for (int i = 0; i< m_gameDecks.decks.size(); ++i) {
            if (m_gameDecks.decks[i].reservedByPlayerId != 0) {
                reservedDecks.append(i);
            }
        }
        Q_EMIT sendCardMoved(source, target);
        Q_EMIT sendReservedDecks(reservedDecks);
    }
}

void GameManager::onRequestUndoLast()
{
    qDebug() << "onRequestUndoLast";
}

void GameManager::onPlayerLeft(const VCDPlayerInfo &player)
{
    qDebug() << "Removing player" << player.name;
    m_players.remove(player.serverPlayerId);
}
