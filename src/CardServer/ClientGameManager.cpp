#include "ClientGameManager.h"

ClientGameManager::ClientGameManager(QObject *parent)
    : QObject(parent)
{
}

QObject *ClientGameManager::playersModel()
{
    return &m_players;
}

QObject *ClientGameManager::gamesModel()
{
    return &m_gamesModel;
}

QObject *ClientGameManager::deckInfoModel()
{
    return &m_deckInfoModel;
}

const QObject *ClientGameManager::deckDataModel(int deckId, int dummyCount) const
{
    Q_UNUSED(dummyCount)
    if (deckId < 0 || deckId >= (int)m_deckDataModels.size()) {
        return nullptr;
    }
    return m_deckDataModels[deckId].get();
}

int ClientGameManager::dummyDeckCount() const
{
    return 1;
}

QString ClientGameManager::playerName() const
{
    return m_player.name;
}

void ClientGameManager::setPlayerName(const QString &name)
{
    if (m_player.name != name) {
        m_player.name = name;
        // FIXME temporary
        m_player.ready = true;
        Q_EMIT playerReadyChanged();
        Q_EMIT playerNameChanged();
        Q_EMIT sendPlayerInfo(m_player);
    }
}

void ClientGameManager::connectedToGameServer()
{
    Q_EMIT sendPlayerInfo(m_player);
}

bool ClientGameManager::playerReady() const
{
    return m_player.ready;
}

void ClientGameManager::setPlayerReady(bool ready)
{
    if (m_player.ready != ready) {
        m_player.ready = ready;
        Q_EMIT sendPlayerInfo(m_player);
    }
}

QString ClientGameManager::mePlayerId() const
{
    return QString::number(m_player.serverPlayerId);
}

void ClientGameManager::onGameList(const VCDGameList &list)
{
    m_gamesModel.setList(list);
}

void ClientGameManager::onGameSelection(int listIndex)
{
    m_gamesModel.setSelectedIndex(listIndex);
}

void ClientGameManager::onPlayerInfo(const VCDPlayerInfo &player)
{
    m_player = player;
    qDebug() << player;
    Q_EMIT playerReadyChanged();
    Q_EMIT playerNameChanged();
    Q_EMIT mePlayerIdChanged();
}

void ClientGameManager::onPlayerList(const VCDPlayerList &list)
{
    m_players.setPlayers(list);
}

void ClientGameManager::onGameDetails(const VCDGameDetails &game)
{
    qDebug() << game;
    m_deckInfoModel.setList(game.decks);
    m_deckInfoModel.showAllPlayerHands = game.showAllPlayerHands;
    m_deckInfoModel.meShowRows = game.meShowRows;
    m_deckInfoModel.meShowColumns = game.meShowColumns;
    m_deckInfoModel.othersShowRows = game.othersShowRows;
    m_deckInfoModel.othersShowColumns = game.othersShowColumns;
    m_deckInfoModel.playerRows = game.playerRows;
    m_deckInfoModel.playerColumns = game.playerColumns;
    m_deckInfoModel.commonRows = game.commonRows;
    m_deckInfoModel.commonColumns = game.commonColumns;
    m_deckInfoModel.meSpace = game.meSpace;
    m_deckInfoModel.commonSpace = game.commonSpace;
    m_deckInfoModel.opponentSpace = game.opponentSpace;
    Q_EMIT m_deckInfoModel.gameChanged();
}

void ClientGameManager::onDeckInventory(const VCDDeckInventory &inventory)
{
    m_deckDataModels.clear();
    for (const auto &deck : inventory.decks) {
        m_deckDataModels.push_back(std::make_unique<VCDDeckDataModel>(deck));
    }
    Q_EMIT dummyDeckCountChanged();
}

void ClientGameManager::onCardMoved(const VCDCardAndPosition &source, const VCDCardAndPosition &target)
{
    if (source.deckIndex < 0 || source.deckIndex >= (int)m_deckDataModels.size()) {
        qWarning() << "Source deck index invalid:" << source.deckIndex << (int)m_deckDataModels.size();
        return;
    }
    auto &srsDeck = m_deckDataModels[source.deckIndex];
    auto cardId = srsDeck->idAt(source.inDeckIndex);
    if (!cardId) {
        qWarning() << "Source in deck index is invalid:" << source.inDeckIndex << srsDeck->rowCount();
        return;
    }

    if (source.cardId != cardId.value()) {
        qWarning() << "Source card id does not match:" << source.cardId << cardId.value();
        return;
    }

    if (target.deckIndex < 0 || target.deckIndex >= (int)m_deckDataModels.size()) {
        qWarning() << "Target deck index invalid:" << target.deckIndex << (int)m_deckDataModels.size();
        return;
    }
    auto &trgDeck = m_deckDataModels[target.deckIndex];
    if (target.inDeckIndex < 0 || target.inDeckIndex > trgDeck->rowCount()) {
        qWarning() << "Target in deck index is invalid:" << target.inDeckIndex << trgDeck->rowCount();
        return;
    }

    auto takeCard = srsDeck->takeAt(source.inDeckIndex);

    trgDeck->insertAt(target.inDeckIndex, takeCard.value());
}

void ClientGameManager::onReserveOK()
{
}

void ClientGameManager::onReserveBlocked(const QList<uint64_t> &players)
{
    qDebug() << "onReserveBlocked" << players;
}

void ClientGameManager::onStartFailed()
{
    qDebug() << "onStartFailed";
}

void ClientGameManager::onReservedDecks(const QList<int> &deckIndexes)
{
    qDebug() << "onReservedDecks";
    for (auto &deck : m_deckDataModels) {
        deck->setReserved(false);
    }
    for (int idx : deckIndexes) {
        if (idx < 0 || idx >= (int)m_deckDataModels.size()) {
            qWarning() << "Invalid deck index:" << idx << m_deckDataModels.size();
            continue;
        }
        m_deckDataModels[idx]->setReserved(true);
    }
}

void ClientGameManager::selectGame(int index)
{
    Q_EMIT sendSelectGame(index);
}

void ClientGameManager::startGame()
{
    Q_EMIT sendStartGame();
}
