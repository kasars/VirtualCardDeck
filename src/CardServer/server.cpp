// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: LGPL-2.0-or-later

#include <QCoreApplication>
#include <QDebug>
#include <QTimer>

#include "ServerComManager.h"
using namespace Qt::Literals::StringLiterals;

int main(int argc, char *argv[]) {
  QCoreApplication a(argc, argv);

  SeverComManager manager;
  return a.exec();
}
