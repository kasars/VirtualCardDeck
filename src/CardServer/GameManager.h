// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include "VCDCard.h"
#include "VCDDeckInventory.h"
#include "VCDGameInfo.h"
#include "VCDPlayer.h"
#include <QObject>

class GameManager : public QObject {
    Q_OBJECT
public:
    explicit GameManager(QObject *parent = nullptr);

public Q_SLOTS:
    void onUpdatePlayerInfo(const VCDPlayerInfo &player);
    void onSelectGame(int index);
    void onStartGame();
    void onClearGame();

    void onReserveMove(const VCDPlayerInfo &player, const VCDCardAndPosition &source, const VCDCardAndPosition &target);
    void onRequestMove(const VCDPlayerInfo &player, const VCDCardAndPosition &source, const VCDCardAndPosition &target);
    void onRequestUndoLast();

    void onPlayerLeft(const VCDPlayerInfo &player);

Q_SIGNALS:

    void sendGameList(const VCDGameList &list);
    void sendGameSelection(int listIndex);
    void sendPlayerInfo(const VCDPlayerInfo &player);
    void sendPlayerList(const VCDPlayerList &list);
    void sendGameDetails(const VCDGameDetails &details);
    void sendDeckInventory(const VCDDeckInventory &inventory);
    void sendCardMoved(const VCDCardAndPosition &source, const VCDCardAndPosition &target);

    // Responses to requests
    void sendReserveOK(const VCDPlayerInfo &player);
    void sendReserveBlocked(const VCDPlayerInfo &player, const QList<uint64_t> &players);
    void sendReservedDecks(const QList<int>& deckIndexes);
    void sendStartFailed();

private:
    struct GameInfo {
        QString fileName;
        VCDGameInfo gameInfo;
    };

    VCDGameList toGameList(const QList<GameInfo> &games);
    GameInfo readGameInfo(const QString &fileName);
    void readGamesInfo();
    void loadGame(const QString &fileName);

    QList<GameInfo> m_gameList;
    int m_selectedGameIndex = -1;
    QMap<uint64_t, VCDPlayerInfo> m_players;
    VCDGameDetails m_gameDetails;
    VCDDeckInventory m_gameDecks;
};
