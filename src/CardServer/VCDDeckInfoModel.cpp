#include "VCDDeckInfoModel.h"

#include <QDebug>

VCDDeckInfoModel::VCDDeckInfoModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

void VCDDeckInfoModel::setList(const VCDDeckList &list)
{
    if (list.isEmpty()) {
        beginResetModel();
        m_list.clear();
        endResetModel();
        return;
    }

    int rowDiff = list.size() - m_list.size();
    if (rowDiff == 0) {
        m_list = list;
        dataChanged(index(0), index(m_list.size() - 1));
    } else if (rowDiff > 0) {
        beginInsertRows(QModelIndex(), m_list.size(), list.size() - 1);
        m_list = list;
        endInsertRows();
        dataChanged(index(0), index(list.size() - 1));
    } else {
        beginRemoveRows(QModelIndex(), list.size(), m_list.size() - 1);
        m_list = list;
        endRemoveRows();
        dataChanged(index(0), index(m_list.size() - 1));
    }
}

QHash<int, QByteArray> VCDDeckInfoModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[TypeRole] = "type";
    roles[OwnerIdRole] = "ownerId";
    roles[RowRole] = "row";
    roles[RowSpanRole] = "rowSpan";
    roles[ColumnRole] = "column";
    roles[ColumnSpanRole] = "columnSpan";
    roles[FaceUpRole] = "faceUp";
    roles[AllowedValueRole] = "allowedValue";
    roles[AllowedFamilyRole] = "allowedFamily";
    roles[AllowMultipleRole] = "allowMultiple";
    roles[PullAllowedRole] = "pullAllowed";
    roles[AllowAutoPullRole] = "allowAutoPull";
    roles[DeckIdBaseRole] = "deckIdBase";
    roles[DeckIdTopRole] = "deckIdTop";
    roles[DeckIdMoveRole] = "deckIdMove";
    return roles;
}

int VCDDeckInfoModel::rowCount(const QModelIndex &) const
{
    return m_list.count();
}

QVariant VCDDeckInfoModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= m_list.count() || index.row() < 0) {
        return QVariant();
    }

    const auto &item = m_list.at(index.row());
    switch (static_cast<Roles>(role)) {
        case TypeRole:
            return item.type;
        case OwnerIdRole:
            return QString::number(item.ownerId);
        case RowRole:
            return item.row;
        case RowSpanRole:
            return item.rowSpan;
        case ColumnRole:
            return item.column;
        case ColumnSpanRole:
            return item.columnSpan;
        case FaceUpRole:
            return item.faceUp;
        case AllowedValueRole:
            return item.allowedValue;
        case AllowedFamilyRole:
            return item.allowedFamily;
        case AllowMultipleRole:
            return item.allowMultiple;
        case PullAllowedRole:
            return item.pullAllowed;
        case AllowAutoPullRole:
            return item.allowAutoPull;
        case DeckIdBaseRole:
            return item.deckIdBase;
        case DeckIdTopRole:
            return item.deckIdTop;
        case DeckIdMoveRole:
            return item.deckIdMove;
    }

    return QVariant();
}
