#pragma once

#include "VCDDeckInfo.h"

#include <QAbstractListModel>
#include <QObject>

class VCDDeckInfoModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(bool showAllPlayerHands MEMBER showAllPlayerHands NOTIFY gameChanged)

    Q_PROPERTY(double meShowRows MEMBER meShowRows NOTIFY gameChanged)
    Q_PROPERTY(double meShowColumns MEMBER meShowColumns NOTIFY gameChanged)
    Q_PROPERTY(double othersShowRows MEMBER othersShowRows NOTIFY gameChanged)
    Q_PROPERTY(double othersShowColumns MEMBER othersShowColumns NOTIFY gameChanged)

    Q_PROPERTY(int playerRows MEMBER playerRows NOTIFY gameChanged)
    Q_PROPERTY(int playerColumns MEMBER playerColumns NOTIFY gameChanged)
    Q_PROPERTY(int commonRows MEMBER commonRows NOTIFY gameChanged)
    Q_PROPERTY(int commonColumns MEMBER commonColumns NOTIFY gameChanged)

    Q_PROPERTY(double meSpace MEMBER meSpace NOTIFY gameChanged)
    Q_PROPERTY(double commonSpace MEMBER commonSpace NOTIFY gameChanged)
    Q_PROPERTY(double opponentSpace MEMBER opponentSpace NOTIFY gameChanged)

public:
    enum Roles {
        TypeRole = Qt::UserRole + 1,
        OwnerIdRole,
        RowRole,
        RowSpanRole,
        ColumnRole,
        ColumnSpanRole,
        FaceUpRole,
        AllowedValueRole,
        AllowedFamilyRole,
        AllowMultipleRole,
        PullAllowedRole,
        AllowAutoPullRole,
        DeckIdBaseRole,
        DeckIdTopRole,
        DeckIdMoveRole,
    };
    Q_ENUM(Roles)

    explicit VCDDeckInfoModel(QObject *parent = nullptr);

    void setList(const VCDDeckList &list);

    bool showAllPlayerHands = false;
    double meShowRows = 0;
    double meShowColumns = 0;
    double othersShowRows = 0;
    double othersShowColumns = 0;

    int playerRows = 0;
    int playerColumns = 0;
    int commonRows = 0;
    int commonColumns = 0;

    double meSpace = 0.4;
    double commonSpace = 0.25;
    double opponentSpace = 0.35;

Q_SIGNALS:
    void gameChanged();

public:
    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    VCDDeckList m_list;
};
