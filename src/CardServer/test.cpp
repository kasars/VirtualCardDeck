// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: LGPL-2.0-or-later

#include <QDebug>
#include <QtCore/QString>
#include <QtTest/QtTest>

#include "GameManager.h"
#include "testhelper.h"

using namespace Qt::Literals::StringLiterals;

VCDGameList GameManagerTest::makeGameList()
{
    VCDGameList gameList;
    VCDGameInfo info;
    info.name = "Foo";
    info.playerCount = 1;
    info.base64QImageData = "12345";
    gameList.append(info);
    return gameList;
}

VCDGameDetails GameManagerTest::makeGameDetails()
{
    VCDGameDetails d;
    d.info.name = "Foo";
    d.info.playerCount = 1;
    d.info.base64QImageData = "12345";

    d.othersShowRows = 2;
    d.othersShowColumns = 2;

    VCDDeckInfo info;
    info.row = 1;
    info.column = 1;
    info.rowSpan = 1;
    info.columnSpan = 1;
    info.type = "Foo";
    info.faceUp = false;
    info.allowedValue = "AllowedValue";
    info.allowedFamily = "allowedFamily";
    info.allowMultiple = false;
    info.pullAllowed = false;
    info.allowAutoPull = true;
    info.dealCount = 1;
    info.topDealCount = 1;
    int deckIndex = 0;
    for (const auto &owner : {123, 321, 0}) {
        info.ownerId = owner;
        info.deckIdBase = deckIndex++;
        info.deckIdTop = deckIndex++;
        info.deckIdMove = deckIndex++;
        d.decks.append(info);
    }
    return d;
}

GameManagerTest::GameManagerTest()
{
    connect(&m_gameManager, &GameManager::sendGameList, &m_helper, &TestHelper::sendGameList);
    connect(&m_gameManager, &GameManager::sendGameSelection, &m_helper, &TestHelper::sendGameSelection);
    connect(&m_gameManager, &GameManager::sendPlayerList, &m_helper, &TestHelper::sendPlayerList);
    connect(&m_gameManager, &GameManager::sendDeckInventory, &m_helper, &TestHelper::sendDeckInventory);
    connect(&m_gameManager, &GameManager::sendCardMoved, &m_helper, &TestHelper::sendCardMoved);
    connect(&m_gameManager, &GameManager::sendReserveOK, &m_helper, &TestHelper::sendReserveOK);
    connect(&m_gameManager, &GameManager::sendReserveBlocked, &m_helper, &TestHelper::sendReserveBlocked);
}

void TestServer::sendToPlayer(const VCDPlayerInfo &, const QJsonObject &obj)
{
    qDebug() << obj;
    signalData(obj);
}

void TestServer::sendToAll(const QJsonObject &obj)
{
    qDebug() << obj;
    signalData(obj);
}

bool TestClient::sendToServer(const QJsonObject &obj)
{
    qDebug() << obj;
    signalData(obj);
    return true;
}

void GameManagerTest::connectPlayers()
{
    VCDPlayerInfo player1{"p1", false, 1};
    VCDPlayerInfo player2{"p2", false, 2};

    m_gameManager.onUpdatePlayerInfo(player1);
    QCOMPARE(m_helper.playerListSent, 1);
    m_gameManager.onUpdatePlayerInfo(player2);
    QCOMPARE(m_helper.playerListSent, 2);
    QCOMPARE(m_helper.players.count(), 2);

    for (const auto &game : m_helper.gameList) {
        qDebug() << game.name << game.playerCount;
    }

    QCOMPARE(m_helper.cardsDealt, 0);
    m_gameManager.onSelectGame(0);
    m_gameManager.onStartGame();
    QCOMPARE(m_helper.cardsDealt, 0);
    player1.ready = true;
    m_gameManager.onUpdatePlayerInfo(player1);
    m_gameManager.onStartGame();
    QCOMPARE(m_helper.cardsDealt, 1);
}

void GameManagerTest::serverToClient()
{
    TestServer server;
    CardClient client;

    connect(&server, &TestServer::signalData, &client, &CardClient::parseServerJson);

    VCDGameList sGameList = makeGameList();
    int gameIndex = 42;
    VCDPlayerInfo sPlayer{"player", true, 1234};
    VCDPlayerInfo sPlayer2{"player2", true, 123};
    VCDPlayerList sPList{sPlayer, sPlayer2};
    VCDGameDetails sGDetails = makeGameDetails();
    VCDDeckInventory sDi;
    for (int i = 0; i < 5; ++i) {
        VCDDeckData deck;
        for (int j = 0; j < 5; ++j) {
            deck.cardIds.append(j);
        }
        sDi.decks.append(deck);
    }
    VCDCardAndPosition sSource{12, 3, 4};
    VCDCardAndPosition sTarget{12, 5, 6};
    QList<uint64_t> sPlayers{1234, 567890};

    QList<int> sDecks{1, 2, 3, 5, 6, 7};

    QStringList sCalled;

    connect(&client, &CardClient::gameList, this, [&](const VCDGameList &list) {
        QCOMPARE(sGameList, list);
        sCalled << "gameList";
    });
    connect(&client, &CardClient::gameSelection, this, [&](int listIndex) {
        QCOMPARE(gameIndex, listIndex);
        sCalled << "gameSelection";
    });
    connect(&client, &CardClient::playerInfo, this, [&](const VCDPlayerInfo &player) {
        QCOMPARE(sPlayer, player);
        sCalled << "playerInfo";
    });
    connect(&client, &CardClient::playerList, this, [&](const VCDPlayerList &list) {
        QCOMPARE(sPList, list);
        sCalled << "playerList";
    });
    connect(&client, &CardClient::gameDetails, this, [&](const VCDGameDetails &game) {
        QCOMPARE(sGDetails, game);
        sCalled << "gameDetails";
    });
    connect(&client, &CardClient::deckInventory, this, [&](const VCDDeckInventory &invrntory) {
        QCOMPARE(sDi, invrntory);
        sCalled << "deckInventory";
    });

    connect(&client,
            &CardClient::cardMoved,
            this,
            [&](const VCDCardAndPosition &source, const VCDCardAndPosition &target) {
                QCOMPARE(sSource, source);
                QCOMPARE(sTarget, target);
                sCalled << "cardMoved";
            });
    connect(&client, &CardClient::reserveOK, this, [&]() {
        sCalled << "reserveOK";
    });
    connect(&client, &CardClient::reserveBlocked, this, [&](const QList<uint64_t> &players) {
        QCOMPARE(sPlayers, players);
        sCalled << "reserveBlocked";
    });
    connect(&client, &CardClient::startFailed, this, [&]() {
        sCalled << "startFailed";
    });
    connect(&client, &CardClient::reservedDecks, this, [&](const QList<int> &decks) {
        QCOMPARE(decks, sDecks);
        sCalled << "reservedDecks";
    });

    void sendReservedDecks(const QList<int> &deckIndexes);

    server.sendGameList(sGameList);
    server.sendGameSelection(gameIndex);
    server.sendPlayerInfo(sPlayer);
    server.sendPlayerList(sPList);
    server.sendGameDetails(sGDetails);
    server.sendDeckInventory(sDi);
    server.sendCardMoved(sSource, sTarget);
    server.sendReserveOK(sPlayer);
    server.sendReserveBlocked(sPlayer, sPlayers);
    server.sendStartFailed();
    server.sendReservedDecks(sDecks);

    QVERIFY(sCalled.contains("gameList"));
    QVERIFY(sCalled.contains("gameSelection"));
    QVERIFY(sCalled.contains("playerInfo"));
    QVERIFY(sCalled.contains("playerList"));
    QVERIFY(sCalled.contains("gameDetails"));
    QVERIFY(sCalled.contains("deckInventory"));
    QVERIFY(sCalled.contains("cardMoved"));
    QVERIFY(sCalled.contains("reserveOK"));
    QVERIFY(sCalled.contains("reserveBlocked"));
    QVERIFY(sCalled.contains("startFailed"));
    QVERIFY(sCalled.contains("reservedDecks"));
}

void GameManagerTest::clientToServer()
{
    TestServer server;
    TestClient client;

    QTcpSocket dummySocket;

    connect(&client, &TestClient::signalData, &server, [&server, &dummySocket](const QJsonObject &obj) {
        server.parseClientJson(&dummySocket, obj);
    });

    VCDPlayerInfo sPlayer{"player", true, (uint64_t)(&dummySocket)};
    VCDCardAndPosition sSource{12, 3, 4};
    VCDCardAndPosition sTarget{12, 5, 6};
    int gameIndex = 42;

    QStringList sCalled;

    connect(&server,
            &CardServer::reserveMove,
            this,
            [&](const VCDPlayerInfo &, const VCDCardAndPosition &source, const VCDCardAndPosition &target) {
                QCOMPARE(source, sSource);
                QCOMPARE(target, sTarget);
                sCalled << "reserveMove";
            });
    connect(&server,
            &CardServer::requestMove,
            this,
            [&](const VCDPlayerInfo &, const VCDCardAndPosition &source, const VCDCardAndPosition &target) {
                QCOMPARE(source, sSource);
                QCOMPARE(target, sTarget);
                sCalled << "requestMove";
            });
    connect(&server, &CardServer::requestUndoLast, this, [&]() {
        sCalled << "requestUndoLast";
    });
    connect(&server, &CardServer::updatePlayerInfo, this, [&](const VCDPlayerInfo &player) {
        QCOMPARE(player, sPlayer);
        sCalled << "updatePlayerInfo";
    });
    connect(&server, &CardServer::selectGame, this, [&](int listIndex) {
        QCOMPARE(listIndex, gameIndex);
        sCalled << "selectGame";
    });
    connect(&server, &CardServer::startGame, this, [&]() {
        sCalled << "startGame";
    });
    connect(&server, &CardServer::clearGame, this, [&]() {
        sCalled << "clearGame";
    });

    client.reserveMove(sSource, sTarget);
    client.requestMove(sSource, sTarget);
    client.requestUndoLast();
    client.sendPlayerInfo(sPlayer);
    client.sendSelectGame(gameIndex);
    client.sendStartGame();
    client.sendClearGame();

    QVERIFY(sCalled.contains("reserveMove"));
    QVERIFY(sCalled.contains("requestMove"));
    QVERIFY(sCalled.contains("requestUndoLast"));
    QVERIFY(sCalled.contains("updatePlayerInfo"));
    QVERIFY(sCalled.contains("selectGame"));
    QVERIFY(sCalled.contains("startGame"));
    QVERIFY(sCalled.contains("clearGame"));
}

void GameManagerTest::reserveMove()
{
    initTest();

    VCDPlayerInfo player1{"p1", true, 1};
    VCDPlayerInfo player2{"p2", false, 2};

    m_gameManager.onUpdatePlayerInfo(player1);
    QCOMPARE(m_helper.playerListSent, 1);
    m_gameManager.onUpdatePlayerInfo(player2);
    QCOMPARE(m_helper.playerListSent, 2);
    QCOMPARE(m_helper.players.count(), 2);

    m_gameManager.onSelectGame(0);
    m_gameManager.onStartGame();
    QCOMPARE(m_helper.cardsDealt, 1);

    QCOMPARE(m_helper.inventory.decks.size(), 36);
    // Game[0] has the deal deck at index 12
    int fromDeckIndex = 12;
    const auto &fromDeck = m_helper.inventory.decks[fromDeckIndex];

    int fromCardId = fromDeck.cardIds.constLast();
    int fromInDeckIndex = fromDeck.cardIds.size() - 1;

    VCDCardAndPosition source{fromCardId, fromDeckIndex, fromInDeckIndex};
    VCDCardAndPosition nullPos{-1, -1, -1};

    m_gameManager.onReserveMove(player1, source, nullPos);
    QCOMPARE(m_helper.reserveOK, true);

    // Player 2 cannot reserve same deck
    m_gameManager.onReserveMove(player2, source, nullPos);
    QCOMPARE(m_helper.reserveOK, false);

    // Clear reservation with nullPos
    m_gameManager.onReserveMove(player1, nullPos, nullPos);
    QCOMPARE(m_helper.reserveOK, true);

    // Now player 2 should be able to reserve
    m_gameManager.onReserveMove(player2, source, nullPos);
    QCOMPARE(m_helper.reserveOK, true);

    // Clear the reservation for player 2
    m_gameManager.onReserveMove(player2, nullPos, nullPos);

    // Test common source
    VCDCardAndPosition target0{-1, 0, 0}; // Target does not have cardId
    m_gameManager.onReserveMove(player1, source, target0);
    QCOMPARE(m_helper.reserveOK, true);

    VCDCardAndPosition target1{-1, 1, 0};
    m_gameManager.onReserveMove(player2, source, target1);
    QCOMPARE(m_helper.reserveOK, false); // player2 cannot use the same source

    // Test common target
    int i = 0;
    for (const auto &deck : m_helper.inventory.decks) {
        qDebug() << i << deck.cardIds;
        i++;
    }
    int fromDeck2Index = 15;
    const auto &fromDeck2 = m_helper.inventory.decks[fromDeck2Index];

    int fromCard2Id = fromDeck2.cardIds.constLast();
    int fromInDeck2Index = fromDeck2.cardIds.size() - 1;
    VCDCardAndPosition source2{fromCard2Id, fromDeck2Index, fromInDeck2Index};

    m_gameManager.onReserveMove(player2, source2, target0);
    QCOMPARE(m_helper.reserveOK, false); // player2 cannot use the same target
}

QTEST_APPLESS_MAIN(GameManagerTest);
