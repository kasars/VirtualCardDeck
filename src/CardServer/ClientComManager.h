#pragma once

#include "CardClient.h"
#include "ClientGameManager.h"

#include <QObject>
#include <QString>
#include <QTimer>

class ClientComManager : public QObject
{
    Q_OBJECT
public:
    explicit ClientComManager(QObject *parent = nullptr);
    ~ClientComManager();
    void connectToServer(const QString &host, int port);

    QObject *gameManager();

public Q_SLOTS:

Q_SIGNALS:

private:
    CardClient m_client;
    ClientGameManager m_gameManager;
    QTimer m_reconnectTmr;
};
