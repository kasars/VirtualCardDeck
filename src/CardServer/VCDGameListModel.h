#pragma once

#include "VCDGameInfo.h"

#include <QAbstractListModel>
#include <QObject>

class VCDGameListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        NameRole = Qt::UserRole + 1,
        Base64ImageDataRole,
        PlayerCountRole,
        SelectedRole,
        ServerIndexRole,
    };
    Q_ENUM(Roles)

    explicit VCDGameListModel(QObject *parent = nullptr);

    void setList(const VCDGameList &list);
    void setSelectedIndex(int index);

public:
    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    VCDGameList m_list;
    int m_serverSelectedIndex = -1;
};
