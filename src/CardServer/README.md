## Client to Server messages:
The data is in JSON format with every message on a separate line.

Only one request that needs a response can be in the queue from a client to the server

### Updating the player status
```
{
    "cmd": "UpdatePlayer",
    "Name": "<Player Name>",
    "Ready": <true/false>
}
```
No direct server reply


### Doing card moves

#### Request permission to start moving a card
The client sends a request to see if a card move is allowed

```
{
    "cmd": "ReserveCardMove",
    "SourceDeck": "<# Deck Id>",
    "SourceInDeckIndex": "<# Card's Deck Index>",
    "TargetDeck": "<# Deck Id (optional)>",
    "TargetInDeckIndex": "<# Cards Deck Index (optional)>"
}
```
If the move is not blocked the server responds with: ```{"cmd": "ReserveOK"}```
and if the move is not possible to finish: 
```
{
    "cmd": "ReserveBlocked",
    "BlockedBy": [ "<Player Name>", ...]
}
```

If the client has recieved a "ReserveOK", the source deck (and if applicable the target deck) is 
reserved for this client and any move to or from the selected deck(s) is blocked for any other client.

The deck blocking is cleared if a subsequent "ReserveCardMove" (possibly with the target) is blocked.
The deck blocking is also removed once the move is done. Or if the connection to the client is lost.

#### Requesting a card move
```
{
    "cmd": "RequestCardMove",
    "SourceDeck": "<# Deck Id>",
    "SourceInDeckIndex": "<# Card's Deck Index>",
    "TargetDeck": "<# Deck Id>",
    "TargetInDeckIndex": "<# Cards Deck Index>"
}
```

If the move is not blocked and done, the server responds by sending the card move and if the move
is not possible to finish, the server responds with:
```
{
    "cmd": "ReserveBlocked",
    "BlockedBy": [ "<Player Name>", ...]
}
```

#### Request undoing of card moves
In case the game allows it, a client can request moves to be undone and the cards will move back in 
reverse order.
```
{"cmd": "UndoLast"}
```

The server responds with ```{"cmd": "UndoFailed"}``` if the effected card would be moved to or from 
a reserved deck and ```{"cmd": "UndoDone"}``` if successful.


### Selecting a game
Before a client player is marked as ready, all clients can request to select a game with

```
{
    "cmd": "SelectGame",
    "GameIndex": <Game List Index>"
}
```
If the selection is not possible the server responds with ```{"cmd": "GameSelectionFailed"}```. Otherwise
the game selection is updated and the server responds with ```{"cmd": "GameSelectionOK"}```




## Server to Client messages
When the client connects to the server, the following messages are sent

### The game list
This is only updated once when the client connects
```
{
    "cmd": "GameList",
    "Games": [
        {
            "Name": "<Game Name>",
            "NumPlayers": <# number of required players>
        },
        ....
    ]
}
```

### Game selection
This message is sent whenever a game selection has been done
```
{
    "cmd": "SelectedGame",
    "SelectedGame": <# game list index>
}
```

### Player List
This list is updated whenever a user is updated
```
{
    "cmd": "Players",
    "Players": [
        {
            "Name": "<Participant Name>",
            "Ready": <true/false>
        }.
        ....
    ]
}
```

### Starting a game
When the number of ready participants corresponds to the number of players, the game starts. The 
game starts by dealing the cards according to the game rules and that is done with the following 
message that contains all the cards of the table

```
{
    "cmd": "DealCards",
    "Cards": [
        {
            "CardId": <# (family * 100 + value)>",
            "DeckIndex": <# Deck index>
        }.
        ....
    ]
}
```

### Signal a card move
```
{
    "cmd": "CardMove",
    "SourceDeck": "<Deck Id>",
    "SourceInDeckIndex": "<# Card's Deck Index>",
    "TargetDeck": "<Deck Id (optional)>",
    "TargetInDeckIndex": "<# Cards Deck Index (optional)>"   "cmd": "MoveCard",
}
```
