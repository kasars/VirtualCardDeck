// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once
#include "VCDDeckInventory.h"

#include <QAbstractListModel>
#include <QObject>

class VCDDeckDataModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(bool reserved READ reserved NOTIFY reservedChanged)
public:
    enum Roles {
        ColorRole = Qt::UserRole + 1,
        FamilyRole,
        ValueRole,
        CardIdRole,
    };
    Q_ENUM(Roles)

    explicit VCDDeckDataModel(QObject *parent = nullptr);
    explicit VCDDeckDataModel(const VCDDeckData &data, QObject *parent = nullptr);

    void setCards(const QList<int> &cardIds);
    bool insertAt(int index, int cardId);
    std::optional<int> takeAt(int index);
    std::optional<int> idAt(int row) const;

    bool reserved() const;
    void setReserved(bool reserved);

Q_SIGNALS:
    void reservedChanged();

public:
    QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    VCDDeckData m_deckData;
    bool m_reserved = false;
};
