#include "VCDGameListModel.h"

#include <QDebug>

VCDGameListModel::VCDGameListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

void VCDGameListModel::setList(const VCDGameList &list)
{
    if (list.isEmpty()) {
        beginResetModel();
        m_list.clear();
        endResetModel();
        return;
    }

    int rowDiff = list.size() - m_list.size();
    if (rowDiff == 0) {
        m_list = list;
        dataChanged(index(0), index(m_list.size() - 1));
    }
    else if (rowDiff > 0) {
        beginInsertRows(QModelIndex(), m_list.size(), list.size() - 1);
        m_list = list;
        endInsertRows();
        dataChanged(index(0), index(m_list.size() - 1));
    }
    else {
        beginRemoveRows(QModelIndex(), list.size(), m_list.size() - 1);
        m_list = list;
        endRemoveRows();
        dataChanged(index(0), index(list.size() - 1));
    }
}

void VCDGameListModel::setSelectedIndex(int idx)
{
    m_serverSelectedIndex = idx;
    dataChanged(index(0), index(m_list.size() - 1));
}

QHash<int, QByteArray> VCDGameListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[Base64ImageDataRole] = "imageData";
    roles[PlayerCountRole] = "numPlayers";
    roles[SelectedRole] = "selected";
    roles[ServerIndexRole] = "serverIndex";
    return roles;
}

int VCDGameListModel::rowCount(const QModelIndex &) const
{
    return m_list.count();
}

QVariant VCDGameListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= m_list.count() || index.row() < 0) {
        return QVariant();
    }

    const auto &item = m_list.at(index.row());
    switch (static_cast<Roles>(role)) {
    case NameRole:
        return item.name;
    case Base64ImageDataRole:
        return item.base64QImageData;
    case PlayerCountRole:
        return item.playerCount;
    case SelectedRole:
        return index.row() == m_serverSelectedIndex;
    case ServerIndexRole:
        return index.row();
    }

    return QVariant();
}
