#pragma once

#include <QQuickImageProvider>

class VCDB64ImageProvider : public QQuickImageProvider
{
    Q_OBJECT
public:
    VCDB64ImageProvider();
    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize) override;
};
