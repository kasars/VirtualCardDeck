#include "VCDPlayersListModel.h"

#include <QDebug>

VCDPlayersListModel::VCDPlayersListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

void VCDPlayersListModel::setPlayers(const VCDPlayerList &players)
{
    if (players.isEmpty()) {
        beginResetModel();
        m_players.clear();
        endResetModel();
        return;
    }

    int rowDiff = players.size() - m_players.size();
    if (rowDiff == 0) {
        m_players = players;
        dataChanged(index(0), index(m_players.size() - 1));
    }
    else if (rowDiff > 0) {
        beginInsertRows(QModelIndex(), m_players.size(), players.size() - 1);
        m_players = players;
        endInsertRows();
        dataChanged(index(0), index(players.size() - 1));
    }
    else {
        beginRemoveRows(QModelIndex(), players.size(), m_players.size() - 1);
        m_players = players;
        endRemoveRows();
        dataChanged(index(0), index(m_players.size() - 1));
    }
}

QHash<int, QByteArray> VCDPlayersListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[ReadyRole] = "ready";
    roles[ServerIdRole] = "serverId";
    roles[SortIndexRole] = "sortIndex";
    return roles;
}

int VCDPlayersListModel::rowCount(const QModelIndex &) const
{
    return m_players.count();
}

QVariant VCDPlayersListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= m_players.count() || index.row() < 0) {
        return QVariant();
    }

    const auto &player = m_players.at(index.row());
    switch (static_cast<Roles>(role)) {
    case NameRole:
        return player.name;
    case ReadyRole:
        return player.ready;
    case ServerIdRole:
        return QString::number(player.serverPlayerId);
    case SortIndexRole:
        return player.sortIndex;
    }

    return QVariant();
}
