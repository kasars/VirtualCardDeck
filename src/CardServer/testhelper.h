// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: LGPL-2.0-or-later

#include <QDebug>
#include <QObject>

#include "CardClient.h"
#include "CardServer.h"
#include "GameManager.h"

using namespace Qt::Literals::StringLiterals;

class TestHelper : public QObject {
    Q_OBJECT
public:
    void initTest()
    {
        gameList.clear();
        gameListSent = false;
        gameSelected = -1;

        playerListSent = 0;
        cardsDealt = 0;
        reserveOK = false;
        blocking = {1, 2};
        players.clear();
        inventory.clear();
    }
    void sendGameList(const VCDGameList &list)
    {
        gameListSent = true;
        gameList = list;
    }

    void sendGameSelection(int listIndex)
    {
        gameSelected = listIndex;
    }

    void sendPlayerList(const VCDPlayerList &list)
    {
        playerListSent++;
        players = list;
    }

    void sendDeckInventory(const VCDDeckInventory & inv)
    {
        cardsDealt++;
        inventory = inv;
    }

    void sendCardMoved(const VCDCardAndPosition &src, const VCDCardAndPosition &trgt)
    {
        source = src;
        target = trgt;
    }

    void sendReserveOK(const VCDPlayerInfo &player)
    {
        lastReceiver = player;
        reserveOK = true;
        blocking.clear();
    }

    void sendReserveBlocked(const VCDPlayerInfo &player, const QList<uint64_t> &players)
    {
        lastReceiver = player;
        reserveOK = false;
        blocking = players;
    }

public:
    VCDGameList gameList;
    bool gameListSent = false;
    int gameSelected = -1;

    int playerListSent = 0;
    int cardsDealt = 0;
    VCDCardAndPosition source;
    VCDCardAndPosition target;
    VCDPlayerInfo lastReceiver;
    bool reserveOK = false;
    QList<uint64_t> blocking{1, 2};
    VCDPlayerList players;
    VCDDeckInventory inventory;
};

class TestServer : public CardServer {
    Q_OBJECT
Q_SIGNALS:
    void signalData(const QJsonObject &obj);

protected:
    void sendToAll(const QJsonObject &obj) override;
    void sendToPlayer(const VCDPlayerInfo &player, const QJsonObject &obj) override;
};

class TestClient : public CardClient {
    Q_OBJECT
Q_SIGNALS:
    void signalData(const QJsonObject &obj);

protected:
    bool sendToServer(const QJsonObject &obj) override;
};

class GameManagerTest : public QObject {
    Q_OBJECT
public:
    GameManagerTest();

    void initTest() { m_gameManager.onClearGame(); m_helper.initTest(); }

    VCDGameList makeGameList();
    VCDGameDetails makeGameDetails();

private Q_SLOTS:
    void connectPlayers();
    void serverToClient();
    void clientToServer();
    void reserveMove();

private:
    GameManager m_gameManager;
    TestHelper m_helper;
};
