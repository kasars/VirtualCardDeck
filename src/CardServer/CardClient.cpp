// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "CardClient.h"
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

CardClient::CardClient(QObject *parent)
    : QObject(parent)
{
    connect(&m_tcpSocket, &QIODevice::readyRead, this, &CardClient::onReadReady);
    connect(&m_tcpSocket, &QAbstractSocket::errorOccurred, this, &CardClient::onErrorOccurred);
    connect(&m_tcpSocket, &QTcpSocket::stateChanged, this, &CardClient::onStateChanged);
}

void CardClient::connectToServer(const QString &host, int port)
{
    m_tcpSocket.connectToHost(host, port);
}

void CardClient::onErrorOccurred(QAbstractSocket::SocketError socketError)
{
    qWarning() << socketError;
}

void CardClient::onStateChanged(QAbstractSocket::SocketState state)
{
    if (state == QAbstractSocket::UnconnectedState) {
        Q_EMIT serverConnectionClosed();
        Q_EMIT connectedChanged();
    }
    else if (state == QAbstractSocket::ConnectedState) {
        Q_EMIT connectedChanged();
    }
}

void CardClient::onReadReady()
{
    while (m_tcpSocket.canReadLine()) {
        QByteArray line = m_tcpSocket.readLine();
        QJsonParseError error;
        QJsonDocument doc(QJsonDocument::fromJson(line, &error));
        QJsonObject root = doc.object();
        parseServerJson(root);
    }
}

void CardClient::parseServerJson(const QJsonObject &obj)
{
    QString cmd = obj["cmd"].toString();
    if (cmd == "GameList") {
        QJsonArray gamesJson = obj["Games"].toArray();
        VCDGameList games;
        for (const auto &gameV : gamesJson) {
            QJsonObject gameObj = gameV.toObject();
            VCDGameInfo game;
            game.name = gameObj["Name"].toString();
            game.playerCount = gameObj["NumPlayers"].toInt(0);
            game.base64QImageData = gameObj["Base64QImageData"].toString();
            games.append(game);
        }
        Q_EMIT gameList(games);
    }
    else if (cmd == "SelectedGame") {
        int selected = obj["SelectedGame"].toInt(0);
        Q_EMIT gameSelection(selected);
    }
    else if (cmd == "Player") {
        QJsonObject playerObj = obj["Player"].toObject();
        VCDPlayerInfo player;
        player.serverPlayerId = playerObj["ServerId"].toString().toULongLong();
        player.name = playerObj["Name"].toString();
        player.ready = playerObj["Ready"].toBool(false);
        Q_EMIT playerInfo(player);
    }
    else if (cmd == "Players") {
        QJsonArray playersJson = obj["Players"].toArray();
        VCDPlayerList players;
        for (const auto &playerV : playersJson) {
            QJsonObject playerObj = playerV.toObject();
            VCDPlayerInfo player;
            player.serverPlayerId = playerObj["ServerId"].toString().toULongLong();
            player.name = playerObj["Name"].toString();
            player.ready = playerObj["Ready"].toBool(false);
            player.sortIndex = playerObj["SortIndex"].toInt(0);
            players.append(player);
        }
        Q_EMIT playerList(players);
    }
    else if (cmd == "DealCards") {
        QJsonArray decksJson = obj["Decks"].toArray();
        VCDDeckInventory inventory;
        for (const auto &deckJson : decksJson) {
            VCDDeckData deck;
            for (const auto &cardId : deckJson.toArray()) {
                deck.cardIds.append(cardId.toInt());
            }
            inventory.decks.append(deck);
        }
        Q_EMIT deckInventory(inventory);
    }
    else if (cmd == "CardMove") {
        VCDCardAndPosition source;
        VCDCardAndPosition target;
        source.cardId = obj["SourceCardId"].toInt();
        source.deckIndex = obj["SourceDeck"].toInt();
        source.inDeckIndex = obj["SourceInDeckIndex"].toInt();
        target.cardId = obj["TargetCardId"].toInt();
        target.deckIndex = obj["TargetDeck"].toInt();
        target.inDeckIndex = obj["TargetInDeckIndex"].toInt();
        Q_EMIT cardMoved(source, target);
    }
    else if (cmd == "ReserveOK") {
        Q_EMIT reserveOK();
    }
    else if (cmd == "ReserveBlocked") {
        QList<uint64_t> users;
        QJsonArray blockJson = obj["BlockedBy"].toArray();
        for (const auto &blockVal : blockJson) {
            users += blockVal.toString().toULongLong();
        }
        Q_EMIT reserveBlocked(users);
    }
    else if (cmd == "GameDetails") {
        VCDGameDetails game;
        game.showAllPlayerHands = obj["ShowAllPlayerHands"].toBool();

        game.meShowRows = obj["MeShowRows"].toDouble();
        game.meShowColumns = obj["MeShowColumns"].toDouble();
        game.othersShowRows = obj["OthersShowRows"].toDouble();
        game.othersShowColumns = obj["OthersShowColumns"].toDouble();

        game.playerRows = obj["PlayerRows"].toInt();
        game.playerColumns = obj["PlayerColumns"].toInt();
        game.commonRows = obj["CommonRows"].toInt();
        game.commonColumns = obj["CommonColumns"].toInt();

        game.meSpace = obj["MeSpace"].toDouble();
        game.commonSpace = obj["CommonSpace"].toDouble();
        game.opponentSpace = obj["OpponentSpace"].toDouble();
        game.info.playerCount = obj["PlayerCount"].toInt();
        game.info.name = obj["Name"].toString();
        game.info.base64QImageData = obj["Base64QImageData"].toString();
        QJsonArray decksArray = obj["GameDecks"].toArray();
        for (const auto &deckValue : decksArray) {
            QJsonObject deckObj = deckValue.toObject();
            VCDDeckInfo deck;
            deck.ownerId = deckObj["OwnerId"].toString().toULongLong();
            deck.row = deckObj["Row"].toInt();
            deck.rowSpan = deckObj["RowSpan"].toInt();
            deck.column = deckObj["Column"].toInt();
            deck.columnSpan = deckObj["ColumnSpan"].toInt();
            deck.type = deckObj["DeckType"].toString();
            deck.faceUp = deckObj["FaceUp"].toBool();
            deck.allowedValue = deckObj["AllowedValue"].toString();
            deck.allowedFamily = deckObj["AllowedFamily"].toString();
            deck.allowMultiple = deckObj["AllowMultiple"].toBool();
            deck.pullAllowed = deckObj["PullAllowed"].toBool();
            deck.allowAutoPull = deckObj["AllowAutoPull"].toBool();
            deck.dealCount = deckObj["DealCount"].toInt();
            deck.topDealCount = deckObj["TopDealCount"].toInt();
            deck.deckIdBase = deckObj["DeckIdBase"].toInt();
            deck.deckIdTop = deckObj["DeckIdTop"].toInt();
            deck.deckIdMove = deckObj["DeckIdMove"].toInt();
            game.decks.append(deck);
        }
        Q_EMIT gameDetails(game);
    }
    else if (cmd == "StartFailed") {
        Q_EMIT startFailed();
    }
    else if (cmd == "ReservedDecks") {
        QList<int> decks;
        QJsonArray decksArray = obj["ReservedDecks"].toArray();
        for (const auto &deckValue : decksArray) {
            decks.append(deckValue.toInt());
        }
        Q_EMIT reservedDecks(decks);
    }
    else {
        qDebug() << "Unknown cmd:" << cmd << obj;
    }
}

bool CardClient::sendToServer(const QJsonObject &obj)
{
    if (!connected()) {
        qWarning() << "not connected";
        return false;
    }
    QJsonDocument doc(obj);
    m_tcpSocket.write(doc.toJson(QJsonDocument::Compact) + "\n");
    return true;
}

bool CardClient::connected() const
{
    return m_tcpSocket.isOpen();
}

void CardClient::reserveMove(const VCDCardAndPosition &source, const VCDCardAndPosition &target)
{
    QJsonObject root;
    root["cmd"] = "ReserveCardMove";
    root["SourceCardId"] = source.cardId;
    root["SourceDeck"] = source.deckIndex;
    root["SourceInDeckIndex"] = source.inDeckIndex;
    if (target.deckIndex != -1) {
        root["TargetCardId"] = target.cardId;
        root["TargetDeck"] = target.deckIndex;
        root["TargetInDeckIndex"] = target.inDeckIndex;
    }
    sendToServer(root);
}

void CardClient::requestMove(const VCDCardAndPosition &source, const VCDCardAndPosition &target)
{
    QJsonObject root;
    root["cmd"] = "RequestCardMove";
    root["SourceCardId"] = source.cardId;
    root["SourceDeck"] = source.deckIndex;
    root["SourceInDeckIndex"] = source.inDeckIndex;
    root["TargetCardId"] = target.cardId;
    root["TargetDeck"] = target.deckIndex;
    root["TargetInDeckIndex"] = target.inDeckIndex;
    sendToServer(root);
}

void CardClient::requestUndoLast()
{
    QJsonObject root;
    root["cmd"] = "UndoLast";
    sendToServer(root);
}

void CardClient::sendPlayerInfo(const VCDPlayerInfo &player)
{
    QJsonObject root;
    root["cmd"] = "UpdatePlayer";
    root["Name"] = player.name;
    root["Ready"] = player.ready;
    sendToServer(root);
}

void CardClient::sendSelectGame(int index)
{
    QJsonObject root;
    root["cmd"] = "SelectedGame";
    root["SelectedGame"] = index;
    sendToServer(root);
}

void CardClient::sendStartGame()
{
    QJsonObject root;
    root["cmd"] = "StartGame";
    sendToServer(root);
}

void CardClient::sendClearGame() {
    QJsonObject root;
    root["cmd"] = "ClearGame";
    sendToServer(root);
}

