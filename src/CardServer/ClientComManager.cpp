#include "ClientComManager.h"
using namespace Qt::Literals::StringLiterals;

ClientComManager::ClientComManager(QObject *parent)
    : QObject(parent)
{
    connect(&m_client, &CardClient::gameList, &m_gameManager, &ClientGameManager::onGameList);
    connect(&m_client, &CardClient::gameSelection, &m_gameManager, &ClientGameManager::onGameSelection);
    connect(&m_client, &CardClient::playerInfo, &m_gameManager, &ClientGameManager::onPlayerInfo);
    connect(&m_client, &CardClient::playerList, &m_gameManager, &ClientGameManager::onPlayerList);
    connect(&m_client, &CardClient::gameDetails, &m_gameManager, &ClientGameManager::onGameDetails);
    connect(&m_client, &CardClient::deckInventory, &m_gameManager, &ClientGameManager::onDeckInventory);
    connect(&m_client, &CardClient::cardMoved, &m_gameManager, &ClientGameManager::onCardMoved);
    connect(&m_client, &CardClient::reserveOK, &m_gameManager, &ClientGameManager::onReserveOK);
    connect(&m_client, &CardClient::reserveBlocked, &m_gameManager, &ClientGameManager::onReserveBlocked);
    connect(&m_client, &CardClient::startFailed, &m_gameManager, &ClientGameManager::onStartFailed);
    connect(&m_client, &CardClient::reservedDecks, &m_gameManager, &ClientGameManager::onReservedDecks);

    connect(&m_gameManager, &ClientGameManager::reserveMove, &m_client, &CardClient::reserveMove);
    connect(&m_gameManager, &ClientGameManager::requestMove, &m_client, &CardClient::requestMove);
    connect(&m_gameManager, &ClientGameManager::requestUndoLast, &m_client, &CardClient::requestUndoLast);
    connect(&m_gameManager, &ClientGameManager::sendPlayerInfo, &m_client, &CardClient::sendPlayerInfo);
    connect(&m_gameManager, &ClientGameManager::sendSelectGame, &m_client, &CardClient::sendSelectGame);
    connect(&m_gameManager, &ClientGameManager::sendStartGame, &m_client, &CardClient::sendStartGame);
    connect(&m_gameManager, &ClientGameManager::sendClearGame, &m_client, &CardClient::sendClearGame);

    m_client.connectToServer(u"localhost"_s, 23174);

    connect(&m_client, &CardClient::serverConnectionClosed, this, [this]() {
        m_reconnectTmr.start(1000);
    });

    connect(&m_reconnectTmr, &QTimer::timeout, this, [this]() {
        m_reconnectTmr.stop();
        m_client.connectToServer(u"localhost"_s, 23174);
    });

    connect(&m_client, &CardClient::connectedChanged, this, [this]() {
        if (m_client.connected()) {
            m_gameManager.connectedToGameServer();
        }
    });
}

ClientComManager::~ClientComManager()
{
    m_client.disconnect();
    m_reconnectTmr.stop();
}

QObject *ClientComManager::gameManager()
{
    return &m_gameManager;
}
