// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "VCDDeckDataModel.h"
#include "VCDCard.h"

#include <QDebug>

VCDDeckDataModel::VCDDeckDataModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

VCDDeckDataModel::VCDDeckDataModel(const VCDDeckData &data, QObject *parent)
    : QAbstractListModel(parent)
    , m_deckData(data)
{
}

void VCDDeckDataModel::setCards(const QList<int> &cardIds)
{
    auto &deckCards = m_deckData.cardIds;
    if (cardIds.isEmpty()) {
        beginResetModel();
        deckCards.clear();
        endResetModel();
        return;
    }

    int rowDiff = cardIds.size() - deckCards.size();
    if (rowDiff == 0) {
        deckCards = cardIds;
        dataChanged(index(0), index(deckCards.size() - 1));
    }
    else if (rowDiff > 0) {
        beginInsertRows(QModelIndex(), deckCards.size(), cardIds.size() - 1);
        deckCards = cardIds;
        endInsertRows();
        dataChanged(index(0), index(cardIds.size() - 1));
    }
    else {
        beginRemoveRows(QModelIndex(), cardIds.size(), deckCards.size() - 1);
        deckCards = cardIds;
        endRemoveRows();
        dataChanged(index(0), index(deckCards.size() - 1));
    }
}

bool VCDDeckDataModel::insertAt(int index, int cardId)
{
    if (index < 0 || index > m_deckData.cardIds.size()) {
        qWarning() << "Cant insert at invalid index:" << index << m_deckData.cardIds.size();
        return false;
    }

    beginInsertRows(QModelIndex(), index, index);
    m_deckData.cardIds.insert(index, cardId);
    endInsertRows();
    return true;
}

std::optional<int> VCDDeckDataModel::takeAt(int index)
{
    if (index < 0 || index >= m_deckData.cardIds.size()) {
        qWarning() << "Cant take a card at invalid index:" << index << m_deckData.cardIds.size();
        return std::nullopt;
    }
    beginRemoveRows(QModelIndex(), index, index);
    int cardId = m_deckData.cardIds.takeAt(index);
    endRemoveRows();
    return cardId;
}

std::optional<int> VCDDeckDataModel::idAt(int row) const
{
    if (row < 0 || row >= m_deckData.cardIds.size()) {
        qWarning() << "Cant return a cardId at invalid index:" << row << m_deckData.cardIds.size();
        return std::nullopt;
    }
    return m_deckData.cardIds[row];
}

QHash<int, QByteArray> VCDDeckDataModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ColorRole] = "color";
    roles[FamilyRole] = "family";
    roles[ValueRole] = "value";
    roles[CardIdRole] = "cardId";
    return roles;
}

int VCDDeckDataModel::rowCount(const QModelIndex &) const
{
    return m_deckData.cardIds.count();
}

QVariant VCDDeckDataModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= m_deckData.cardIds.count() || index.row() < 0) {
        return QVariant();
    }

    const VCDCard card(m_deckData.cardIds.at(index.row()));

    switch ((Roles)role) {
    case ColorRole:
        return card.color();
    case FamilyRole:
        return card.family();
    case ValueRole:
        return card.value();
    case CardIdRole:
        return card.cardId();
    }
    return QVariant();
}

bool VCDDeckDataModel::reserved() const
{
    return m_reserved;
}

void VCDDeckDataModel::setReserved(bool reserved)
{
    if (m_reserved != reserved) {
        m_reserved = reserved;
        Q_EMIT reservedChanged();
    }
}
