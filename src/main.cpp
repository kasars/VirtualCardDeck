/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 *  by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#include <QGuiApplication>
#include <QQuickView>
#include <QtQml>
#include <QIcon>

#include "VCDCard.h"
#include "VCDComModel.h"
#include "VCDDeckModel.h"
#include "SortProxyModel.h"
#include "FilterProxyModel.h"
#include "VCDTableModel.h"
#include "VCDGameControl.h"
#include "VCDGamesModel.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setWindowIcon(QIcon(":/android/res/drawable-hdpi/icon.png"));

    QCoreApplication::setOrganizationName("Sars");
    #ifdef MULTIPLAYER_DISABLED
    QCoreApplication::setApplicationName("Virtual Solitaire");
    #else
    QCoreApplication::setApplicationName("Virtual Card Deck");
    #endif

    qmlRegisterType<VCDDeckModel> ("VirtualCardDeck", 1, 0, "VCDDeckModel");
    qmlRegisterType<VCDCard> ("VirtualCardDeck", 1, 0, "VCDCard");
    qmlRegisterType<SortProxyModel> ("VirtualCardDeck", 1, 0, "SortProxyModel");
    qmlRegisterType<FilterProxyModel> ("VirtualCardDeck", 1, 0, "FilterProxyModel");
    qmlRegisterType<VCDTableModel> ("VirtualCardDeck", 1, 0, "VCDTableModel");
    qmlRegisterType<VCDGamesModel> ("VirtualCardDeck", 1, 0, "VCDGamesModel");

    VCDGameControl gameControl;
    VCDComModel comModel;

    QQuickView view;
    view.engine()->rootContext()->setContextProperty("gameControl", &gameControl);
    view.engine()->rootContext()->setContextProperty("comModel", &comModel);

    // view.setFlags(view.flags() | Qt::MaximizeUsingFullscreenGeometryHint);
    view.setFlag(Qt::MaximizeUsingFullscreenGeometryHint, true);
    view.setFlag(Qt::Window, true);
    view.setResizeMode(QQuickView::SizeRootObjectToView);

    view.setSource(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    #ifdef Q_OS_ANDROID
    view.showFullScreen();
    #else
    view.show();
    #endif
    return app.exec();
}
