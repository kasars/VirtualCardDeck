/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#ifndef VCDComModel_h
#define VCDComModel_h

#include <QObject>
#include <QString>
#include <QDebug>


class VCDComModel: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isServer READ isServer NOTIFY gameChanged)
    Q_PROPERTY(bool waitingForStart READ waitingForStart NOTIFY gameChanged)
    Q_PROPERTY(bool readyToStart READ readyToStart NOTIFY gameChanged)
    Q_PROPERTY(bool gameStarted READ gameStarted NOTIFY gameChanged)
    Q_PROPERTY(QString gameName READ gameName NOTIFY gameChanged)
    Q_PROPERTY(QString gameFileName READ gameFileName NOTIFY gameChanged)
    Q_PROPERTY(int numPlayers READ numPlayers NOTIFY gameChanged)
    Q_PROPERTY(int numJoined READ numJoined NOTIFY gameChanged)
    Q_PROPERTY(QStringList joinedPlayers READ joinedPlayers NOTIFY gameChanged)

    Q_PROPERTY(QString playerName READ playerName WRITE setPlayerName NOTIFY gameChanged)
    Q_PROPERTY(int playerIndex READ playerIndex NOTIFY gameChanged)


    Q_PROPERTY(QList<QObject*> hostInfos READ hostInfos NOTIFY hostsChanged)


public:
    VCDComModel();
    ~VCDComModel();

    bool isServer() const;

    bool waitingForStart() const;
    bool readyToStart() const;
    bool gameStarted() const;

    const QString gameName() const;
    const QString gameFileName() const;
    int numPlayers() const;
    int numJoined() const;
    const QStringList joinedPlayers() const;

    const QString playerName() const;
    void setPlayerName(const QString &playerName);
    int playerIndex() const;

    Q_INVOKABLE void hostGame(const QString &name, const QString &fileName, int numPlayers);
    Q_INVOKABLE void waitForGame();

    Q_INVOKABLE void joinGame(const QString &hostId);


    Q_INVOKABLE void sendCards(const QString &jsonData);
    Q_INVOKABLE void sendMoveInfo(const QString &moveInfo);
    Q_INVOKABLE void sendUndoLast();

    Q_INVOKABLE void startGame();

    const QList<QObject*> hostInfos() const;

public Q_SLOTS:

Q_SIGNALS:
    void gameChanged();
    void hostsChanged();
    void gameDeckDataChanged(const QString &data);

    void dealerDeckSet(const QString &jsonData);

    void remoteMove(const QString &moveInfo);
    void remoteUndoLast();

private:
    class Private;
    friend class Private;
    Private *const d;
};

#endif
