/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#ifndef VCDGameControl_h
#define VCDGameControl_h

#include <QObject>

class VCDGameControl: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool multiplayer READ multiplayer NOTIFY multiplayerChanged)

public:
    VCDGameControl();
    ~VCDGameControl();

    #ifdef MULTIPLAYER_DISABLED
    bool multiplayer() const { return false; }
    #else
    bool multiplayer() const { return true; }
    #endif

public Q_SLOTS:

    Q_INVOKABLE bool remoteMoved(const QString &moveInfo);

    Q_INVOKABLE void moveCardFromTo(QObject *fromDeck, QObject *toDeck, int fromIndex, int toIndex, int moveId, int cardId, bool isRemote = false);
    Q_INVOKABLE void moveUndone();
    Q_INVOKABLE bool undoLast();
    Q_INVOKABLE bool undoLastRemote();
    Q_INVOKABLE void clearMoves();

    Q_INVOKABLE void setDeckId(QObject *deck, const QString &deckId);

    Q_INVOKABLE int nextMoveId();

Q_SIGNALS:
    void undoMove(QObject *fromDeck, QObject *toDeck, int fromIndex, int toIndex, int moveId, int cardId);

    void remoteMove(QObject *fromDeck, QObject *toDeck, int fromIndex, int toIndex, int moveId, int cardId);
    void signalMoveToRemote(const QString &moveInfo);
    void signalUndoToRemote();
    void multiplayerChanged();

private:
    class Private;
    Private *const d;
};

#endif
