/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#include "VCDDeckModel.h"

#include <QDebug>
#include <QDateTime>

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QRandomGenerator>

class VCDDeckModel::Private {
public:
    QList<VCDCard> cards;

    bool validIndex(int i) { return i>=0 && i<cards.count(); }
};

VCDDeckModel::VCDDeckModel(QObject *parent) : QAbstractListModel(parent), d(new Private())
{
}

void VCDDeckModel::newDeck()
{
    QVariantList cards;
    deleteCards();
    for (int i=VCDCard::Hearts; i<=VCDCard::Clubs; i++) {
        for (int j=VCDCard::Ace; j<=VCDCard::King; j++) {
            VCDCard card((VCDCard::Family)i, (VCDCard::Value)j);
            cards << QVariant::fromValue(card);
        }
    }
    return addCardsTop(cards);
}


QString VCDDeckModel::cardsJson()
{
    QString data = QStringLiteral("{\"cards\":[");
    for (int i=0; i<d->cards.count(); ++i) {
        if (i > 0) data += QChar(',');
        data += QStringLiteral("{\"fam\":%1,\"val\":%2}").arg(d->cards[i].family()).arg(d->cards[i].value());
    }
    data += QLatin1String("]}");
    return data;
}

void VCDDeckModel::setCardsJson(const QString &data)
{
    QJsonParseError error;
    QJsonDocument doc(QJsonDocument::fromJson(data.toUtf8(), &error));
    QJsonObject root = doc.object();
    QJsonArray array = root["cards"].toArray();

    beginResetModel();
    d->cards.clear();
    for (int i=0; i<array.size(); ++i) {
        QJsonObject card = array[i].toObject();
        //qDebug() << (VCDCard::Family)card["fam"].toInt() << (VCDCard::Value)card["val"].toInt();
        //qDebug() << card["val"].toString() << card["val"].toInt() << card["val"];
        //qDebug() << card["fam"] << card["fam"].toInt();
        d->cards.append(VCDCard((VCDCard::Family)card["fam"].toInt(), (VCDCard::Value)card["val"].toInt()));
    }

    endResetModel();
}


void VCDDeckModel::moveCardTo(QObject *targetDeck,int fromIndex, int toIndex)
{
    VCDDeckModel *toDeck = qobject_cast<VCDDeckModel *>(targetDeck);
    if (!toDeck || toDeck == this) {
        qDebug() << "Bad targetDeck";
        return;
    }
    toDeck->addCardsAt(toIndex, takeCardAt(fromIndex));
}


void VCDDeckModel::addCardsTop(const QVariantList &cards)
{
    beginInsertRows(QModelIndex(), 0, cards.count()-1);
    for (int i=0; i<cards.count(); ++i) {
        d->cards.prepend(cards[i].value<VCDCard>());
    }
    endInsertRows();
}

void VCDDeckModel::addCardsBottom(const QVariantList &cards)
{
    beginInsertRows(QModelIndex(), d->cards.count(), d->cards.count()+cards.count()-1);
    for (int i=0; i<cards.count(); ++i) {
        d->cards.append(cards[i].value<VCDCard>());
    }
    endInsertRows();
}

void VCDDeckModel::addCardsAt(int index, const QVariantList &cards)
{
    beginInsertRows(QModelIndex(), index, index+cards.count()-1);
    for (int i=0; i<cards.count(); ++i) {
        d->cards.insert(index, cards[i].value<VCDCard>());
    }
    endInsertRows();
}


QVariantList VCDDeckModel::takeCardsTop(int count)
{
    QVariantList cards;
    int takeCount = qMin(count, d->cards.count());
    beginRemoveRows(QModelIndex(), 0, takeCount-1);

    for (int i=0; i<takeCount; ++i) {
        d->cards.first().selected = false;
        cards << QVariant::fromValue(d->cards.takeFirst());
    }

    endRemoveRows();
    return cards;
}

QVariantList VCDDeckModel::takeCardAt(int index)
{
    if (index <0 || index >= d->cards.count()) {
        qDebug() << "Bad index" << index;
        return QVariantList();
    }

    QVariantList cards;
    beginRemoveRows(QModelIndex(), index, index);
    d->cards[index].selected = false;
    cards << QVariant::fromValue(d->cards.takeAt(index));
    endRemoveRows();
    return cards;
}


void VCDDeckModel::deleteCards()
{
    beginResetModel();
    d->cards.clear();
    endResetModel();
}


QVariantList VCDDeckModel::takeSelected()
{
    QVariantList cards;
    for (int i=d->cards.count()-1; i>=0; --i) {
        if (d->cards[i].selected) {
            beginRemoveRows(QModelIndex(), i, i);
            d->cards[i].selected = false;
            cards << QVariant::fromValue(d->cards.takeAt(i));
            endRemoveRows();
        }
    }
    return cards;
}

void VCDDeckModel::setSelected(int row, bool selected)
{
    if (!d->validIndex(row)) {
        return;
    }
    d->cards[row].selected = selected;
    dataChanged(index(row), index(row));
}

void VCDDeckModel::clearSelection()
{
    for (int i=0; i<d->cards.count(); ++i) {
        if (d->cards[i].selected) {
            d->cards[i].selected = false;
            dataChanged(index(i), index(i));
        }
    }
}

void VCDDeckModel::shuffle()
{
    QRandomGenerator *randGen = QRandomGenerator::global();
    beginResetModel();
    for (int i=0; i<d->cards.count(); ++i) {
        int toIndex = randGen->bounded(d->cards.count()-1);
        d->cards.swapItemsAt(i, toIndex);
    }
    endResetModel();
}

void VCDDeckModel::moveCard(int from, int to)
{
    int add = 0;
    if (from == to) return;
    if (to > from) {add = 1;}
    if (!d->validIndex(from)) return;
    if (!d->validIndex(to)) return;

    bool ok = beginMoveRows(QModelIndex(), from, from, QModelIndex(), to+add);
    if (!ok) {
        qDebug() << "Failed to move" << from << to << add << d->cards.count();
        return;
    }
    d->cards.move(from, to);
    endMoveRows();
}


int VCDDeckModel::cardId(int row) const
{
    if (row >= d->cards.count() || row < 0) {
        return 0;
    }
    return d->cards[row].cardId();
}

QHash<int, QByteArray> VCDDeckModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ColorRole]    = "color";
    roles[FamilyRole]   = "family";
    roles[ValueRole]    = "value";
    roles[CardIdRole]   = "cardId";
    roles[SelectedRole] = "selected";
    return roles;
}


int VCDDeckModel::rowCount(const QModelIndex &) const
{
    return d->cards.count();
}

QVariant VCDDeckModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= d->cards.count() || index.row() < 0) {
        return QVariant();
    }

    switch (role) {
        case ColorRole:
            return d->cards[index.row()].color();
        case FamilyRole:
            return d->cards[index.row()].family();
        case ValueRole:
            return d->cards[index.row()].value();
        case CardIdRole:
            return d->cards[index.row()].cardId();
        case SelectedRole:
            return d->cards[index.row()].selected;
    }
    return QVariant();
}


