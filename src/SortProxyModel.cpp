/* ============================================================
 *
 * Copyright (C) 2016, 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#include "SortProxyModel.h"

#include <QAbstractListModel>
#include <QDebug>
#include <QModelIndex>


bool SortProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    if (QSortFilterProxyModel::sortRole() == IndexRole) {
        return (left.row() < right.row());
    }
    return QSortFilterProxyModel::lessThan(left, right);
}


int SortProxyModel::sortRole() const { return QSortFilterProxyModel::sortRole(); }
bool SortProxyModel::sortAscending() const { return sortOrder() == Qt::AscendingOrder; }
QObject *SortProxyModel::srcModel() const { return sourceModel(); }

void SortProxyModel::setSortRole(int role)
{
    if (QSortFilterProxyModel::sortRole() != role) {
        QSortFilterProxyModel::setSortRole(role);
        setDynamicSortFilter(true);
        sort(0, sortOrder());
        invalidate();
        emit sortRoleChanged();
    }
}

void SortProxyModel::setSrcModel(QObject *model)
{
    QAbstractListModel *aModel = qobject_cast<QAbstractListModel *>(model);
    if (sourceModel() != aModel) {
        setSourceModel(aModel);
        setDynamicSortFilter(true);
        sort(0, sortOrder());
        invalidate();
        emit srcModelChanged();
    }
}

void SortProxyModel::setSortAscending(bool ascending)
{
    if (sortAscending() != ascending) {
        setDynamicSortFilter(true);
        sort(0, (ascending ? Qt::AscendingOrder : Qt::DescendingOrder));
        invalidate();
        emit sortOrderChanged();
    }
}

int SortProxyModel::mapToSrcRow(int row) const
{
    QModelIndex srcIndex = mapToSource(index(row, 0));
    return srcIndex.row();
}

