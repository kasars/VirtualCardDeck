// SPDX-FileCopyrightText: 2018 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: GPL-2.0-or-later
#pragma once

#include <QObject>
#include <QDebug>

class VCDCard: public QObject
{
    Q_OBJECT

public:
    enum Family {
        Hearts = 0,
        Diamonds = 1,
        Spades = 2,
        Clubs = 3,
    };
    Q_ENUM(Family)

    enum Value {
        Ace = 1,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,
        Jack,
        Queen,
        King,
        Joker = 500
    };
    Q_ENUM(Value)

    enum Color {
        Red,
        Black,
    };

    VCDCard(Family f=Hearts, Value v=Ace, QObject *parent = nullptr): QObject(parent), card_id(f * 100 + v), selected(false) {}
    VCDCard(const VCDCard &other): QObject(), card_id(other.card_id), selected(other.selected) {}
    VCDCard(int cardId, QObject *parent = nullptr): QObject(parent), card_id(cardId) {}

    VCDCard& operator=(const VCDCard &other) {
        card_id = other.card_id;
        return *this;
    }

    inline bool operator<(const VCDCard &other) const {
        return card_id < other.card_id;
    }

    ~VCDCard() {}

    Family family() const { return (Family)(card_id / 100); }
    Value value() const { return (Value)(card_id % 100); }

    Color color() const { return card_id < 200 ? Red : Black; }

    int cardId() const { return card_id; }

    bool isJoker() const { return card_id == 500; }

    bool isValid() const { return card_id != 0; }

private:
    int card_id = 0;

public:
    // FIXME this is not a property of the card
    bool selected = false;
};

struct VCDCardAndPosition
{
    int cardId = -1;
    int deckIndex = -1;
    int inDeckIndex = -1;
    bool isValid() const { return cardId != -1; }
    bool operator==(const VCDCardAndPosition&) const = default;
};

Q_DECLARE_METATYPE(VCDCard);
Q_DECLARE_METATYPE(VCDCardAndPosition);

inline QDebug operator<<(QDebug debug, const VCDCard &card)
{
    QDebugStateSaver saver(debug);
    debug.nospace().noquote() << "[VCDCard Family: " << card.family() << ", Value: " << card.value() << "]";
    return debug;
}

inline QDebug operator<<(QDebug debug, const VCDCardAndPosition &cardPos)
{
    QDebugStateSaver saver(debug);
    debug.nospace().noquote() << "[VCDCardAndPosition cardId: " << cardPos.cardId << ", deckIndex: " << cardPos.deckIndex << "]";
    return debug;
}
