/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#include "VCDGameControl.h"
#include "VCDCard.h"
#include <QMutex>
#include <QVector>
#include <QHash>
#include <QTimer>
#include <QDebug>

#include <QJsonObject>
#include <QJsonDocument>

class VCDGameControl::Private {
public:
    struct Move {
        QObject *fromDeck;
        QObject *toDeck;
        int fromIndex = 0;
        int toIndex = 0;
        int moveId = 0;
        int cardId = 0;
    };

    Private() {}

    QVector<Move> moves;

    QHash<QString, QObject*> decks;
    QHash<QObject*, QString> deckIds;
};

VCDGameControl::VCDGameControl() : QObject(), d(new Private()) {}
VCDGameControl::~VCDGameControl() { delete d; }


bool VCDGameControl::undoLast()
{
    if (d->moves.isEmpty()) {
        return false;
    }
    Private::Move move = d->moves.last();
    emit undoMove(move.fromDeck, move.toDeck, move.fromIndex, move.toIndex, move.moveId, move.cardId);
    emit signalUndoToRemote();
    return true;
}

bool VCDGameControl::undoLastRemote()
{
    if (d->moves.isEmpty()) {
        return false;
    }
    Private::Move move = d->moves.last();
    emit undoMove(move.fromDeck, move.toDeck, move.fromIndex, move.toIndex, move.moveId, move.cardId);
    return true;
}

void VCDGameControl::clearMoves()
{
    d->moves.clear();
}

int VCDGameControl::nextMoveId()
{
    if (d->moves.isEmpty()) {
        return 1;
    }
    return d->moves.last().moveId+1;
}


void VCDGameControl::moveCardFromTo(QObject *fromDeck, QObject *toDeck, int fromIndex, int toIndex, int moveId, int cardId, bool isRemote)
{
    if (!fromDeck || !toDeck) {
        qWarning() << "Null pointer!!!" << fromDeck << toDeck;
        return;
    }

    qDebug() << d->deckIds.value(fromDeck, QString());
    qDebug() << d->deckIds.value(toDeck, QString());
    qDebug() << QString::number(fromIndex);
    qDebug() << QString::number(toIndex);
    qDebug() << QString::number(moveId);
    qDebug() << QString::number(cardId);

    Private::Move move;
    move.fromDeck = fromDeck;
    move.toDeck = toDeck;
    move.fromIndex = fromIndex;
    move.toIndex = toIndex;
    move.moveId = moveId;
    move.cardId = cardId;
    d->moves.append(move);

    if (!isRemote) {
        QJsonObject moveInfos;
        moveInfos["fromDeck"] = d->deckIds.value(fromDeck, QString());
        moveInfos["toDeck"] = d->deckIds.value(toDeck, QString());
        moveInfos["fromIndex"] = fromIndex;
        moveInfos["toIndex"] = toIndex;
        moveInfos["moveId"] = moveId;
        moveInfos["cardId"] = cardId;
        QJsonDocument moveDoc(moveInfos);
        emit signalMoveToRemote(moveDoc.toJson(QJsonDocument::Compact));
    }
}

void VCDGameControl::moveUndone()
{
    Private::Move move = d->moves.takeLast();
    if (!d->moves.isEmpty() && d->moves.last().moveId == move.moveId) {
        QTimer::singleShot(1, this, &VCDGameControl::undoLast);
    }
}

void VCDGameControl::setDeckId(QObject *deck, const QString &deckId)
{
    //qDebug() << deck << deckId;
    d->decks.insert(deckId, deck);
    d->deckIds.insert(deck, deckId);

    connect(deck, &QObject::destroyed, [this, deck]() {
        //qDebug() << d->deckIds.value(deck);
        d->decks.remove(d->deckIds.take(deck));
    });
}

bool VCDGameControl::remoteMoved(const QString &moveInfo)
{
    qDebug() << moveInfo;
    QJsonParseError error;
    QJsonDocument doc(QJsonDocument::fromJson(moveInfo.toUtf8(), &error));
    QJsonObject root = doc.object();

    QObject *fromDeck = d->decks.value(root["fromDeck"].toString(), nullptr);
    QObject *toDeck = d->decks.value(root["toDeck"].toString(), nullptr);
    int fromIndex = root["fromIndex"].toInt();
    int toIndex = root["toIndex"].toInt();
    qDebug() << root["moveId"];
    int moveId = root["moveId"].toInt();
    int cardId = root["cardId"].toInt();

    if (!fromDeck || !toDeck) {
        qDebug() << "Got nullptr" << fromDeck << toDeck;
        return false;
    }

    if (!d->moves.isEmpty() && moveId != d->moves.last().moveId+1) {
        qDebug() << "Bad moveId" << moveId << " != " << d->moves.last().moveId+1;
        return false;
    }

    emit remoteMove(fromDeck, toDeck, fromIndex, toIndex, moveId, cardId);
    return true;
}
