/* ============================================================
 *
 * Copyright (C) 2022 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#ifndef VCDHostInfo_h
#define VCDHostInfo_h

#include <QObject>
#include <QElapsedTimer>
#include <QDebug>

class VCDHostInfo: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString hostId READ hostId WRITE setHostId NOTIFY hostChanged)
    Q_PROPERTY(QString hostName READ hostName NOTIFY hostChanged)
    Q_PROPERTY(QString gameName READ gameName WRITE setGameName NOTIFY hostChanged)
    Q_PROPERTY(int numPlayers READ numPlayers WRITE setNumPlayers NOTIFY hostChanged)
    Q_PROPERTY(int numJoined READ numJoined WRITE setJoinedPlayers NOTIFY hostChanged)
    Q_PROPERTY(QString playerName READ playerName WRITE setPlayerName NOTIFY hostChanged)

Q_SIGNALS:
    void hostChanged();


public:

    QString m_hostId;
    QString m_gameName;
    QString m_playerName;
    int     m_numPlayers = 0;
    int     m_numJoined = 1;
    QElapsedTimer m_age;

    explicit VCDHostInfo(QObject *parent): QObject(parent) {}
    VCDHostInfo(const QString &hostId,
                const QString &gameName,
                const QString &playerName,
                int numPlayers,
                int numJoined,
                QObject *parent = 0)
    :QObject(parent),
    m_hostId(hostId),
    m_gameName(gameName),
    m_playerName(playerName),
    m_numPlayers(numPlayers),
    m_numJoined(numJoined) {
        m_age.restart();
    }

    const QString hostId() const { return m_hostId; }
    void setHostId(const QString &hostId) {
        if (m_hostId != hostId) {
            m_hostId = hostId;
            emit hostChanged();
        }
    }

    const QString hostName() const {
        QString host = m_hostId;
        host.remove(QLatin1String("::ffff:"));
        int i = host.lastIndexOf(QLatin1Char(';'));
        return host.left(i);
    }

    const QString gameName() const { return m_gameName; }
    void setGameName(const QString &gameName) {
        if (m_gameName != gameName) {
            m_gameName = gameName;
            qDebug() << gameName;
            emit hostChanged();
        }
    }

    const QString playerName() const { return m_playerName; }
    void setPlayerName(const QString &playerName) {
        if (m_playerName != playerName) {
            m_playerName = playerName;
            emit hostChanged();
        }
    }

    int numPlayers() const { return m_numPlayers; }
    void setNumPlayers(int numPlayers) {
        if (m_numPlayers != numPlayers) {
            m_numPlayers = numPlayers;
            emit hostChanged();
        }
    }

    int numJoined() const { return m_numJoined; }
    void setJoinedPlayers(int numJoined) {
        if (m_numJoined != numJoined) {
            m_numJoined = numJoined;
            emit hostChanged();
        }
    }
};


#endif
