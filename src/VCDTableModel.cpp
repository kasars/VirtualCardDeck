/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#include "VCDTableModel.h"

#include <QDebug>
#include <QDateTime>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QFile>



class VCDTableModel::Private {
public:
    struct DeckInfo {
        QString player;
        int row = 0;
        int column = 0;
        int rowSpan = 1;
        int columnSpan = 1;
        QString type;
        bool faceUp = true;
        QString allowedValue;
        QString allowedFamily;
        bool allowMultiple = false;
        bool pullAllowed = false;
        bool allowAutoPull = false;
        int dealCount = 0;
        int topDealCount = 0;

    };

    QList<DeckInfo> decks;
    QString gameName;
    bool playerHasHand = false;
    bool showAllPlayerHands = false;
    int playerCount = 1;
    int playerRows = 1;
    int playerColumns = 1;
    int commonRows = 0;
    int commonColumns = 0;

    QString gameConfig;

    bool validIndex(int i) { return i>=0 && i<decks.count(); }
};

VCDTableModel::VCDTableModel(QObject *parent) : QAbstractListModel(parent), d(new Private()) {}
VCDTableModel::~VCDTableModel() {}

QString VCDTableModel::gameName() const { return d->gameName; }
bool    VCDTableModel::playerHasHand() const { return d->playerHasHand; }
bool    VCDTableModel::showAllPlayerHands() const { return d->showAllPlayerHands; }
int     VCDTableModel::playerCount() const { return d->playerCount; }
int     VCDTableModel::playerRows() const { return d->playerRows; }
int     VCDTableModel::playerColumns() const { return d->playerColumns; }
int     VCDTableModel::commonRows() const { return d->commonRows; }
int     VCDTableModel::commonColumns() const { return d->commonColumns; }


const QString VCDTableModel::config() const { return d->gameConfig; }

Q_INVOKABLE bool VCDTableModel::setConfig(const QString &data)
{
    beginResetModel();

    d->gameConfig.clear();
    d->decks.clear();
    d->gameName.clear();
    d->playerHasHand = false;
    d->showAllPlayerHands = false;
    d->playerCount = 1;
    d->playerRows = 1;
    d->playerColumns = 1;

    if (data.isEmpty()) {
        endResetModel();
        return false;
    }

    QJsonParseError error;
    QJsonDocument confDoc(QJsonDocument::fromJson(data.toUtf8(),&error));
    QJsonObject root = confDoc.object();

    if (root.isEmpty()) {
        emit failedToReadGameConfig();
        qDebug() << "Failed to read configuration" << error.errorString() << error.offset;
        qDebug() << "data:" << data << root;
        endResetModel();
        return false;
    }

    d->gameName = root["name"].toString();
    d->playerHasHand = root["playerHasHand"].toBool();
    d->showAllPlayerHands = root["showAllPlayerHands"].toBool();
    d->playerCount = root["playerCount"].toInt();

    QJsonObject pDecks = root["playerDecks"].toObject();

    d->playerRows = pDecks["rows"].toInt();
    d->playerColumns = pDecks["columns"].toInt();
    QJsonArray array = pDecks["decks"].toArray();
    for (int i = 0; i < array.size(); ++i) {
        QJsonObject deck = array[i].toObject();
        Private::DeckInfo info;
        info.row = deck["row"].toInt();
        info.column = deck["column"].toInt();
        info.rowSpan = deck["rowSpan"].toInt();
        info.columnSpan = deck["columnSpan"].toInt();
        info.type = deck["type"].toString();
        info.faceUp = deck["faceUp"].toBool();
        info.allowedValue = deck["allowedValue"].toString();
        info.allowedFamily = deck["allowedFamily"].toString();
        info.allowMultiple = deck["allowMultiple"].toBool();
        info.pullAllowed = deck["pullAllowed"].toBool();
        info.allowAutoPull = deck["allowAutoPull"].toBool();
        info.dealCount = deck["dealCount"].toInt(0);
        info.topDealCount = deck["topDealCount"].toInt(0);
        info.player = "player0Decks";
        d->decks.append(info);
        for (int j=1; j<d->playerCount; ++j) {
            info.player = QString("player%1Decks").arg(j);
            d->decks.append(info);
        }
    }

    QJsonObject cDecks = root["commonDecks"].toObject();
    d->commonRows = cDecks["rows"].toInt();
    d->commonColumns = cDecks["columns"].toInt();
    array = cDecks["decks"].toArray();
    for (int i = 0; i < array.size(); ++i) {
        QJsonObject deck = array[i].toObject();
        Private::DeckInfo info;
        info.row = deck["row"].toInt();
        info.column = deck["column"].toInt();
        info.rowSpan = deck["rowSpan"].toInt();
        info.columnSpan = deck["columnSpan"].toInt();
        info.type = deck["type"].toString();
        info.faceUp = deck["faceUp"].toBool();
        info.allowedValue = deck["allowedValue"].toString();
        info.allowedFamily = deck["allowedFamily"].toString();
        info.allowMultiple = deck["allowMultiple"].toBool();
        info.pullAllowed = deck["pullAllowed"].toBool();
        info.allowAutoPull = deck["allowAutoPull"].toBool();
        info.dealCount = deck["dealCount"].toInt();
        info.topDealCount = deck["topDealCount"].toInt();
        info.player = "common";
        d->decks.append(info);
    }


    d->gameConfig = data;
    emit gameChanged();

    endResetModel();
    return true;
}

Q_INVOKABLE bool VCDTableModel::setConfigFile(const QString &path)
{
    QFile loadConfig(path);
    if (!loadConfig.open(QIODevice::ReadOnly)) {
        qDebug() << "Failed to read" << path;
        emit failedToReadGameConfig();
        setConfig(QString());
        return false;
    }
    QString config = loadConfig.readAll();
    return setConfig(config);
}

QHash<int, QByteArray> VCDTableModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[PlayerRole]        = "player";
    roles[RowRole]           = "row";
    roles[ColumnRole]        = "column";
    roles[RowSpanRole]       = "rowSpan";
    roles[ColumnSpanRole]    = "columnSpan";
    roles[TypeRole]          = "type";
    roles[FaceUpRole]        = "faceUp";
    roles[AllowedValueRole]  = "allowedValue";
    roles[AllowedFamilyRole] = "allowedFamily";
    roles[AllowMultipleRole] = "allowMultiple";
    roles[PullAllowedRole]   = "pullAllowed";
    roles[AllowPullRole]     = "allowAutoPull";
    roles[DealCountRole]     = "dealCount";
    roles[TopDealCountRole]  = "topDealCount";

    return roles;
}


int VCDTableModel::rowCount(const QModelIndex &) const
{
    return d->decks.count();
}


QVariant VCDTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !d->validIndex(index.row())) {
        return QVariant();
    }

    switch (role) {
        case PlayerRole:
            return d->decks[index.row()].player;
        case RowRole:
            return d->decks[index.row()].row;
        case ColumnRole:
            return d->decks[index.row()].column;
        case RowSpanRole:
            return d->decks[index.row()].rowSpan;
        case ColumnSpanRole:
            return d->decks[index.row()].columnSpan;
        case TypeRole:
            return d->decks[index.row()].type;
        case FaceUpRole:
            return d->decks[index.row()].faceUp;
        case AllowedValueRole:
            return d->decks[index.row()].allowedValue;
        case AllowedFamilyRole:
            return d->decks[index.row()].allowedFamily;
        case AllowMultipleRole:
            return d->decks[index.row()].allowMultiple;
        case PullAllowedRole:
            return d->decks[index.row()].pullAllowed;
        case AllowPullRole:
            return d->decks[index.row()].allowAutoPull;
        case DealCountRole:
            return d->decks[index.row()].dealCount;
        case TopDealCountRole:
            return d->decks[index.row()].topDealCount;
    }
    return QVariant();
}


