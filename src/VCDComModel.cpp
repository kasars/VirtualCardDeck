/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#include "VCDComModel.h"
#include "VCDHostInfo.h"
#include <QTimer>
#include <QDebug>
#include <QUdpSocket>
#include <QTcpSocket>
#include <QTcpServer>
#include <QNetworkInterface>
#include <QTimer>
#include <QVector>
#include <QFile>
#include <QSettings>

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>


class VCDComModel::Private {
public:
    Private(VCDComModel *pub): q(pub) {}
    VCDComModel *q;
    QString gameName;
    QString gameFileName;
    QString playerName;
    int playerIndex = 0;

    int numPlayers = 0;

    bool gameStarted = false;

    QList<VCDHostInfo*> hostInfos;

    QUdpSocket hostRSocket;
    QUdpSocket bcastSocket;

    QTcpSocket tcpSocket;
    QTcpServer tcpServer;
    QList<QTcpSocket *> clientSockets;
    QStringList joinedPlayers;


    QTimer sendHostRequestTmr;
    QTimer cleanHostsTmr;

    enum SendLevel {
        SendInfoOnly,
        SendGameDeckData,
    };


    int hostIndex(const QString &hostId) {
        for (int i=0; i<hostInfos.size(); ++i) {
            if (hostInfos[i]->m_hostId == hostId) {
                return i;
            }
        }
        return -1;
    }


    void pendingDatagrams(bool *hChanged) {
        QByteArray datagram;
        QHostAddress senderAddr;
        quint16 port = 0;

        while (hostRSocket.hasPendingDatagrams()) {
            datagram.resize((int)hostRSocket.pendingDatagramSize());
            hostRSocket.readDatagram(datagram.data(), datagram.size(), &senderAddr, &port);

            if (datagram == "##VCD##WhoHas##") {
                QByteArray datagram = gameName.toUtf8() + "##VCD##" + playerName.toUtf8() + "##VCD##" + QByteArray::number(clientSockets.count()+1) + "##VCD##" + QByteArray::number(numPlayers);
                //qDebug() << "Sending:" << datagram;
                bcastSocket.writeDatagram(datagram, senderAddr, 46348);
            }
            else {
                QString datagramStr(datagram);
                QStringList infos = datagramStr.split(QLatin1String("##VCD##"));
                //qDebug() << infos << senderAddr.toString() << port;
                if (infos.size() != 4) {
                    qDebug() << "Not a VCD message";
                    continue;
                }

                QString hostId = senderAddr.toString() + QLatin1Char(';') + QString::number(port);
                QString gName = infos[0];
                QString pName = infos[1];
                int     jPlayers = infos[2].toInt();
                int     nPlayers = infos[3].toInt();

                int hIndex = hostIndex(hostId);
                if (hIndex == -1) {
                    hostInfos.append(new VCDHostInfo(hostId, gName, pName, nPlayers, jPlayers));
                    if (hChanged) { *hChanged = true; }
                }
                else {
                    VCDHostInfo *i = hostInfos[hIndex];
                    if (i && (i->gameName() != gName || i->playerName() != pName ||
                        i->numPlayers() != nPlayers || i->numJoined() != jPlayers))
                    {
                        i->setGameName(gName);
                        i->setPlayerName(pName);
                        i->setNumPlayers(nPlayers);
                        i->setJoinedPlayers(jPlayers);
                    }
                    i->m_age.restart();
                }
            }
        }
    }


    bool removeOldHostInfos() {
        bool removed = false;
        for (int i=hostInfos.size()-1; i>=0; --i) {
            if (hostInfos[i]->m_age.elapsed() > 1000) {
                removed = true;
                delete hostInfos.takeAt(i);
            }
        }
        return removed;
    }

    void broadcastHwoHas() {
        QByteArray datagram = "##VCD##WhoHas##";
        bool sentOK = false;

        // Get the list of all interfaces
        const QList<QNetworkInterface> ifaces = QNetworkInterface::allInterfaces();

        for (auto iface: ifaces) {
            // Get all IP addresses for the interface
            const QList<QNetworkAddressEntry> addrs = iface.addressEntries();

            for (auto addr: addrs) {
                // Only send from LocalHost if no other network is configured;
                if (addr.ip().toString() == "127.0.0.1") continue;

                //qDebug() << "Sending from" << addr.ip().toString() << addr.broadcast().toString();
                if (addr.broadcast().toString() != "") {
                    int sent = bcastSocket.writeDatagram(datagram, addr.broadcast(), 23174);
                    if (sent == -1) {
                        qDebug() << "failed to send on" << addr.ip().toString() << bcastSocket.error();
                    }
                    else {
                        sentOK = true;
                    }
                }
            }
        }

        // Only send from LocalHost if no other network is configured;
        if (!sentOK) {
            int sent = bcastSocket.writeDatagram(datagram, QHostAddress::LocalHost, 23174);
            if (sent == -1) {
                qDebug() << "lo sent == -1" << bcastSocket.error();
            }
        }
    }

    void playerWantsToJoin() {
        while (tcpServer.hasPendingConnections()) {
            //qDebug() << "player wants to join";
            QTcpSocket *clientConnection = tcpServer.nextPendingConnection();
            connect(clientConnection, &QAbstractSocket::disconnected, clientConnection, &QObject::deleteLater);

            if (q->numJoined() == numPlayers) {
                qDebug() << "Game full";
                clientConnection->close();
                continue;
            }
            connect(clientConnection, &QTcpSocket::destroyed, q, [this](QObject *obj) { playerLeft(obj); });
            connect(clientConnection, &QIODevice::readyRead, q, [this]() { clientSentData(); });
            clientSockets.append(clientConnection);
            qDebug() << "gameChanged();";
            emit q->gameChanged();
            sendGameUpdate(SendGameDeckData);
            clientConnection->write("{ \"cmd\": \"SendGameUpdate\" }\n");
        }
    }

    void playerLeft(QObject *obj) {
        for (int i=0; i<clientSockets.size(); ++i) {
            if (obj == clientSockets[i]) {
                if (i>=joinedPlayers.size()) {
                    qDebug() << "Programming error!! clients and joined players must match";
                    qDebug() << i << ">=" << joinedPlayers.size();
                }
                else {
                    joinedPlayers[i] = tr("Waiting for player");
                }
                clientSockets.removeAt(i);
                qDebug() << "gameChanged();";
                emit q->gameChanged();
                sendGameUpdate();
                return;
            }
        }
        qDebug() << "Bad TCP socket" << obj;
        return;
    }

    void serverSentData() {
        while (tcpSocket.canReadLine()) {
            QByteArray jsonData = tcpSocket.readLine();
            QJsonParseError error;
            QJsonDocument doc(QJsonDocument::fromJson(jsonData, &error));
            QJsonObject root = doc.object();

            if (root.isEmpty()) {
                qDebug() << "failed to read Json data:" << error.errorString() << jsonData;
                continue;
            }
            QString cmd = root["cmd"].toString();

            if (cmd.isEmpty()) {
                qDebug() << "No command found";
            }
            else if (cmd == QLatin1String("gameUpdated")) {
                if (root.contains("gameData")) {
                    QJsonDocument doc(root["gameData"].toObject());
                    QString gData = doc.toJson(QJsonDocument::Compact);
                    emit q->gameDeckDataChanged(gData);
                }

                joinedPlayers.clear();
                QJsonArray array = root["joinedPlayers"].toArray();
                for (int i=0; i<array.size(); ++i) {
                    QJsonObject player = array[i].toObject();
                    joinedPlayers.append(player["playerName"].toString());
                }
                numPlayers = root["numPlayers"].toInt();
                playerIndex = root["playerIndex"].toInt();

                qDebug() << "gameChanged();";
                emit q->gameChanged();
                sendGameUpdate();
            }
            else if (cmd == QLatin1String("cardMoved")) {
                qDebug() << "Not implemented";
            }
            else if (cmd == QLatin1String("SendGameUpdate")) {
                //qDebug() << "Server asked to send game info";
                sendGameUpdate();
            }
            else if (cmd == QLatin1String("DealCards")) {
                qDebug() << "Server sent cards";
                QJsonDocument doc(root["cards"].toObject());
                QString cardData = doc.toJson(QJsonDocument::Compact);
                emit q->dealerDeckSet(cardData);
            }
            else if (cmd == QLatin1String("StartGame")) {
                qDebug() << "Server sent StartGame";
                gameStarted = true;
                emit q->gameChanged();
            }
            else if (cmd == QLatin1String("MoveCard")) {
                qDebug() << "Server sent card move";
                QJsonDocument doc(root["moveInfo"].toObject());
                QString moveInfo = doc.toJson(QJsonDocument::Compact);
                q->remoteMove(moveInfo);
            }
            else if (cmd == QLatin1String("UndoLast")) {
                qDebug() << "Server sent undo last";
                emit q->remoteUndoLast();
            }
            else {
                qDebug() << "Unknown command" << cmd;
            }
        }
    }

    void clientSentData() {
        for (int i=0; i<clientSockets.size(); ++i) {
            // NOTE: clientSockets.size() must be less or equal to joinedPlayers.size()
            while (clientSockets[i]->canReadLine()) {
                QByteArray jsonData = clientSockets[i]->readLine();
                QJsonParseError error;
                QJsonDocument doc(QJsonDocument::fromJson(jsonData, &error));
                QJsonObject root = doc.object();

                if (root.isEmpty()) {
                    qDebug() << "failed to read Json data" << jsonData;
                    qDebug() << "Error" << error.errorString();
                    continue;
                }
                QString cmd = root["cmd"].toString();

                if (cmd.isEmpty()) {
                    qDebug() << "No command found";
                }
                else if (cmd == QLatin1String("gameUpdated")) {
                    if (i>=joinedPlayers.size()) {
                        qDebug() << "Programming error!! clients and joined players must match";
                        qDebug() << i << ">=" << joinedPlayers.size();
                    }
                    else {
                        if (joinedPlayers[i] != root["playerName"].toString()) {
                            joinedPlayers[i] = root["playerName"].toString();
                            qDebug() << "gameChanged();";
                            emit q->gameChanged();
                            sendGameUpdate();
                        }
                    }
                }
                else if (cmd == QLatin1String("MoveCard")) {
                    qDebug() << "Client sent card move";
                    QJsonDocument doc(root["moveInfo"].toObject());
                    QString moveInfo = doc.toJson(QJsonDocument::Compact);
                    q->remoteMove(moveInfo);

                    for (int j=0; j<clientSockets.size(); ++j) {
                        if (i != j) {
                            clientSockets[j]->write(jsonData);
                        }
                    }
                }
                else if (cmd == QLatin1String("UndoLast")) {
                    qDebug() << "Client sent \"undo last\"";
                    emit q->remoteUndoLast();

                    for (int j=0; j<clientSockets.size(); ++j) {
                        if (i != j) {
                            clientSockets[j]->write(jsonData);
                        }
                    }
                }
                else {
                    qDebug() << "Unknown command" << cmd;
                }
            }
        }
    }

    void sendGameUpdate(SendLevel level = SendInfoOnly) {
        QString gameInfo = QStringLiteral("{ \"cmd\": \"gameUpdated\", \"playerName\": \"%1\", \"gameName\": \"%2\", \"numPlayers\": \"%3\", \"numJoined\": \"%4\", \"joinedPlayers\": [ ")
        .arg(playerName).arg(gameName).arg(numPlayers).arg(q->numJoined());

        gameInfo += QStringLiteral(" { \"playerName\": \"%1\" } ").arg(playerName);
        for (auto player: joinedPlayers) {
            gameInfo += QStringLiteral(", { \"playerName\": \"%1\" } ").arg(player);
        }
        gameInfo += QStringLiteral("] ");

        if (!gameFileName.isEmpty() && level == SendGameDeckData) {
            QFile gameDataFile(gameFileName);
            if (!gameDataFile.open(QIODevice::ReadOnly)) {
                qDebug() << "Failed to read" << gameFileName;
            }
            else {
                QString gameData = gameDataFile.readAll();
                gameData.remove(QLatin1Char('\n'));

                if (!gameData.isEmpty()) {
                    gameInfo += QStringLiteral(", \"gameData\": %1").arg(gameData);
                }
            }
        }

        gameInfo += QLatin1String("}\n");

        if (clientSockets.isEmpty() && tcpSocket.state() == QAbstractSocket::ConnectedState) {
            tcpSocket.write(gameInfo.toUtf8());
        }
        else {
            for (int i=0; i<clientSockets.count(); ++i) {
                gameInfo.replace(QLatin1String("}\n"), QStringLiteral(",\"playerIndex\":%1}\n").arg(i+1));
                clientSockets[i]->write(gameInfo.toUtf8());
            }
        }
    }

};

VCDComModel::VCDComModel() : QObject(), d(new Private(this))
{
    connect(&d->sendHostRequestTmr, &QTimer::timeout, this, [this]() { d->broadcastHwoHas(); });

    connect(&d->cleanHostsTmr, &QTimer::timeout, this, [this]() {
        if (d->removeOldHostInfos()) {
            emit hostsChanged();
        }
    });

    d->cleanHostsTmr.start(400);

    connect(&d->hostRSocket, &QUdpSocket::readyRead, this, [this]() {
        bool hChanged = false;
        d->pendingDatagrams(&hChanged);
        if (hChanged) {
            emit hostsChanged();
        }
    });

    // Client connects to the server
    connect(&d->tcpServer, &QTcpServer::newConnection, this, [this]() { d->playerWantsToJoin(); });

    connect(&d->tcpSocket, &QIODevice::readyRead, this, [this]() { d->serverSentData(); });
    #if QT_VERSION >= QT_VERSION_CHECK(5,15,0)
    connect(&d->tcpSocket, &QAbstractSocket::errorOccurred,
    #else
    connect(&d->tcpSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),
    #endif
            this, [](QAbstractSocket::SocketError err) { qDebug() << err; });

    connect(&d->tcpSocket, &QTcpSocket::stateChanged, this, [this](QAbstractSocket::SocketState state) {
        qDebug() << state;
        if (state == QAbstractSocket::UnconnectedState) {
            waitForGame();
        }
    });

    QSettings settings;

    d->playerName = settings.value("playerName", "Player NN").toString();

    waitForGame();
}

VCDComModel::~VCDComModel()
{
    QSettings settings;
    settings.setValue("playerName", d->playerName);
    settings.sync();

    for (QTcpSocket *client: d->clientSockets) {
        client->disconnect();
        client->abort();
    }
    d->tcpSocket.disconnect();
    d->tcpServer.disconnect();

    qDeleteAll(d->clientSockets);
    qDeleteAll(d->hostInfos);

    delete d;
}

#ifdef MULTIPLAYER_DISABLED
bool VCDComModel::isServer() const { return true; }
bool VCDComModel::readyToStart() const { return true; }
#else
bool VCDComModel::isServer() const { return d->tcpServer.isListening() || !d->clientSockets.isEmpty(); }
bool VCDComModel::readyToStart() const { return d->clientSockets.count() == d->numPlayers-1; }
#endif

bool VCDComModel::gameStarted() const { return d->gameStarted; }


const QString VCDComModel::gameName() const { return d->gameName; }
const QString VCDComModel::gameFileName() const { return d->gameFileName; }
int VCDComModel::numPlayers() const { return d->numPlayers; }
int VCDComModel::numJoined() const { return d->clientSockets.count()+1; }
const QStringList VCDComModel::joinedPlayers() const
{
    if (isServer() || d->joinedPlayers.isEmpty()) return QStringList(d->playerName) + d->joinedPlayers;

    return d->joinedPlayers;
}
const QString VCDComModel::playerName() const { return d->playerName; }

void VCDComModel::setPlayerName(const QString &playerName)
{
    if (d->playerName != playerName) {
        d->playerName = playerName;
        QSettings settings;
        settings.setValue("playerName", d->playerName);
        settings.sync();
        qDebug() << "gameChanged();";
        emit gameChanged();
        d->sendGameUpdate();
    }
}

int VCDComModel::playerIndex() const { return d->playerIndex; }


void VCDComModel::hostGame(const QString &name, const QString &fileName, int numPlayers)
{
    if (d->gameStarted) {
        qDebug() << "Game already started!!!";
        return;
    }

    d->gameName = name;
    d->gameFileName = fileName;
    d->numPlayers = numPlayers;
    d->joinedPlayers.clear();
    for (int i=0; i<numPlayers-1; ++i) {
        d->joinedPlayers << tr("Waiting for player");
    }

    d->sendHostRequestTmr.stop();
    d->hostRSocket.close();

    #ifndef MULTIPLAYER_DISABLED
    bool bindOK = d->hostRSocket.bind(QHostAddress::Any, 23174, QUdpSocket::ShareAddress | QAbstractSocket::ReuseAddressHint);
    //qDebug() << bindOK << name << fileName << numPlayers << d->hostRSocket.multicastInterface().humanReadableName();
    if (!bindOK) {
        qDebug() << "Failed to bind the socket";
    }

    //const QList<QNetworkInterface> allIFs = QNetworkInterface::allInterfaces();
    //for (const auto i: allIFs) {
    //    qDebug() << i.humanReadableName();
    //}


    if (!d->tcpServer.isListening()) {
        if (!d->tcpServer.listen(QHostAddress::Any, 23173)) {
            qDebug() << "Failed to start server" << d->tcpServer.errorString();
            waitForGame();
        }
    }

    #endif
    qDebug() << "gameChanged();";
    emit gameChanged();

    #ifndef MULTIPLAYER_DISABLED
    d->sendGameUpdate(Private::SendGameDeckData);
    #endif
}

bool VCDComModel::waitingForStart() const
{
    #ifndef MULTIPLAYER_DISABLED
    return d->tcpSocket.state() > QAbstractSocket::UnconnectedState || d->tcpServer.isListening();
    #else
    return true;
    #endif
}


void VCDComModel::joinGame(const QString &hostId)
{
    if (d->tcpSocket.state() > QAbstractSocket::UnconnectedState) {
        qDebug() << "already connected" << hostId;
    }

    int hIndex = d->hostIndex(hostId);
    if (hIndex == -1) {
        qDebug() << "No such host" << hostId;
        return;
    }
    else {
        d->tcpServer.close();
        d->hostRSocket.close();
        d->sendHostRequestTmr.stop();
        d->tcpSocket.abort();
        QStringList hostStrs = hostId.split(QLatin1Char(';'));
        if (hostStrs.count() != 2) {
            qDebug() << "Bad host info";
            return;
        }
        d->tcpSocket.connectToHost(hostStrs[0], 23173);
    }
}

void VCDComModel::startGame()
{
    qDebug() << "----------------startGame()";
    if (d->clientSockets.count() != d->numPlayers-1) {
        qDebug() << "Not ready to start yet";
        return;
    }
    d->tcpServer.close();
    d->hostRSocket.close();
    d->gameStarted = true;
    qDebug() << "gameChanged();";
    emit gameChanged();
    d->sendGameUpdate();
    for (auto socket: d->clientSockets) {
        socket->write("{\"cmd\":\"StartGame\"}\n");
    }

}

const QList<QObject*> VCDComModel::hostInfos() const
{
    QList<QObject*> hostList;

    for (int i=0; i<d->hostInfos.size(); ++i) {
        hostList.append(d->hostInfos[i]);
    }
    return hostList;
}

void VCDComModel::sendCards(const QString &jsonData)
{
    // Only the server sends cards
    if (d->clientSockets.count() != d->numPlayers-1) {
        qDebug() << "Not ready to send cards yet";
        return;
    }

    QByteArray data = "{\"cmd\":\"DealCards\",\"cards\":"+jsonData.toUtf8()+"}\n";

    for (auto socket: d->clientSockets) {
        socket->write(data);
    }
}

void VCDComModel::sendMoveInfo(const QString &moveInfo)
{
    QByteArray data = "{\"cmd\":\"MoveCard\",\"moveInfo\":"+moveInfo.toUtf8()+"}\n";
    qDebug() << data;

    if (d->clientSockets.isEmpty() && d->tcpSocket.state() == QAbstractSocket::ConnectedState) {
        d->tcpSocket.write(data);
    }
    else {
        for (auto socket: d->clientSockets) {
            socket->write(data);
        }
    }
}

void VCDComModel::sendUndoLast()
{
    QByteArray data = "{\"cmd\":\"UndoLast\"}\n";

    if (d->clientSockets.isEmpty() && d->tcpSocket.state() == QAbstractSocket::ConnectedState) {
        d->tcpSocket.write(data);
    }
    else {
        for (auto socket: d->clientSockets) {
            socket->write(data);
        }
    }
}



void VCDComModel::waitForGame()
{
    qDebug() << "Reset all";
    d->gameStarted = false;
    d->gameName.clear();
    d->numPlayers = 0;
    d->playerIndex = 0;

    d->sendHostRequestTmr.start(1000);
    d->hostRSocket.close();
    #ifndef MULTIPLAYER_DISABLED
    d->hostRSocket.bind(QHostAddress::Any, 46348, QUdpSocket::ShareAddress | QAbstractSocket::ReuseAddressHint);

    // Stop server
    d->tcpServer.close();
    if (d->tcpSocket.isOpen()) {
        d->tcpSocket.abort();
    }
    for (QTcpSocket *client: d->clientSockets) {
        client->abort();
    }
    #endif
    d->joinedPlayers.clear();

    qDebug() << "gameChanged();";
    emit gameChanged();
    emit gameDeckDataChanged(QByteArray());
    d->sendGameUpdate();
}


