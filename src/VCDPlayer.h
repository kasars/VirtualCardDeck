// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QObject>
#include <QDebug>

struct VCDPlayerInfo {
    QString name;
    bool ready = false;
    uint64_t serverPlayerId = 0;
    int sortIndex = -1;
    bool operator==(const VCDPlayerInfo&) const = default;
};

typedef QList<VCDPlayerInfo> VCDPlayerList;
Q_DECLARE_METATYPE(VCDPlayerInfo);
Q_DECLARE_METATYPE(VCDPlayerList);

inline QDebug operator<<(QDebug debug, const VCDPlayerInfo &pl)
{
    QDebugStateSaver saver(debug);
    debug.nospace().noquote() << "[VCDPlayerInfo name: " << pl.name << ", ready: " << pl.ready << ", serverPlayerId: " << pl.serverPlayerId << "]";
    return debug;
}
