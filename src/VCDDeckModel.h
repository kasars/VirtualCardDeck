/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#ifndef VCDDeckModel_H
#define VCDDeckModel_H

#include <QObject>
#include <QAbstractListModel>
#include <QVariantList>

#include "VCDCard.h"

class VCDDeckModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum VCDDeckModelRoles {
        ColorRole = Qt::UserRole+1,
        FamilyRole,
        ValueRole,
        CardIdRole,
        SelectedRole,
    };
    Q_ENUM(VCDDeckModelRoles)

    explicit VCDDeckModel(QObject *parent = nullptr);

    Q_INVOKABLE void moveCardTo(QObject *targetDeck, int fromIndex, int toIndex);

    Q_INVOKABLE void setSelected(int row, bool selected);
    Q_INVOKABLE void clearSelection();

    Q_INVOKABLE void newDeck();
    Q_INVOKABLE void shuffle();

    Q_INVOKABLE void moveCard(int from, int to);

    Q_INVOKABLE void deleteCards();

    Q_INVOKABLE static QString familyStr(int f) { return f == VCDCard::Hearts ? "Hearts" : f == VCDCard::Diamonds ? "Diamonds" : f == VCDCard::Spades ? "Spades" : f == VCDCard::Clubs ? "Clubs" : ""; }
    Q_INVOKABLE static QString valueStr(int v) { return QString::number(v); }
    Q_INVOKABLE static QString famValStr(int f, int v) { return familyStr(f) + "_" + valueStr(v); }

    Q_INVOKABLE int cardId(int row) const;

    Q_INVOKABLE QString cardsJson();
    Q_INVOKABLE void setCardsJson(const QString &data);

public:
    QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    Q_INVOKABLE void addCardsTop(const QVariantList &cards);
    Q_INVOKABLE void addCardsBottom(const QVariantList &cards);
    Q_INVOKABLE void addCardsAt(int index, const QVariantList &cards);

    Q_INVOKABLE QVariantList takeSelected();
    Q_INVOKABLE QVariantList takeCardAt(int index);
    Q_INVOKABLE QVariantList takeCardsTop(int count);


    class Private;
    Private *const d;
};

#endif
