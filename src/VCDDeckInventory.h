// SPDX-FileCopyrightText: 2025 Kåre Särs <kare.sars@iki.fi>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "VCDCard.h"
#include <QObject>

struct VCDDeckData {
    // Bottom is QList::first()
    // Top is QList::last()
    QList<int> cardIds;
    uint64_t reservedByPlayerId = 0;
    bool operator==(const VCDDeckData &) const = default;
};

struct VCDDeckInventory {
    std::optional<QList<uint64_t>> reserveMove(uint64_t playerId, //
                                               const VCDCardAndPosition &source,
                                               const VCDCardAndPosition &target);
    bool moveCard(const VCDCardAndPosition &source, const VCDCardAndPosition &target);

    /** This function return a list of players that block the move. The move is not blocked
     * if it is the specified player that has reserved the decks
     */
    std::optional<QList<uint64_t>> moveBlocked(uint64_t playerId, //
                                               const VCDCardAndPosition &source,
                                               const VCDCardAndPosition &target);

    void clear()
    {
        decks.clear();
    }
    QList<VCDDeckData> decks;
    bool operator==(const VCDDeckInventory &other) const = default;
};

Q_DECLARE_METATYPE(VCDDeckInventory);

inline QDebug operator<<(QDebug debug, const VCDDeckData &dd)
{
    QDebugStateSaver saver(debug);
    debug.nospace().noquote() << "[VCDDeckData cardIds: " << dd.cardIds
                              << "reservedByPlayerId:" << dd.reservedByPlayerId << "]";
    return debug;
}

inline QDebug operator<<(QDebug debug, const VCDDeckInventory &di)
{
    QDebugStateSaver saver(debug);
    debug.nospace().noquote() << "[VCDDeckInventory decks:\n";
    int i = 0;
    for (const auto &deck : di.decks) {
        debug.nospace().noquote() << QString("    %1: ").arg(i, 3, u' ') << deck << '\n';
        i++;
    }
    return debug;
}
