#!/bin/sh

APP_PID=$(adb shell pidof org.sars.virtualsolitaire)

adb logcat --pid $APP_PID
